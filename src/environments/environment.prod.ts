// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  name: 'production',
  production: true,
  apiHost: 'http://bcfnet.bcfadm.com.br/api',
  uploadFile: 'http://bcfnet.bcfadm.com.br/api/v1/uploads/',
  websiteHost: 'https://bcfnet.com.br/'
};
