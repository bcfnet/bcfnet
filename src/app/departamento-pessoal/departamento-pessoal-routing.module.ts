import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DepartamentoPessoalComponent } from './departamento-pessoal.component';

const routes: Routes = [
  {
    path: '',
    component: DepartamentoPessoalComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartamentoPessoalRoutingModule { }
