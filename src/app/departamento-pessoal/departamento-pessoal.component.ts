import { Component, OnInit } from '@angular/core';
import { AdministrativoService } from '../shared/services/administrativo.service';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { UtilsService } from '../shared/services/utils.service';

@Component({
  selector: 'app-departamento-pessoal',
  templateUrl: './departamento-pessoal.component.html',
  styleUrls: ['./departamento-pessoal.component.scss']
})
export class DepartamentoPessoalComponent implements OnInit {

  public datas = [];
  public loading = true;

  constructor(
    private administrativoService: AdministrativoService,
    private spinnerService: SpinnerService,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.administrativoService.get('departamento_pessoal', 'Informações')
      .subscribe((data: any) => {
        this.datas = data
          .map((administrativo) => {
            administrativo.arvore = this.utils.convertToUTF8(administrativo.arvore.split('#')[2]);
            return administrativo;
          });
        this.spinnerService.hide();
        this.loading = false;
    });
  }

}
