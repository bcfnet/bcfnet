import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DepartamentoPessoalRoutingModule } from './departamento-pessoal-routing.module';
import { DepartamentoPessoalComponent } from './departamento-pessoal.component';

@NgModule({
  imports: [
    CommonModule,
    DepartamentoPessoalRoutingModule
  ],
  declarations: [DepartamentoPessoalComponent]
})
export class DepartamentoPessoalModule { }
