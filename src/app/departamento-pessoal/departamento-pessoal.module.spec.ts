import { DepartamentoPessoalModule } from './departamento-pessoal.module';

describe('DepartamentoPessoalModule', () => {
  let departamentoPessoalModule: DepartamentoPessoalModule;

  beforeEach(() => {
    departamentoPessoalModule = new DepartamentoPessoalModule();
  });

  it('should create an instance', () => {
    expect(departamentoPessoalModule).toBeTruthy();
  });
});
