import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../shared/services/api.service';
import { User } from '../shared/models/user';
import { UserService } from '../shared/services/user.service';
import { Router } from '@angular/router';
import { SpinnerService } from '../shared/components/spinner/spinner.service';

@Injectable()
export class AuthService {
  private authUrl = '/v3/auth';

  constructor(
    private router: Router,
    private apiService: ApiService,
    private userService: UserService,
    private spinnerService: SpinnerService,
  ) { }

  login(user: User): Observable<any> {
    return this.apiService.login(user.login, user.password);
  }

  logout() {
    localStorage.clear();
    return true;
  }

  swapStatus() {
    return (localStorage.getItem('bcfnet.swap') != null);
  }

  swapOn(login: string, login_internet: any) {
    const swap = {
      login: login,
      login_internet: login_internet,
      status: 'S'
    };
    localStorage.setItem('bcfnet.swap', JSON.stringify(swap));
  }

  swapOff() {
    const swap = JSON.parse(localStorage.getItem('bcfnet.swap'));

    this.spinnerService.show();

    this.apiService.post('/swap', {
      login: swap.login,
      login_internet: swap.login_internet,
      status: 'B'
    })
    .subscribe((result: any) => {
      if (result.success === '1') {
        this.login({
          login: swap.login,
          password: result.senha
        })
        .subscribe((response) => {
          if (response.success === '1') {
            localStorage.removeItem('bcfnet.swap');
            // this.spinnerService.hide();
            // window.location.reload(true);
            window.location.href = '/';
          }
        });
      }
    }, (error) => {
      this.spinnerService.hide();
    });

    // localStorage.removeItem('bcfnet.swap');
  }

  recoverPassword(email: string): Observable<{message: string}> {
    return this.apiService.post(this.authUrl + '/esqueci-minha-senha', {
      email: email
    });
  }

}
