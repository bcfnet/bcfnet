import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { UserService } from '../../shared/services/user.service';
import { Router } from '@angular/router';
import { User } from '../../shared/models/user';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { PermissionsService } from '../../shared/services/permissions.service';
import { ToasterService } from 'angular2-toaster';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: User;
  errorMessage: string;
  submitting: boolean;
  loginform: FormGroup;
  password: FormControl;
  login: FormControl;
  @ViewChild('trigger') trigger: ElementRef;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private spinnerService: SpinnerService,
    private permissionService: PermissionsService,
    private toasterService: ToasterService,
    private router: Router,
  ) {
    console.log('Login component');
  }

  ngOnInit() {
    this.createFormControls();
    this.createForm();

    this.router.routerState.root.queryParams.subscribe(params => {
      if (params.login && params.password) {
        this.login.setValue(params.login);
        this.password.setValue(params.password);
        this.onSubmit();
      }
    }).unsubscribe();
  }

  onSubmit() {
    this.submitting = true;
    this.spinnerService.display(true);

    this.authService.login({
      login: this.login.value,
      password: this.password.value
    })
      .subscribe(
        response => {
            this.errorMessage = null;

            if (response) {
              if (this.permissionService.isAdmin()) {
                this.trigger.nativeElement.click();
                // window.location.href = '/admin';
              } else if (this.permissionService.isProprietario()) {
                window.location.href = '/';
                // this.router.navigate(['/admin/proprietario']);
                // window.location.href = '/admin/proprietario'; // Fix router
              } else {
                window.location.href = '/';
                // this.router.navigate(['/']);
                // todo: teste... window.location.href = '/'; // Fix router
              }

            } else {
              this.toasterService.pop('error', `Usuário ou a Senha estão incorretos`);
            }
            this.spinnerService.hide();
        },
        error => {
          this.errorMessage = <any>error;
          this.submitting = false;
          this.spinnerService.hide();
        }
      );
  }

  isFieldValid(field: string) {
    return (!this.loginform.get(field).valid && this.loginform.get(field).touched) ||
      (this.loginform.get(field).untouched) && this.submitting == false;
  }

  onModalClose() {
    $('.modal-backdrop').remove();
  }

  private createForm() {
    this.loginform = new FormGroup({
      login: this.login,
      password: this.password,
    });
  }

  private createFormControls() {
    this.login = new FormControl('', [
      Validators.required
    ]);
    this.password = new FormControl('', [
      Validators.required
    ]);
  }

}
