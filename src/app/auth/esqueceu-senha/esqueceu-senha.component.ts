import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { UserService } from '../../shared/services/user.service';
import { Router } from '@angular/router';
import { User } from '../../shared/models/user';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ApiService } from '../../shared/services/api.service';

@Component({
  selector: 'app-esqueceu-minha-senha',
  templateUrl: './esqueceu-senha.component.html',
  styleUrls: ['./esqueceu-senha.component.scss']
})
export class EsqueceuSenhaComponent implements OnInit {

  user: User;
  errorMessage: string;
  submitting: boolean;
  loginform: FormGroup;
  login: FormControl;
  password: FormControl;
  email: FormControl;

  constructor(
    private spinnerService: SpinnerService,
    private authService: AuthService,
    private apiService: ApiService,
    private router: Router
  ) { }

  ngOnInit() {
    this.createFormControls();
    this.createForm();
  }

  onSubmit() {
    this.submitting = true;

    this.spinnerService.show();

    this.apiService.post('/esqueci_minha_senha', {
      login: this.login.value
    })
      .subscribe(
        response => {
          if (response) {
            window.alert(`${this.login.value}, verifique sua caixa de entrada para recuperar sua senha.`);
          } else {
            window.alert(`Não foi encontrado nenhum e-mail para este login.`);
          }
          this.spinnerService.hide();
        },
        error => {
          this.errorMessage = <any>error;
          console.log(error);
          this.submitting = false;
          this.spinnerService.hide();
        }
      );
  }

  isFieldValid(field: string) {
    return (!this.loginform.get(field).valid && this.loginform.get(field).touched) ||
      (this.loginform.get(field).untouched) && this.submitting === false;
  }

  private createForm() {
    this.loginform = new FormGroup({
      login: this.login,
    });
  }

  private createFormControls() {
    /*this.login = new FormControl('', [
      Validators.required
    ]);
    this.password = new FormControl('', [
      Validators.required
    ]); */
    this.login = new FormControl('', [
      Validators.required
    ]);
  }

  back() {
    this.router.navigate(['/login']);
  }

}
