import { Component, OnInit, ViewChild, OnChanges } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { SaldoFinalAtualizado } from '../shared/models/saldo_final_atualizado';
import { Gerente } from '../shared/models/gerente';
import { PosicaoRecibos } from '../shared/models/posicao_recibos';
import 'chart.piecelabel.js';
import { PermissionsService } from '../shared/services/permissions.service';
import { Router } from '@angular/router';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    public saldoFinalAtualizado: SaldoFinalAtualizado;
    public gerente: Gerente;
    public posicaoRecibos: PosicaoRecibos;
    public fullYear: number;

    // Donut Chart Config
    public donutChartLabels: string[] = ['Boletos Não Pagos', 'Boletos Pagos'];
    public donutChartData: any;
    public donutChartType = 'doughnut';
    public donutChartLegend = true;
    public dountChartOptions: any = {
        pieceLabel: {
            render: function (args) {
              const label = args.label,
                    value = args.value;
              return label + ': ' + value;
        },
        chart: {
            type: 'pieChart',
            donut: true,
            labelThreshold: 0.01,
            showLabels: true,
            legend: {
                margin: {
                    top: 5,
                    right: 35,
                    bottom: 5,
                    left: 0
                }
            },
            pieceLabel: {
                render: 'label'
             }
        },
        cutoutPercentage: 75
    }
    };

    constructor(
        public permissionService: PermissionsService,
        private apiService: ApiService,
        private router: Router
    ) {
        this.posicaoRecibos = new PosicaoRecibos();
        this.posicaoRecibos.TotalPago = 0;
        this.posicaoRecibos.TotalNaoPago = 0;
        this.donutChartData = [0, 0];

        if (this.permissionService.isLocatario()) {
            this.router.navigate(['/dashboard/locatario']);
        }
    }

    ngOnInit() {
        this.apiService.get('/financeiro/saldo_final_atualizado', {
            month: new Date().getMonth() + 1,
            year: new Date().getFullYear()
        }).subscribe((result: any) => {
            this.saldoFinalAtualizado = result[0];
        });

        this.apiService.get('/condominio/dados_do_gerente').subscribe((result: any) => {
            this.gerente = result[0];
        });

        this.apiService.get('/financeiro/posicao_recibos_grafico', {
            month: new Date().getMonth() + 1,
            year: new Date().getFullYear()
        }).subscribe((result: any) => {
            // console.log(result);
            this.posicaoRecibos = result[0];
            this.donutChartData = [this.posicaoRecibos.QuantidadeNaoPago, this.posicaoRecibos.QuantidadePago];
        });

        this.fullYear = new Date().getFullYear();
    }

}
