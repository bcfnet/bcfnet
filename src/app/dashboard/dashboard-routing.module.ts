import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { LocatarioComponent } from './locatario/locatario.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'locatario', component: LocatarioComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
