import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { AuthService } from '../../auth/auth.service';
import { AccountService } from '../../shared/services/account.service';
import { UserService } from '../../shared/services/user.service';
import { User } from '../../shared/models/user';
import { CondominioService } from '../../shared/services/condominio.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { PermissionsService } from '../../shared/services/permissions.service';
import { Router } from '@angular/router';
import { DadosPerfil } from 'src/app/shared/models/dados_perfil';

@Component({
  selector: 'app-locatario',
  templateUrl: './locatario.component.html',
  styleUrls: ['./locatario.component.scss']
})
export class LocatarioComponent implements OnInit {

  user: User;
  account: any;

  constructor(
    private apiService: ApiService,
    private authService: AuthService,
    private accountService: AccountService,
    private userService: UserService,
    private condominioService: CondominioService,
    private spinnerService: SpinnerService,
    private permissionService: PermissionsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.account = this.accountService.get();

    this.apiService.get(`/cadastro/dados_perfil`, {CodigoCliente: this.account.codigo}).subscribe((result: DadosPerfil) => {
        if (result.NomeCliente) {
            this.account.NomeCompleto = result.NomeCliente;
        } else {
            this.account.NomeCompleto = this.account.condominio;
        }
    });
  }
}
