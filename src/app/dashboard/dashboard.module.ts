import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GraficosModule } from '../graficos/graficos.module';
import { SharedModule } from '../shared/shared.module';
import { LocatarioComponent } from './locatario/locatario.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { ChartsModule } from 'ng2-charts';
import { CalendarModule } from 'ap-angular2-fullcalendar';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GraficosModule,
    DashboardRoutingModule,
    ChartsModule,
    CalendarModule,
  ],
  declarations: [
    LocatarioComponent,
    DashboardComponent
  ]
})
export class DashboardModule { }
