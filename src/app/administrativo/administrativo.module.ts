import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdministrativoRoutingModule } from './administrativo-routing.module';
import { AtasDeAssembleiaComponent } from './atas-de-assembleia/atas-de-assembleia.component';
import { ConvocacaoDeAssembleiasComponent } from './convocacao-de-assembleias/convocacao-de-assembleias.component';

@NgModule({
  imports: [
    CommonModule,
    AdministrativoRoutingModule
  ],
  declarations: [AtasDeAssembleiaComponent, ConvocacaoDeAssembleiasComponent]
})
export class AdministrativoModule { }
