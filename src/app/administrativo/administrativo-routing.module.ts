import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AtasDeAssembleiaComponent } from './atas-de-assembleia/atas-de-assembleia.component';
import { ConvocacaoDeAssembleiasComponent } from './convocacao-de-assembleias/convocacao-de-assembleias.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'atas-de-assembleia',
  },
  {
    path: 'atas-de-assembleia',
    component: AtasDeAssembleiaComponent
  },
  {
    path: 'convocacao-de-assembleias',
    component: ConvocacaoDeAssembleiasComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdministrativoRoutingModule { }
