import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { AdministrativoService } from '../../shared/services/administrativo.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../shared/services/utils.service';

@Component({
  selector: 'app-atas-de-assembleia',
  templateUrl: './atas-de-assembleia.component.html',
  styleUrls: ['./atas-de-assembleia.component.scss'],
  providers: [AdministrativoService]
})
export class AtasDeAssembleiaComponent implements OnInit {
  public datas = [];
  public loading = true;

  constructor(
    private administrativoService: AdministrativoService,
    private spinnerService: SpinnerService,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.administrativoService.get('administrativo', 'Atas de Assembléia')
      .subscribe((data: any) => {
        this.datas = data
          .map((administrativo) => {
            administrativo.arvore = this.utils.convertToUTF8(administrativo.arvore.split('#')[2]);
            return administrativo;
          });
        this.spinnerService.hide();
        this.loading = false;
        // console.log(data);
    });
  }
}
