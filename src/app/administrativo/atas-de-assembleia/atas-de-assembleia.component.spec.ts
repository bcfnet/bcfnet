import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtasDeAssembleiaComponent } from './atas-de-assembleia.component';

describe('AtasDeAssembleiaComponent', () => {
  let component: AtasDeAssembleiaComponent;
  let fixture: ComponentFixture<AtasDeAssembleiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtasDeAssembleiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtasDeAssembleiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
