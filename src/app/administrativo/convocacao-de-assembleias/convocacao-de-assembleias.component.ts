import { Component, OnInit } from '@angular/core';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { AdministrativoService } from '../../shared/services/administrativo.service';
import { UtilsService } from '../../shared/services/utils.service';

@Component({
  selector: 'app-convocacao-de-assembleias',
  templateUrl: './convocacao-de-assembleias.component.html',
  styleUrls: ['./convocacao-de-assembleias.component.scss']
})
export class ConvocacaoDeAssembleiasComponent implements OnInit {

  public datas = [];
  public loading = true;

  constructor(
    private administrativoService: AdministrativoService,
    private spinnerService: SpinnerService,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.administrativoService.get('administrativo', 'Convocação de Assembléias')
      .subscribe((data: any) => {
        this.datas = data
          .map((administrativo) => {
            administrativo.arvore = this.utils.convertToUTF8(administrativo.arvore.split('#')[2]);
            return administrativo;
          });
        this.spinnerService.hide();
        this.loading = false;
        console.log(data);
    });
  }

}
