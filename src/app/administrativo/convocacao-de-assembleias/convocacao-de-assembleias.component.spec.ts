import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConvocacaoDeAssembleiasComponent } from './convocacao-de-assembleias.component';

describe('ConvocacaoDeAssembleiasComponent', () => {
  let component: ConvocacaoDeAssembleiasComponent;
  let fixture: ComponentFixture<ConvocacaoDeAssembleiasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConvocacaoDeAssembleiasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConvocacaoDeAssembleiasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
