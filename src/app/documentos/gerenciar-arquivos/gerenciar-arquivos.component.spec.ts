import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenciarArquivosComponent } from './gerenciar-arquivos.component';

describe('GerenciarArquivosComponent', () => {
  let component: GerenciarArquivosComponent;
  let fixture: ComponentFixture<GerenciarArquivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenciarArquivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenciarArquivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
