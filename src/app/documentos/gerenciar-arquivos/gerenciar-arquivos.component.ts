import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Arquivo } from '../../shared/models/arquivo';
import { Unidade } from '../../shared/models/unidade';
import { UserService } from '../../shared/services/user.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ArquivoService } from '../../shared/services/arquivo.service';
import { ToasterService } from 'angular2-toaster';
import { UtilsService } from '../../shared/services/utils.service';
import { PermissionsService } from '../../shared/services/permissions.service';

@Component({
  selector: 'app-gerenciar-arquivos',
  templateUrl: './gerenciar-arquivos.component.html',
  styleUrls: ['./gerenciar-arquivos.component.scss']
})
export class GerenciarArquivosComponent implements OnInit {

  errorMessage: string;
  submitting: boolean;
  arquivo: Arquivo;
  arquivos: Array<Arquivo[]> = [];
  unidades: Array<Unidade[]> = [];

  constructor(
    private apiService: ApiService,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    public userService: UserService,
    public arquivoService: ArquivoService,
    private utils: UtilsService,
    public permissionService: PermissionsService
  ) { }

  ngOnInit() {
    this.get();
  }

  onChange(login: string) {
    this.spinnerService.show();
    this.arquivoService.get(login)
      .subscribe((result: any) => {
        this.arquivos = result.map((administrativo) => {
          administrativo.arvore = this.utils.convertToUTF8(administrativo.arvore.split('#')[2]);
          return administrativo;
        });
        this.spinnerService.hide();
    });
  }

  delete(login: string, id: number) {
    const confirm = window.confirm('Você tem certeza disso?'); // todo: trocar por algo mais interessante...

    if (confirm) {
      this.spinnerService.show();
      this.arquivoService.delete(login, id)
        .subscribe((result: any) => {
          this.toasterService.pop('success', 'Parabéns!', `O arquivo foi deletado com sucesso.`);
          this.get();
        });
      this.spinnerService.hide();
    }
  }

  private get() {
    this.spinnerService.show();

    this.apiService.get('/cadastro/retorna_unidades')
      .subscribe((result: any) => {
        this.unidades = result;
      });

    this.arquivoService.get()
      .subscribe((result: any) => {
        this.arquivos = result.map((administrativo) => {
            administrativo.arvore = this.utils.convertToUTF8(administrativo.arvore.split('#')[2]);
          return administrativo;
        });
        console.log(this.arquivos);
        this.spinnerService.hide();
      });
  }

}
