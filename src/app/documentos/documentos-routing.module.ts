import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EnviarArquivosComponent } from './enviar-arquivos/enviar-arquivos.component';
import { GerenciarArquivosComponent } from './gerenciar-arquivos/gerenciar-arquivos.component';
import { AtualizarArquivoComponent } from './atualizar-arquivo/atualizar-arquivo.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'enviar-arquivos',
    pathMatch: 'full'
  },
  {
    path: 'enviar-arquivos',
    component: EnviarArquivosComponent
  },
  {
    path: 'gerenciar-arquivos',
    component: GerenciarArquivosComponent
  },
  {
    path: 'p/:id',
    component: AtualizarArquivoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentosRoutingModule { }
