import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Arquivo } from '../../shared/models/arquivo';
import { Unidade } from '../../shared/models/unidade';
import { ApiService } from '../../shared/services/api.service';
import { UserService } from '../../shared/services/user.service';
import { ArquivoService } from '../../shared/services/arquivo.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-enviar-arquivos',
  templateUrl: './enviar-arquivos.component.html',
  styleUrls: ['./enviar-arquivos.component.scss']
})
export class EnviarArquivosComponent implements OnInit {

  errorMessage: string;
  submitting: boolean;
  form: FormGroup;
  arquivo: Arquivo;
  upload_file;
  unidades: Array<Unidade[]> = [];

    constructor(
        public userService: UserService,
        public arquivoService: ArquivoService,
        private apiService: ApiService,
        private spinnerService: SpinnerService,
        private toasterService: ToasterService,
        private router: Router,
        private cd: ChangeDetectorRef
    ) {
        this.arquivo = new Arquivo();
    }

    ngOnInit() {
        this.createForm();
        this.get();
    }

    private createForm() {
        this.form = new FormGroup({
            pasta: new FormControl(null, Validators.required),
            tipo: new FormControl(null, Validators.required),
            data_expiracao: new FormControl(null, Validators.required),
            permissao: new FormControl(null, Validators.required),
            login: new FormControl(null, Validators.required),
            status: new FormControl(null, Validators.required),
            data_inicial: new FormControl(null),
            data_final: new FormControl(null),
            data_inclusao: new FormControl(null, Validators.required),
            descricao: new FormControl(null, Validators.required),
            link_arquivo: new FormControl(null)
        });
        this.form.controls['pasta'].setValue('Administrativo', {onlySelf: true});
        this.form.controls['tipo'].setValue('Acompanhamento de Processos', {onlySelf: true});
        this.form.controls['permissao'].setValue('P', {onlySelf: true});
        this.form.controls['login'].setValue(this.userService.getUserLogin(), {onlySelf: true});
        this.form.controls['status'].setValue('A', {onlySelf: true});
    }

    onFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);

            reader.onload = () => {
                // this.arquivo.link_arquivo = reader.result;
                this.form.controls['link_arquivo'].setValue(reader.result);

                this.cd.markForCheck();
            };
        }
    }

    formatDate(data: any) {
        return moment(new Date(data.year, data.month, data.day)).format('DD/MM/YYYY');
    }

    onSubmit() {
        this.submitting = false;

        if (this.form.valid) {
            this.spinnerService.show();
            this.arquivo = this.form.value;
            this.arquivo.data_expiracao = this.arquivo.data_expiracao ? this.formatDate(this.arquivo.data_expiracao) : null;
            this.arquivo.data_inicial = this.arquivo.data_inicial ? this.formatDate(this.arquivo.data_inicial) : null;
            this.arquivo.data_inclusao = this.arquivo.data_inclusao ? this.formatDate(this.arquivo.data_inclusao) : null;
            this.arquivo.data_final = this.arquivo.data_final ? this.formatDate(this.arquivo.data_final) : null;

        this.arquivoService.create(this.arquivo)
            .subscribe(
            (result) => {
                if (result.error == 0) {
                    this.toasterService.pop('success', 'Parabéns!', 'O documento cadastrado com sucesso.');
                    this.router.navigate(['/documentos/gerenciar-arquivos']);
                } else {
                    this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
                }
                this.spinnerService.hide();
            },
            (err) => {
                console.log(`Error: ${err}`);
                this.spinnerService.hide();
                this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
            }
            );
        } else {
            this.toasterService.pop('error', 'Atenção!', 'Há erros de validação no formulário.');
        }
    }

  isFieldValid(field: string) {
    return;
    /*return (!this.form.get(field).valid && this.form.get(field).touched) ||
      (this.form.get(field).untouched) && this.submitting === false; */
  }

  private get() {
    this.apiService.get('/cadastro/retorna_unidades')
      .subscribe((result: any) => {
        this.unidades = result;
      });
  }
}
