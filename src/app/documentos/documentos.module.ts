import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentosRoutingModule } from './documentos-routing.module';
import { EnviarArquivosComponent } from './enviar-arquivos/enviar-arquivos.component';
import { GerenciarArquivosComponent } from './gerenciar-arquivos/gerenciar-arquivos.component';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { FlatpickrModule } from 'angularx-flatpickr';
import { NgbDateMomentParserFormatter } from '../shared/extends/ngb-datemoment-parser-formatter';
import { AtualizarArquivoComponent } from './atualizar-arquivo/atualizar-arquivo.component';

@NgModule({
  imports: [
    CommonModule,
    DocumentosRoutingModule,
    NgbModule.forRoot(),
    FlatpickrModule.forRoot(),
    SharedModule
  ],
  declarations: [EnviarArquivosComponent, GerenciarArquivosComponent, AtualizarArquivoComponent],
  providers: [
    {
      provide: NgbDateParserFormatter,
      useFactory: () => {
        return new NgbDateMomentParserFormatter('DD/MM/YYYY');
      }
    }
  ]
})
export class DocumentosModule { }
