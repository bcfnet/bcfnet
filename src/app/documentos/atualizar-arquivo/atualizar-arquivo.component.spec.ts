import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarArquivoComponent } from './atualizar-arquivo.component';

describe('AtualizarArquivoComponent', () => {
  let component: AtualizarArquivoComponent;
  let fixture: ComponentFixture<AtualizarArquivoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtualizarArquivoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarArquivoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
