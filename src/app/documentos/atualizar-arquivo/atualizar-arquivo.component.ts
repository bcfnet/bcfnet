import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { UserService } from '../../shared/services/user.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Arquivo } from '../../shared/models/arquivo';
import { Unidade } from '../../shared/models/unidade';
import { ArquivoService } from '../../shared/services/arquivo.service';
import { ApiService } from '../../shared/services/api.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ToasterService } from 'angular2-toaster';
import { Router, ActivatedRoute } from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-atualizar-arquivo',
  templateUrl: './atualizar-arquivo.component.html',
  styleUrls: ['./atualizar-arquivo.component.scss']
})
export class AtualizarArquivoComponent implements OnInit {

    errorMessage: string;
    submitting: boolean;
    form: FormGroup;
    arquivo: Arquivo;
    upload_file;
    unidades: Array<Unidade[]> = [];
    private id;

    constructor(
        public userService: UserService,
        public arquivoService: ArquivoService,
        private apiService: ApiService,
        private spinnerService: SpinnerService,
        private toasterService: ToasterService,
        private router: Router,
        private cd: ChangeDetectorRef,
        private route: ActivatedRoute
    ) {
        this.arquivo = new Arquivo();
    }
  
    ngOnInit() {
        this.createForm();
        this.get();
    }
  
    private createForm() {
        this.form = new FormGroup({
            pasta: new FormControl(null, Validators.required),
            tipo: new FormControl(null, Validators.required),
            data_expiracao: new FormControl(null, Validators.required),
            permissao: new FormControl(null, Validators.required),
            login: new FormControl(null, Validators.required),
            status: new FormControl(null, Validators.required),
            data_inicial: new FormControl(null),
            data_final: new FormControl(null),
            data_inclusao: new FormControl(null, Validators.required),
            descricao: new FormControl(null, Validators.required),
            link_arquivo: new FormControl(null)
        });

        this.route.params.subscribe(params => {
            this.id = params['id'];

            this.arquivoService.get(this.userService.getUserLogin(), this.id)
                .subscribe((arquivo: any) => {
                    if (arquivo && arquivo.length > 0) {
                        this.arquivo = arquivo[0];

                        let pasta = 'Administrativo';
                        let arvore;

                        if (this.arquivo.arvore) {
                            arvore = this.arquivo.arvore.split('#');
                            pasta = arvore[0];
                        }

                        this.form.controls['pasta'].setValue(pasta);
                        this.form.controls['descricao'].setValue(arvore[2]);
                        this.form.controls['tipo'].setValue(arvore[1]);
                        this.form.controls['permissao'].setValue(this.arquivo.permissao);
                        this.form.controls['login'].setValue(this.userService.getUserLogin());
                        this.form.controls['data_expiracao'].setValue(this.unformatDate(this.arquivo.data_expiracao));
                        this.form.controls['data_inicial'].setValue(this.unformatDate(this.arquivo.data_inicial));
                        this.form.controls['data_final'].setValue(this.unformatDate(this.arquivo.data_final));
                        this.form.controls['data_inclusao'].setValue(this.unformatDate(this.arquivo.data_inclusao));
                        this.form.controls['link_arquivo'].setValue(this.arquivo.link_arquivo);
                        this.form.controls['status'].setValue(this.arquivo.status);
                    } else {
                        this.router.navigate(['/documentos']);
                        this.toasterService.pop('error', 'Atenção!', 'Você não tem permissão para visualizar este arquivo');
                    }
                });
        });
    }
  
    onFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);

            reader.onload = () => {
                this.form.controls['link_arquivo'].setValue(reader.result);

                this.cd.markForCheck();
            };
        }
    }
  
    formatDate(data: any) {
        return moment(new Date(data.year, data.month, data.day)).format('DD/MM/YYYY');
    }
  
    onSubmit() {
        this.submitting = false;

        if (this.form.valid) {
            this.spinnerService.show();
            this.arquivo = this.form.value;
            this.arquivo.data_expiracao = this.arquivo.data_expiracao ? this.formatDate(this.arquivo.data_expiracao) : null;
            this.arquivo.data_inicial = this.arquivo.data_inicial ? this.formatDate(this.arquivo.data_inicial) : null;
            this.arquivo.data_inclusao = this.arquivo.data_inclusao ? this.formatDate(this.arquivo.data_inclusao) : null;
            this.arquivo.data_final = this.arquivo.data_final ? this.formatDate(this.arquivo.data_final) : null;

            this.arquivoService.update(this.id, this.arquivo)
                .subscribe(
                    (result) => {
                        if (result.error == 0) {
                            this.toasterService
                                .pop('success', 'Parabéns!', 'O documento foi atualizado com sucesso.');
                            this.router.navigate(['/documentos/gerenciar-arquivos']);
                        } else {
                            this.toasterService
                                .pop('error', 'Atenção!', 'Há um error durante a tentativa de atualização. Por favor, refaça.');
                        }
                        this.spinnerService.hide();
                    },
                    (err) => {
                        console.log(`Error: ${err}`);
                        this.spinnerService.hide();
                        this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
                    }
                );
            } else {
                this.toasterService.pop('error', 'Atenção!', 'Há erros de validação no formulário.');
            }
    }

    isFieldValid(field: string) {
      return;
      /*return (!this.form.get(field).valid && this.form.get(field).touched) ||
        (this.form.get(field).untouched) && this.submitting === false; */
    }

    private unformatDate(date) {
        if (!date || date === null) {
            return '';
        }

        const $date = date.split('/');

        return {
            year: parseInt($date[2], 10),
            month: parseInt($date[1], 10),
            day: parseInt($date[0], 10)
        };
    }

    private get() {
      this.apiService.get('/cadastro/retorna_unidades')
        .subscribe((result: any) => {
          this.unidades = result;
        });
    }
}
