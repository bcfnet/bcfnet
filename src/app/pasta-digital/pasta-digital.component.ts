import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { UtilsService } from '../shared/services/utils.service';
import { ModalService } from '../shared/components/modal/modal.service';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-pasta-digital',
  templateUrl: './pasta-digital.component.html',
  styleUrls: ['./pasta-digital.component.scss']
})
export class PastaDigitalComponent implements OnInit {

    public url: any;
    public data;
    public periodo;
    public loading = true;
    public identifier;
    private url_position;
    private urls = [];
    private base = 'https://www.bcfadm.com.br/bcfnet/pageflipper/?url=';

    constructor(
        public utils: UtilsService,
        private apiService: ApiService,
        private spinnerService: SpinnerService,
        private modalService: ModalService,
        private sanitizer: DomSanitizer
    ) {
        this.data = [];
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.base);
    }

    ngOnInit() {
        this.get();
    }

    open(balancete) {
        if (!balancete.URL) {
            return;
        }
        this.loading = true;

        let url = balancete.URL;
        url = this.base + this.utils.base64Encode(url);
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url);

        setTimeout(() => {
            this.url_position = this.urls.findIndex((value) => {
                return value.url === url;
            });
            this.loading = false;
        }, 300);
    }

    next() {
        const limit = this.urls.length - 1;

        if (this.url_position < limit) {
            this.url_position++;
        } else {
            this.url_position = 0;
        }
        
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urls[this.url_position].url);
    }

    previous() {
        const limit = this.urls.length - 1;

        if (this.url_position != 0) {
            this.url_position--;            
        } else {
            this.url_position = limit;            
        }
        
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urls[this.url_position].url);
    }

    get() {
        this.spinnerService.show();

        this.apiService.get('/financeiro/pasta_digital', {
            month: this.periodo ? this.periodo[0] : new Date().getMonth() + 1,
            year: this.periodo ? this.periodo[1] : new Date().getFullYear()
        }).subscribe((result: any) => {
            this.data = result.map((data) => {
                if (data.URL) {
                    const url: any = this.base + this.utils.base64Encode(data.URL);
                    this.urls.push({
                        title: data.Campo2,
                        url: url
                    });
                }
                return data;
            });
            this.spinnerService.hide();
        });
    }

    getModalTitle() {
        const balancete = this.urls[this.url_position];

        if (balancete) {
            return balancete.title;
        }
        return;
    }

    onChange(periodo) {
        this.periodo = periodo.split('/');
        this.get();
    }

}
