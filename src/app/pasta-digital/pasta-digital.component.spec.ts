import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastaDigitalComponent } from './pasta-digital.component';

describe('PastaDigitalComponent', () => {
  let component: PastaDigitalComponent;
  let fixture: ComponentFixture<PastaDigitalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastaDigitalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastaDigitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
