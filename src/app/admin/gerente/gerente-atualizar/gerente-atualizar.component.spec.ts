import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenteAtualizarComponent } from './gerente-atualizar.component';

describe('GerenteAtualizarComponent', () => {
  let component: GerenteAtualizarComponent;
  let fixture: ComponentFixture<GerenteAtualizarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenteAtualizarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenteAtualizarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
