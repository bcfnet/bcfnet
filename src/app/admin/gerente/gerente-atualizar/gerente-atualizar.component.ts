import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GerenteService } from '../../../shared/services/gerente.service';
import { CondominioService } from '../../../shared/services/condominio.service';
import { ToasterService } from 'angular2-toaster';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
declare var require: any;
const unserialize = require('locutus/php/var/unserialize');

@Component({
  selector: 'app-gerente-atualizar',
  templateUrl: './gerente-atualizar.component.html',
  styleUrls: ['./gerente-atualizar.component.scss']
})
export class GerenteAtualizarComponent implements OnInit {

  errorMessage: string;
  submitting: boolean;
  form: FormGroup;
  gerente;
  selectedCondominios = [];
  condominios = [];
  singleSelectOptions: any = [];

  singleSelectConfig: any = {
      labelField: 'label',
      valueField: 'value',
      searchField: ['label', 'code']
  };

  singleSelectValue: string[] = [];
  private id: number;

  constructor(
    private gerenteService: GerenteService,
    private condominioService: CondominioService,
    private toasterService: ToasterService,
    private spinnerService: SpinnerService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.get();
  }

  onValueChange(number: string) {
    this.form.value.clientes.push(number);
    const index = this.condominios.indexOf(number);
    if (index !== -1) {
      this.condominios.splice(index, 1);
    }
  }

  remove(id: number) {
    this.form.value.clientes.splice(id, 1);
  }

  onSubmit() {
    if (this.form.valid) {
      this.spinnerService.show();
      this.gerente = this.form.value;

      this.gerenteService.update(this.id, this.gerente)
        .subscribe(
          (result) => {
            if (result.error === '0') {
              this.toasterService.pop('success', 'Parabéns!', 'O Gerente foi atualizado com sucesso.');

              this.router.navigate(['/admin/gerentes/']);
            } else {
              this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
            }
            this.spinnerService.hide();
          },
          (err) => {
            console.log(`Error: ${err}`);
            this.spinnerService.hide();
            this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
          }
        );
    } else {
      this.toasterService.pop('error', 'Atenção!', 'Há erros de validação no formulário.');
    }

  }

  private createForm() {
    this.form = new FormGroup({
        nome: new FormControl(null, Validators.required),
        endereco: new FormControl(null),
        tipo_endereco: new FormControl(null),
        complemento: new FormControl(null),
        bairro: new FormControl(null),
        municipio: new FormControl(),
        estado: new FormControl(),
        cep: new FormControl(),
        email: new FormControl(),
        telefone_residencial: new FormControl(),
        telefone_comercial: new FormControl(),
        telefone_movel: new FormControl(),
        fax: new FormControl(),
        identificacao: new FormControl(),
        login_gerente: new FormControl(),
        senha: new FormControl(),
        clientes: new FormControl([])
    });

    this.form.controls['estado'].setValue('rj', {onlySelf: true});
    this.form.controls['tipo_endereco'].setValue('comercial', {onlySelf: true});
  }

  private get() {

    this.id = this.route.snapshot.params.id;

    this.gerenteService.get(this.id)
      .subscribe((result: any) => {
        if (result.length > 0) {
          this.gerente = result[0];

          this.form.controls['nome'].setValue(this.gerente.nome, {onlySelf: true});
          this.form.controls['endereco'].setValue(this.gerente.endereco, {onlySelf: false});
          this.form.controls['tipo_endereco'].setValue(this.gerente.tipo_endereco, {onlySelf: true});
          this.form.controls['complemento'].setValue(this.gerente.complemento, {onlySelf: true});
          this.form.controls['bairro'].setValue(this.gerente.bairro, {onlySelf: true});
          this.form.controls['municipio'].setValue(this.gerente.municipio, {onlySelf: true});
          this.form.controls['estado'].setValue(this.gerente.estado, {onlySelf: true});
          this.form.controls['cep'].setValue(this.gerente.cep, {onlySelf: true});
          this.form.controls['email'].setValue(this.gerente.email, {onlySelf: true});
          this.form.controls['telefone_residencial'].setValue(this.gerente.telefone_residencial, {onlySelf: true});
          this.form.controls['telefone_comercial'].setValue(this.gerente.telefone_comercial, {onlySelf: true});
          this.form.controls['telefone_movel'].setValue(this.gerente.telefone_movel, {onlySelf: true});
          this.form.controls['fax'].setValue(this.gerente.fax, {onlySelf: true});
          this.form.controls['identificacao'].setValue(this.gerente.identificacao, {onlySelf: true});
          this.form.controls['clientes'].setValue(unserialize(this.gerente.clientes), {onlySelf: true});

          if (this.gerente.senha.length !== 0) {
            this.form.controls['senha'].setValue(this.gerente.senha, {onlySelf: true});
          } else {
            this.form.controls['senha'].setValue(this.gerente.senha, {onlySelf: true});
          }

          if (this.gerente.login.length !== 0) {
            this.form.controls['login_gerente'].setValue(this.gerente.login, {onlySelf: true});
          } else {
            this.form.controls['login_gerente'].setValue(this.generateLogin(), {onlySelf: true});
          }
        } else {
          this.router.navigate(['/admin/gerentes']);
        }
    });

    this.condominioService.get()
      .subscribe((condominios: any) => {
        if (condominios.length > 0) {
          this.singleSelectValue = condominios[0].login_internet;
          for (const condominio of condominios) {
            this.condominios.push(condominio.login_internet);
            this.singleSelectOptions.push({
              label: `${condominio.login_internet} - ${condominio.nome}`,
              value: condominio.login_internet,
              code: condominio.login_internet
            });
          }
        }
      });
  }

  private generateLogin() {
    let text = '';
    const possible = '0123456789';

    for (let i = 0; i < 4; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return ['5', text].join('');
  }

}
