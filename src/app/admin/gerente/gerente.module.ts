import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GerenteRoutingModule } from './gerente-routing.module';
import { GerenteCadastrarComponent } from './gerente-cadastrar/gerente-cadastrar.component';
import { GerenteListarComponent } from './gerente-listar/gerente-listar.component';
import { SharedModule } from '../../shared/shared.module';
import { GerenteAtualizarComponent } from './gerente-atualizar/gerente-atualizar.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    GerenteRoutingModule
  ],
  declarations: [GerenteCadastrarComponent, GerenteListarComponent, GerenteAtualizarComponent]
})
export class GerenteModule { }
