import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GerenteListarComponent } from './gerente-listar/gerente-listar.component';
import { GerenteCadastrarComponent } from './gerente-cadastrar/gerente-cadastrar.component';
import { GerenteAtualizarComponent } from './gerente-atualizar/gerente-atualizar.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: GerenteListarComponent },
      { path: 'cadastrar', component: GerenteCadastrarComponent },
      { path: 'atualizar/:id', component: GerenteAtualizarComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GerenteRoutingModule { }
