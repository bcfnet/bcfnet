import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { GerenteService } from '../../../shared/services/gerente.service';
import { CondominioService } from '../../../shared/services/condominio.service';
import { ToasterService } from 'angular2-toaster';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';

@Component({
  selector: 'app-gerente-cadastrar',
  templateUrl: './gerente-cadastrar.component.html',
  styleUrls: ['./gerente-cadastrar.component.scss']
})
export class GerenteCadastrarComponent implements OnInit {

  errorMessage: string;
  submitting: boolean;
  form: FormGroup;
  gerente;
  selectedCondominios = [];
  condominios = [];
  singleSelectOptions: any = [];

  singleSelectConfig: any = {
      labelField: 'label',
      valueField: 'value',
      searchField: ['label', 'code']
  };

  singleSelectValue: string[] = ['10001'];

  constructor(
    private gerenteService: GerenteService,
    private condominioService: CondominioService,
    private toasterService: ToasterService,
    private spinnerService: SpinnerService,
    private router: Router
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.get();
  }

  onValueChange(number: string) {
    this.form.value.clientes.push(number);
    const index = this.condominios.indexOf(number);
    if (index !== -1) {
      this.condominios.splice(index, 1);
    }
  }

  remove(id: number) {
    this.form.value.clientes.splice(id, 1);
  }

  onSubmit() {
    if (this.form.valid) {
      this.spinnerService.show();
      this.gerente = this.form.value;
      this.spinnerService.hide();

      this.gerenteService.create(this.gerente)
        .subscribe(
          (result) => {
            if (result.error === '0') {
              this.toasterService.pop('success', 'Parabéns!', 'O Gerente foi cadastrado com sucesso.');

              this.router.navigate(['/admin/gerentes/']);
            } else {
              this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
            }
            this.spinnerService.hide();
          },
          (err) => {
            console.log(`Error: ${err}`);
            this.spinnerService.hide();
            this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
          }
        );
    } else {
      this.toasterService.pop('error', 'Atenção!', 'Há erros de validação no formulário.');
    }

  }

  private createForm() {
    this.form = new FormGroup({
        nome: new FormControl(null, Validators.required),
        endereco: new FormControl(null),
        tipo_endereco: new FormControl(null),
        complemento: new FormControl(null),
        bairro: new FormControl(null),
        municipio: new FormControl(),
        estado: new FormControl(),
        cep: new FormControl(),
        email: new FormControl(),
        telefone_residencial: new FormControl(),
        telefone_comercial: new FormControl(),
        telefone_movel: new FormControl(),
        fax: new FormControl(),
        identificacao: new FormControl(),
        login_gerente: new FormControl(),
        senha: new FormControl(),
        clientes: new FormControl([])
    });

    this.form.controls['estado'].setValue('rj', {onlySelf: true});
    this.form.controls['tipo_endereco'].setValue('comercial', {onlySelf: true});
    this.form.controls['login_gerente'].setValue(this.generateLogin(), {onlySelf: true});
  }

  private get() {
    this.condominioService.get()
      .subscribe((condominios: any) => {
        if (condominios.length > 0) {
          this.singleSelectValue = condominios[0].login_internet;
          for (const condominio of condominios) {
            this.condominios.push(condominio.login_internet);
            this.singleSelectOptions.push({
              label: `${condominio.login_internet} - ${condominio.nome}`,
              value: condominio.login_internet,
              code: condominio.login_internet
            });
          }
        }
      });
  }

  private generateLogin() {
    let text = '';
    const possible = '0123456789';

    for (let i = 0; i < 4; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return ['5', text].join('');
  }

}
