import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenteCadastrarComponent } from './gerente-cadastrar.component';

describe('GerenteCadastrarComponent', () => {
  let component: GerenteCadastrarComponent;
  let fixture: ComponentFixture<GerenteCadastrarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenteCadastrarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenteCadastrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
