import { Component, OnInit } from '@angular/core';
import { GerenteService } from '../../../shared/services/gerente.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { ToasterService } from '../../../../../node_modules/angular2-toaster';
import { ModalService } from '../../../shared/components/modal/modal.service';
import { UtilsService } from '../../../shared/services/utils.service';
declare var require: any;
const unserialize = require('locutus/php/var/unserialize');

@Component({
  selector: 'app-gerente-listar',
  templateUrl: './gerente-listar.component.html',
  styleUrls: ['./gerente-listar.component.scss']
})
export class GerenteListarComponent implements OnInit {

  public gerentes = [];

  constructor(
    private gerenteService: GerenteService,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private modalService: ModalService,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.get();
  }

  open(gerente) {
    this.modalService.show(this.utils.convertToUTF8(gerente.nome), this.utils.convertToUTF8(gerente.clientes));
  }

  get() {
    this.spinnerService.show();

    this.gerenteService.get()
      .subscribe((gerentes: any) => {
        this.gerentes = gerentes.map((gerente) => {
          if (gerente.clientes != "") {
            gerente.clientes = unserialize(gerente.clientes);
          }
          gerente.nome = this.utils.convertToUTF8(gerente.nome);
          // console.log(gerente.clientes);
          // gerente.clientes = unserialize(gerente.clientes);
          return gerente;
        });
        this.spinnerService.hide();
    });
  }

  delete(id: number) {
    const confirm = window.confirm(`Você tem certeza que deseja deletar o gerente ${id}?`);

    if (confirm) {
      this.spinnerService.show();
      this.gerenteService.delete(id)
        .subscribe(result => {
          if (result.error == 0) {
            this.toasterService.pop('success', 'Parabéns!', 'O gerente foi apagado com sucesso.');
            this.get();
          } else {
            this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
          }
          this.spinnerService.hide();
        });
    }
  }

}
