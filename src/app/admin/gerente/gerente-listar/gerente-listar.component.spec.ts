import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GerenteListarComponent } from './gerente-listar.component';

describe('GerenteListarComponent', () => {
  let component: GerenteListarComponent;
  let fixture: ComponentFixture<GerenteListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GerenteListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GerenteListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
