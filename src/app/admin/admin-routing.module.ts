import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CondominioComponent } from './condominio/condominio.component';
import { CondominoListarComponent } from './condominio/condomino-listar/condomino-listar.component';
import { ProprietarioComponent } from './proprietario/proprietario.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'condominio'
  },
  {
    path: 'condominio',
    component: CondominioComponent
  },
  {
    path: 'condominio/:id',
    component: CondominoListarComponent
  },
  {
    path: 'gerentes',
    loadChildren: './gerente/gerente.module#GerenteModule'
  },
  {
    path: 'proprietario',
    component: ProprietarioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
