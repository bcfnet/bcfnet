import { Component, OnInit } from '@angular/core';
import { Unidade } from '../../../shared/models/unidade';
import { UserService } from '../../../shared/services/user.service';
import { ApiService } from '../../../shared/services/api.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-condomino-listar',
  templateUrl: './condomino-listar.component.html',
  styleUrls: ['./condomino-listar.component.scss']
})
export class CondominoListarComponent implements OnInit {

  unidade: Unidade;
  unidades: Array<Unidade[]>;

  constructor(
    public userService: UserService,
    private apiService: ApiService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    const login: string = this.route.snapshot.params.id;

    this.apiService.get('/cadastro/retorna_unidades', { login: login })
      .subscribe((result: any) => {
        this.unidades = result;
        this.unidade = result[0];
        // console.log(result);
      });
  }

  goBack() {
    this.router.navigate(['/admin/condominio']);
  }

}
