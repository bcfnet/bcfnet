import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CondominoListarComponent } from './condomino-listar.component';

describe('CondominoListarComponent', () => {
  let component: CondominoListarComponent;
  let fixture: ComponentFixture<CondominoListarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CondominoListarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CondominoListarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
