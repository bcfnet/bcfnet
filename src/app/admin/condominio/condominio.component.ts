import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CondominioService } from '../../shared/services/condominio.service';
import { ApiService } from '../../shared/services/api.service';
import { UserService } from '../../shared/services/user.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { AuthService } from '../../auth/auth.service';
import { User } from '../../shared/models/user';
import { PermissionsService } from '../../shared/services/permissions.service';
import { UtilsService } from '../../shared/services/utils.service';

@Component({
  selector: 'app-condominio',
  templateUrl: './condominio.component.html',
  styleUrls: ['./condominio.component.scss']
})
export class CondominioComponent implements OnInit {

  user: User;
  condominios = [];
  singleSelectOptions: any = [];

  singleSelectConfig: any = {
      labelField: 'label',
      valueField: 'value',
      searchField: ['label', 'code']
  };

  singleSelectValue: string[] = ['10001'];

  constructor(
    public permissionService: PermissionsService,
    private apiService: ApiService,
    private authService: AuthService,
    private userService: UserService,
    private condominioService: CondominioService,
    private spinnerService: SpinnerService,
    private router: Router,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.get();
  }

  open() {
    const login_internet =  this.singleSelectValue.length == 1 ? this.singleSelectValue[0] : this.singleSelectValue;
    this.router.navigate([`/admin/condominio/${login_internet}`]);
  }

  swap() {
    const confirm = window.confirm(`Você tem certeza que deseja entrar com o condomínio ${this.singleSelectValue} ?`);

    if (confirm && this.shield()) {
      const login = this.userService.getUserLogin();
      const login_internet =  this.singleSelectValue.length == 1 ? this.singleSelectValue[0] : this.singleSelectValue;

      this.spinnerService.show();

      this.apiService.post('/swap', {
        login: login,
        login_internet: login_internet,
        status: 'S'
      })
      .subscribe((result) => {

        if (result.success == '1') {
          this.user = new User();
          this.user.login = login_internet;
          this.user.password = result.senha;

          this.authService.login(this.user)
            .subscribe(
              response => {
                this.authService.swapOn(login, login_internet);
                this.spinnerService.hide();
                  if (response) {
                    window.location.href = '/'; // Fix router
                  }
                },
              error => {
                console.log(`Error: ${error}`);
                this.spinnerService.hide();
              }
            );
        }
      });
    }
  }

  private get() {
    this.condominioService.get()
      .subscribe((condominios: any) => {
            condominios = condominios.sort(this.condominioService.compare);
            condominios.shift();

            if (condominios.length > 0) {
                this.singleSelectValue = condominios[0].login_internet;
                for (const condominio of condominios) {
                    this.condominios.push(condominio.login_internet);
                    this.singleSelectOptions.push({
                        label: `${condominio.login_internet} - ${condominio.nome}`,
                        value: condominio.login_internet,
                        code: condominio.login_internet
                    });
                }
            }
      });
  }

  private shield() {
    /*if (!this.permissionService.isAdmin()) {
      return true;
    } else {
      //if () {}
      console.log(this.singleSelectValue);
    }
    return false; */
    return true;
  }

}
