import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { CondominioComponent } from './condominio/condominio.component';
import { SharedModule } from '../shared/shared.module';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateMomentParserFormatter } from '../shared/extends/ngb-datemoment-parser-formatter';
import { CondominoListarComponent } from './condominio/condomino-listar/condomino-listar.component';
import { ProprietarioComponent } from './proprietario/proprietario.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule,
    NgbModule.forRoot(),
  ],
  declarations: [
    CondominioComponent,
    CondominoListarComponent,
    ProprietarioComponent
  ],
  providers: [
    {
      provide: NgbDateParserFormatter,
      useFactory: () => {
        return new NgbDateMomentParserFormatter('DD/MM/YYYY');
      }
    }
  ]
})
export class AdminModule { }
