import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StickyModule } from 'ng2-sticky-kit';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';
import { NgxMaskModule } from 'ngx-mask';
import { ScrollToModule } from 'ng2-scroll-to';
import { PerfilRoutingModule } from './perfil-routing.module';
import { PerfilComponent } from './perfil.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PerfilRoutingModule,
        StickyModule,
        AngularStickyThingsModule,
        NgxMaskModule.forRoot(),
        ScrollToModule.forRoot()
    ],
    declarations: [
        PerfilComponent
    ]
})
export class PerfilModule { }
