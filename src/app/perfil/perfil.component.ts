import { Component, OnInit } from '@angular/core';
import { ApiService } from '../shared/services/api.service';
import { AccountService } from '../shared/services/account.service';
import { DadosPerfil } from '../shared/models/dados_perfil';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import { Router } from '@angular/router';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { FormasPagamento } from '../shared/models/formas_pagamento';
import { PermissionsService } from '../shared/services/permissions.service';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {

    dados_perfil_form: FormGroup;
    formas_pagamento_form: FormGroup;
    dados_perfil: DadosPerfil;
    formas_pagamento: FormasPagamento;
    accept = false;

    constructor(
        public permissionService: PermissionsService,
        private apiService: ApiService,
        private accountService: AccountService,
        private toasterService: ToasterService,
        private router: Router,
        private spinnerService: SpinnerService,
        private userService: UserService
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.get();
    }

    onSubmit() {
        const account = this.accountService.get();

        if (this.dados_perfil_form.valid) {
            const dados_perfil: DadosPerfil = this.dados_perfil_form.value;
            dados_perfil.CodigoCliente = this.userService.getUserLogin();

            this.spinnerService.show();

            this.apiService.post(`/cadastro/dados_perfil/update`, dados_perfil).subscribe(
                (result: any) => {
                    if (result.error === '0') {
                        this.toasterService.pop('success', 'Parabéns!', 'O Perfil foi atualizado com sucesso.');

                        setTimeout(() => {
                            window.location.reload();
                        }, 500);
                    } else {
                        this.toasterService.pop('error', 'Atenção!',
                            'Há um error durante a tentativa de atualização do perfil. Por favor, refaça.');
                    }
                    this.spinnerService.hide();
                },
                (err) => {
                    console.log(`Error: ${err}`);
                    this.spinnerService.hide();
                    this.toasterService.pop('error', 'Atenção!', 
                        'Há um error durante a tentativa de atualização do perfil. Por favor, refaça.');
                }
            );
        } else {
            this.toasterService.pop('error', 'Atenção!',
                        'Por favor, preencha todos os campos necessários. O campo Nome e CPF ou CNPJ é obrigatório.');
        }
    }

    getBancoPorDigito(digito) {
        let banco = '';

        switch(digito) {
            case '237':
                banco = 'Bradesco';
            break;
            case '341':
                banco = 'Itaú';
            break;
            case '033':
                banco = 'Santander';
            break;
        }
        return banco;
    }

    onSubmitFormasPagamento() {
        if (this.accept === false) {
            this.toasterService.pop('error', 'Atenção!',
                        'Você precisa aceitar os termos de autorização de débito automático para prosseguir.');
            return;
        }

        const account = this.accountService.get();
        const dados_perfil: DadosPerfil = this.dados_perfil_form.value;
        this.formas_pagamento_form.value['CodigoCliente'] = account.codigo;
        this.formas_pagamento_form.value['NomeCliente'] = dados_perfil.NomeCliente;
        this.formas_pagamento_form.value['CPF_CNPJ'] = dados_perfil.CPF_CNPJ;
        const formas_pagamento: FormasPagamento = this.formas_pagamento_form.value;

        if (this.formas_pagamento_form.valid) {
            formas_pagamento.Banco = this.getBancoPorDigito(formas_pagamento.Banco_Digito);

            this.spinnerService.show();

            this.apiService.post(`/cadastro/formas_pagamento/update`, formas_pagamento).subscribe(
                (result: any) => {
                    if (result.error === '0') {
                        this.toasterService.pop('success', 'Parabéns!', 'A Forma de Pagamento foi atualizada com sucesso.');
                    } else {
                        this.toasterService.pop('error', 'Atenção!',
                            'Há um error durante a tentativa de atualização do perfil. Por favor, refaça.');
                    }
                    this.spinnerService.hide();
                },
                (err) => {
                    console.log(`Error: ${err}`);
                    this.spinnerService.hide();
                    this.toasterService.pop('error', 'Atenção!',
                        'Há um error durante a tentativa de atualização do perfil. Por favor, refaça.');
                }
            );
        } else {
            this.toasterService.pop('error', 'Atenção!',
                        'Por favor, preencha todos os campos necessários. O campo CPF ou CNPJ precisa ser obrigatório.');
        }
    }

    onAcceptTerms() {
        this.accept = !this.accept;
    }

    private get() {
        const account = this.accountService.get();

        this.spinnerService.show();

        this.apiService.get(`/cadastro/dados_perfil`, {CodigoCliente: this.userService.getUserLogin()}).subscribe((result: DadosPerfil) => {
            this.dados_perfil = result;

            this.dados_perfil_form.controls['NomeCliente']
                .setValue(this.dados_perfil.NomeCliente == 'null' ? '' : this.dados_perfil.NomeCliente);
            this.dados_perfil_form.controls['CPF_CNPJ'].setValue(this.dados_perfil.CPF_CNPJ == 'null' || this.dados_perfil.CPF_CNPJ == 'undefined' ? '' : this.dados_perfil.CPF_CNPJ);
            this.dados_perfil_form.controls['E_Mail'].setValue(this.dados_perfil.E_Mail == 'null' || this.dados_perfil.E_Mail == 'undefined' ? '' : this.dados_perfil.E_Mail);
            this.dados_perfil_form.controls['Telefone'].setValue(this.dados_perfil.Telefone == 'null' || this.dados_perfil.Telefone == 'undefined' ? '' : this.dados_perfil.Telefone);
            this.dados_perfil_form.controls['Celular'].setValue(this.dados_perfil.Celular == 'null' || this.dados_perfil.Celular == 'undefined' ? '' : this.dados_perfil.Celular);
            this.dados_perfil_form.controls['CEP'].setValue(this.dados_perfil.CEP == 'null' || this.dados_perfil.CEP == 'undefined' ? '' : this.dados_perfil.CEP);
            this.dados_perfil_form.controls['Enderecos'].setValue(this.dados_perfil.Enderecos == 'null' || this.dados_perfil.Enderecos == 'undefined' ? '' : this.dados_perfil.Enderecos);
            this.dados_perfil_form.controls['Bairro'].setValue(this.dados_perfil.Bairro == 'null' || this.dados_perfil.Bairro == 'undefined' ? '' : this.dados_perfil.Bairro);
            this.dados_perfil_form.controls['Cidade'].setValue(this.dados_perfil.Cidade == 'null' || this.dados_perfil.Cidade == 'undefined' ? '' : this.dados_perfil.Cidade);
            this.dados_perfil_form.controls['Estado'].setValue(this.dados_perfil.Estado == 'null' || this.dados_perfil.Estado == 'undefined'? '' : this.dados_perfil.Estado);

            if (this.permissionService.isCondomino() && !this.dados_perfil.NomeCliente) {
                this.dados_perfil_form.controls['NomeCliente']
                    .setValue(account.condomino);
            }

            this.spinnerService.hide();
        });

        this.apiService.get(`/cadastro/formas_pagamento`, {CodigoCliente: account.codigo}).subscribe((result: FormasPagamento) => {
            this.formas_pagamento = result;

            this.formas_pagamento_form.controls['Banco']
                .setValue(this.formas_pagamento.Banco == 'null' || this.formas_pagamento.Banco == 'undefined' ? '' : this.formas_pagamento.Banco);
            this.formas_pagamento_form.controls['Banco_Digito']
                .setValue(this.formas_pagamento.Banco_Digito == 'null' || this.formas_pagamento.Banco_Digito == 'undefined' ? '' : this.formas_pagamento.Banco_Digito);
            this.formas_pagamento_form.controls['Agencia']
                .setValue(this.formas_pagamento.Agencia == 'null' || this.formas_pagamento.Agencia == 'undefined' ? '' : this.formas_pagamento.Agencia);
            this.formas_pagamento_form.controls['Digito_Agencia']
                .setValue(this.formas_pagamento.Digito_Agencia == 'null' || this.formas_pagamento.Digito_Agencia == 'undefined' ? '' : this.formas_pagamento.Digito_Agencia);
            this.formas_pagamento_form.controls['Conta']
                .setValue(this.formas_pagamento.Conta == 'null' || this.formas_pagamento.Conta == 'undefined' ? '' : this.formas_pagamento.Conta);
            this.formas_pagamento_form.controls['Digito_Conta']
                .setValue(this.formas_pagamento.Digito_Conta == 'null' || this.formas_pagamento.Digito_Conta == 'undefined' ? '' : this.formas_pagamento.Digito_Conta);

            this.spinnerService.hide();
        });
    }

    private createForm() {
        this.dados_perfil_form = new FormGroup({
            NomeCliente: new FormControl(null, Validators.required),
            CPF_CNPJ: new FormControl(null),
            E_Mail: new FormControl(null, Validators.email),
            Telefone: new FormControl(null),
            Celular: new FormControl(null),
            Senha: new FormControl(null),
            CEP: new FormControl(null),
            Tipo_logradouro: new FormControl(null),
            Enderecos: new FormControl(null),
            Numero: new FormControl(null),
            Complemento: new FormControl(null),
            Bairro: new FormControl(null),
            Estado: new FormControl(null),
            Cidade: new FormControl(null),
            dt_update: new FormControl(null),
        });

        this.formas_pagamento_form = new FormGroup({
            /* NomeCliente: new FormControl(null),
            CodigoCliente: new FormControl(null),
            CPF_CNPJ: new FormControl(null), */
            Banco: new FormControl(null),
            Banco_Digito: new FormControl(null, Validators.required),
            Agencia: new FormControl(null, Validators.required),
            Digito_Agencia: new FormControl(null, Validators.required),
            Conta: new FormControl(null, Validators.required),
            Digito_Conta: new FormControl(null, Validators.required),

        });
    }

}
