import { environment } from '../environments/environment';

export const imageToBlob = function(url, callback) {
    const xhr = new XMLHttpRequest();
    xhr.onload = function() {
      const reader = new FileReader();
      reader.onloadend = function() {
        callback(reader.result);
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.responseType = 'blob';
    xhr.send();
};

export const exportToCSV = function(filename, rows) {
    let csvFile = '';
    const listPropertyNames = {};

    if (filename.charAt(0) === '/') {
        filename = filename.substr(1);
    }
    filename = filename.replace(/\//g, '-');

    const processRow = function (row) {
        let finalVal = '';

        let count = 0;
        // tslint:disable-next-line: forin
        for (const propertyName in row) {
            let innerValue: any = '';

            innerValue = row[propertyName];

            innerValue = innerValue === null ? '' : innerValue.toString();
            if (innerValue instanceof Date) {
                innerValue = innerValue.toLocaleString();
            }

            let result = innerValue.replace(/"/g, '""');
            if (result.search(/("|,|\n)/g) >= 0) {
                result = '"' + result + '"';
            }
            if (count) {
                finalVal += ',';
            }
            finalVal += result;
            count++;
        }

        return finalVal + '\n';
    };

    for (let i = 0; i < rows.length; i++) {
        // tslint:disable-next-line: forin
        for (const propertyName in rows[i]) {
            listPropertyNames[propertyName] = '';
        }
    }
    
    for (let i = 0; i < rows.length; i++) {
        rows[i] = Object.assign(JSON.parse(JSON.stringify([listPropertyNames])).pop(), rows[i]);
        csvFile += processRow(rows[i]);
    }

    csvFile = Object.getOwnPropertyNames(listPropertyNames).join(',') + '\n' + csvFile;


    const blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
        navigator.msSaveBlob(blob, filename);
    } else {
        const link = document.createElement('a');
        if (link.download !== undefined) { // feature detection
            // Browsers that support HTML5 download attribute
            const url = URL.createObjectURL(blob);
            link.setAttribute('href', url);
            link.setAttribute('download', filename);
            link.style.visibility = 'hidden';
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    }
};

export const exportToPDF = function(filename, content, elStyle, showHtml = false) {
    let style = '';
    elStyle.each(function(index, item) { style += item.outerHTML; });

    if (filename.charAt(0) === '/') {
        filename = filename.substr(1);
    }
    filename = filename.replace(/\//g, '-');

    const images = content.find('img');
    images.each(function(index, item) {
        imageToBlob($(item).attr('src'), function(data) {
            $(item).attr('src', data);

            if (images.length === index + 1) {
                $.post(environment.apiHost + '/PDF', {
                    content: content.html(),
                    style,
                    filename
                }, function(response) {
                    const win = window.open(response + '/' + showHtml, '_blank');
                    win.focus();
                });
            }
        });
    });
};
