import { Component, OnInit } from '@angular/core';
import { Arquivo } from '../shared/models/arquivo';
import { ApiService } from '../shared/services/api.service';
import { ArquivoService } from '../shared/services/arquivo.service';
import { UtilsService } from '../shared/services/utils.service';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.css']
})

export class AgendaComponent implements OnInit {
    arquivo: Arquivo;
    arquivos: Array<Arquivo[]> = [];
    loading = true;
    fullYear;

    public data = [];
    public type;
    public Total: any = 0;
    public url: SafeResourceUrl;
    private base = 'https://www.bcfadm.com.br/bcfnet/pageflipper/?url=';
    private mes: any;
    private ano: any;
    private natureza: string;
    public identifier;
    private url_position;
    private urls = [];

    constructor(
        public arquivoService: ArquivoService,
        private apiService: ApiService,
        private utils: UtilsService,
        private sanitizer: DomSanitizer
    ) {
        this.fullYear = new Date().getFullYear();
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.base);
    }

    ngOnInit() {
        this.init();
    }

    init() {
        const date = new Date();

        this.arquivoService.get()
            .subscribe((result: any) => {
                this.arquivos = result.filter((administrativo) => {
                    if (administrativo.data_expiracao != '') {
                        let data_expiracao = administrativo.data_expiracao.split('/');
                        data_expiracao = new Date(data_expiracao[2], data_expiracao[1] - 1, data_expiracao[0]);

                        if (data_expiracao < date) {
                            return false;
                        }
                    }
                    return true;
                }).map((administrativo) => {
                    administrativo.link_arquivo = 'http://bcfadm.com.br/bcfnet/' + administrativo.link_arquivo;
                    administrativo.arvore = this.utils.convertToUTF8(administrativo.arvore.split('#')[2]);
                    return administrativo;
                });
                this.loading = false;
            });
    }

    getModalTitle() {
        const balancete = this.urls[this.url_position];

        if (balancete) {
            return balancete.title;
        }
        return;
    }

    open(balancete) {
        if (!balancete.link_arquivo) {
            return;
        }
        console.log(balancete);

        let url = balancete.link_arquivo;
        url = this.base + this.utils.base64Encode(url);
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        console.log(this.url);

        setTimeout(() => {
            this.url_position = this.urls.findIndex((value) => {
                return value.url === url;
            });
        }, 300);
    }

    next() {
        const limit = this.urls.length - 1;

        if (this.url_position < limit) {
            this.url_position++;
        } else {
            this.url_position = 0;
        }
        
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urls[this.url_position].url);
    }

    previous() {
        const limit = this.urls.length - 1;

        if (this.url_position != 0) {
            this.url_position--;            
        } else {
            this.url_position = limit;            
        }
        
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urls[this.url_position].url);
    }
}
