import { Component, OnInit } from '@angular/core';
import { NgbCalendar, NgbDateParserFormatter} from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, FormArray, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { ReservasService } from '../../shared/services/reservas.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../shared/services/utils.service';
import { PermissionsService } from '../../shared/services/permissions.service';
import { UserService } from '../../shared/services/user.service';
import { ModalService } from '../../shared/components/modal/modal.service';
import { exportToCSV, exportToPDF, imageToBlob } from 'src/app/helpers';

@Component({
  selector: 'app-reservas',
  templateUrl: './reservas.component.html',
  styleUrls: ['./reservas.component.scss'],
  providers: [ReservasService]
})
export class ReservasComponent implements OnInit {
  public datas = [];
  public loading = true;
  public espacos;
  form: FormGroup;

  constructor(
    private reservasService: ReservasService,
    private spinnerService: SpinnerService,
    private utils: UtilsService,
    public permissionService: PermissionsService,
    private userService: UserService,
    private modalService: ModalService,
    private calendar: NgbCalendar,
    public formatter: NgbDateParserFormatter,
    private router: Router
  ) {
   }

  ngOnInit() {
    this.get();
    this.createFormInputs();
  }

  get() {
    this.spinnerService.show();

    const login = this.permissionService.isSindico() ? this.permissionService.getCondominioLogin() : this.userService.getUserLogin();

    this.reservasService.get('reserva', {
      condomino: login,
      with_espaco: true
    })
      .subscribe((data: any) => {
        this.datas = data;
        console.log('this.datas', this.datas);

        this.datas = this.datas.map((item, idx) => {
          const horas = [];
          let horas_reservadas = [];
          const _horas_reservadas = item.horas_reservadas.split(',');

          if (_horas_reservadas.length) {
            horas_reservadas.push(_horas_reservadas.shift());
          }

          if (_horas_reservadas.length) {
            horas_reservadas.push(_horas_reservadas.pop());
          }

          horas_reservadas = horas_reservadas.map(hora => {
            return hora + ':00';
          });

          item.horas_reservadas = horas_reservadas.join(' - ');

          return item;
        });

        this.spinnerService.hide();
        this.loading = false;
      });


    this.reservasService.get('espaco', {
      condomino: login
    })
      .subscribe((data: any) => {
        this.espacos = data;
      });
  }

  approve(id) {
    this.reservasService.update('reserva', {
      id: id,
      status: 1
    })
      .subscribe((data: any) => {
        if (data.success) {
          this.datas.map((item, index) => {
            if (item.id === id) {
              item.status = 1;
            }
            return item;
          });
        }
      });
  }

  disapprove(id, status = 2) {
    if ( status === 3 && !confirm('Deseja cancelar a reserva?')) {
      return false;
    }

    if (status === 2 && !confirm('Deseja reprovar a reserva?')) {
      return false;
    }

    this.reservasService.update('reserva', {
      id: id,
      status: status
    })
      .subscribe((data: any) => {
        if (data.success) {
          this.datas.map((item, index) => {
            if (item.id === id) {
              item.status = status;
            }
            return item;
          });
        } else if (data.error) {
          alert(data.message);
        }
      });
  }

  openModal(data) {
    this.modalService.show(this.utils.convertToUTF8('Lista de Convidados'), this.utils.convertToUTF8(data));
  }

  createFormInputs() {
    this.form = new FormGroup({
      espacoId: new FormControl(),
      nomeUsuario: new FormControl(),
      loginUsuario: new FormControl(),
      status: new FormControl(),
      dataInicio: new FormControl(),
      dataFim: new FormControl(),
    });
  }

  onSubmit() {
    const params = {with_espaco: true};

    if (this.form.value.loginUsuario) {
      params['condomino'] = this.form.value.loginUsuario;
    }

    if (this.form.value.dataInicio) {
      params['data_inicio'] =
        this.form.value.dataInicio.year + '-' + this.form.value.dataInicio.month + '-' + this.form.value.dataInicio.day;
    }

    if (this.form.value.dataFim) {
      params['data_fim'] =
        this.form.value.dataFim.year + '-' + this.form.value.dataFim.month + '-' + this.form.value.dataFim.day;
    }

    if (this.form.value.espacoId) {
      params['espaco_id'] = this.form.value.espacoId;
    }

    if (this.form.value.espacoId) {
      params['nome_usuario'] = this.form.value.nomeUsuario;
    }

    if (this.form.value.status) {
      params['status'] = this.form.value.status;
    }

    if (this.form.value.nomeUsuario) {
      params['nome_usuario'] = this.form.value.nomeUsuario;
    }

    this.reservasService.get('reserva', params)
      .subscribe((data: any) => {
        this.datas = data;
        console.log('this.datas', this.datas);

        this.datas = this.datas.map((item, idx) => {
          const days = [];

          return item;
        });

        this.spinnerService.hide();
        this.loading = false;
      });
  }

  exportAsCSV() {
    if (this.datas.length) {
      exportToCSV(this.router.url + '.csv', this.datas);
    }
  }

  exportAsPrint() {
    exportToPDF(
      this.router.url + '-' + 'bcfnet',
      $('#reservas').clone(),
      $('style'),
      true
    );
  }

  exportAsPDF() {
    exportToPDF(
      this.router.url + '-' + 'bcfnet',
      $('#reservas').clone(),
      $('style'),
      false
    );
  }
}
