import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { NgbModule, NgbCalendar, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CKEditorModule } from '@ckeditor/ckeditor5-angular';

import { SharedModule } from '../shared/shared.module';
import { ReservasRoutingModule } from './reservas-routing.module';
import { EspacosComponent } from './espacos/espacos.component';
import { ReservarComponent } from './espacos/reservar/reservar.component';
import { ReservasComponent } from './reservas/reservas.component';
import { CadastrarEspacoComponent } from './espacos/cadastrar-espaco/cadastrar-espaco.component';
import { AtualizarEspacoComponent } from './espacos/atualizar-espaco/atualizar-espaco.component';
import { DisableControlDirective } from '../shared/directives/disableControl.directive';
import { NgbDateMomentParserFormatter } from '../shared/extends/ngb-datemoment-parser-formatter';

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    CKEditorModule,
    NgbModule.forRoot(), //NgbModule.forRoot(),
    ReservasRoutingModule
  ],
  declarations: [
    EspacosComponent,
    ReservarComponent,
    ReservasComponent,
    CadastrarEspacoComponent,
    AtualizarEspacoComponent,
    DisableControlDirective],
  providers: [
      {
        provide: NgbDateParserFormatter,
        useFactory: () => {
          return new NgbDateMomentParserFormatter('DD/MM/YYYY');
        }
      }
    ]
})
export class ReservasModule { }
