import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray, ValidatorFn } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbDateStruct, NgbCalendar,  } from '@ng-bootstrap/ng-bootstrap';
import { ReservasService } from '../../../shared/services/reservas.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../../shared/services/utils.service';
import { __core_private_testing_placeholder__ } from '@angular/core/testing';

@Component({
  selector: 'app-reservas',
  templateUrl: './reservar.component.html',
  styleUrls: ['./reservar.component.scss'],
  providers: [ReservasService]
})
export class ReservarComponent implements OnInit {
  public espaco;
  public reservas;
  public listHours = [];
  public loading = true;
  public espacos_id;
  public model: NgbDateStruct;
  public date: {year: number, month: number};
  public datesDisabled = true;
  public minDate;
  public dateCalendar = '';
  public markDisabled;
  public accept;
  public terms;
  public availableHours = [];
  form: FormGroup;
  
  constructor(
    private route: ActivatedRoute,
    private reservasService: ReservasService,
    private spinnerService: SpinnerService,
    private utils: UtilsService,
    private calendar: NgbCalendar,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.listHours = this.generatelistHours();
    this.espacos_id = this.route.snapshot.paramMap.get('espacos_id');
    this.espaco = { nome: '' };
    this.get();

    this.createFormInputs();
  }

  get() {
    this.spinnerService.show();
    this.minDate = { year: new Date().getFullYear(), month: new Date().getMonth() + 1, day: new Date().getDate() };
    // this.listHours = this.generatelistHours();
    this.get_espaco();
  }

  /*createFormInputs() {
    this.personForm = new FormGroup({
      hobbies: this.createHobbies(this.myhobbies)
    });
    this.getSelectedHobbies();

    //this.form = new FormGroup({
    //  firstName: new FormControl(null, Validators.required),
    //  hours: new FormControl({ disabled: true }, Validators.required)
    //  //hours: this.createHobbies(this.listHours)
    //});

    //this.getSelectedHobbies();
  }*/

  /*
  createHobbies(hobbiesInputs) {
    const arr = hobbiesInputs.map(hobby => {
      return new FormControl(hobby.available || false);
    });
    console.log('arr', arr);
    return new FormArray(arr);
  }

  getSelectedHobbies() {
    this.selectedHobbiesNames = this.form.controls.hobbies['controls'].map(
      (hour, i) => {
        return hour.value && this.espaco.dias_horas.horas[i].value;
      }
    );
    this.getSelectedHobbiesName();
  }

  getSelectedHobbiesName() {
    this.selectedHobbiesNames = this.selectedHobbiesNames.filter(
      function(hobby) {
        if (hobby !== false) {
          return hobby;
        }
      }
    );
  }*/

  get_espaco() {
    this.reservasService.get('espaco', {espacos_id: this.espacos_id})
      .subscribe((data: any) => {
        this.espaco = data.pop();

        console.log(this.espaco);

        const days = [];
        if (this.espaco.dias_horas.dia) {
          for (const index of Object.keys(this.espaco.dias_horas.dia)) {
            days.push(Number(index) ? Number(index) : 7);
          }
        }

        this.markDisabled = (date: any) => {
          return !(days.indexOf(this.calendar.getWeekday(date)) > -1);
        };

        // this.listHours = this.generatelistHours();

        this.spinnerService.hide();
        this.loading = false;
    });
  }

  getReservas(date) {
    this.dateCalendar = date;
    this.reservasService.get('reserva', {
      espacos_id: this.espacos_id,
      data: date
    })
      .subscribe((data: any) => {
        this.reservas = data;

        console.log('this.form.value.hours', this.form.value.hours);
        /*this.form.value.hours.map((item, index) => {
          if (this.espaco.dias_horas.hora.indexOf(String(index)) < 0) {
            (<FormArray>this.form.controls['hours']).at(Number(index)).patchValue(false);
          }
        });*/

        this.listHours = this.generatelistHours();
        this.availableHours = this.listHours.filter(item => (item.available ? true : false));
        console.log(this.availableHours.length);
    });
  }

  onDateSelection(date: any) {
    this.getReservas(`${date.year}-${String('0' + date.month).slice(-2)}-${String('0' + date.day).slice(-2)}`);
  }

  generatelistHours() {
    const arr = [];
    for (let i = 0; i < 24; i++) {
      arr.push({available: false, hour: String('0' + i).slice(-2) + ':00', reserved: false });
    }

    if (this.espaco && typeof this.espaco.dias_horas !== 'undefined' && this.espaco.dias_horas.hora) {
      this.espaco.dias_horas.hora.forEach((item, index) => {
        arr[item].available = true;
        if (this.espaco.tipo_de_reserva === '2') {
          arr[item].daily = true;
        }
      });
    }

    if (this.reservas && typeof this.reservas !== 'undefined') {
      this.reservas.forEach((reserva, index) => {
        reserva.horas_reservadas.split(',').forEach((item) => {
          arr[Number(item)].available = false;
          arr[Number(item)] = { ...arr[Number(item)], ... { reserved: true } };
        });
      });
    }

    return arr;
  }

  selectToday() {
    this.model = this.calendar.getToday();
  }

  onSubmitReserve() {
    const selectedHours = this.form.value.hours.map((v, i) =>
      ((v === true && this.listHours[i].available === true) || this.listHours[i].daily) ? this.listHours[i].hour.replace(':00', '') : null)
    .filter(v => v !== null).join(',');

    if (this.espaco.tipo_de_reserva === '1' && selectedHours.split(',').length > Number(this.espaco.limite_horas)) {
      alert('Você atingiu o limite máximo de horas do dia. Por favor marque apenas ' + this.espaco.limite_horas + ' horário(s).');
      return false;
    }

    if (
      !selectedHours.length ||
      this.form.value.guestList === '' ||
      this.form.value.guestNumber === ''
    ) {
      alert('Preencha todos os campos antes de finalizar sua reserva.');
      return false;
    }

    if (!this.dateCalendar == null) {
      alert('Selecione uma data para a reserva.');
      return false;
    }

    if (!this.form.value.acceptTerms) {
      alert('Leia e aceite os termos da reserva.');
      return false;
    }

    this.reservasService.create('reserva', {
      espacos_id: this.espacos_id,
      horas_reservadas: selectedHours,
      nome_convidados: this.form.value.guestList,
      total_convidados: this.form.value.guestNumber,
      data: this.dateCalendar,
      observacao: this.form.value.comments
    })
      .subscribe((data: any) => {
        this.router.navigate(['/reservas']);
      });

    //horas_reservadas
    //espacos_id
    //nome_convidados
    //total_convidados
    //data
  }

  createFormInputs() {
    this.form = new FormGroup({
      // dateCalendar: new FormControl(true, Validators.required),
      guestList: new FormControl(),
      guestNumber: new FormControl('', Validators.required),
      comments: new FormControl('', Validators.required),
      acceptTerms: new FormControl(),
      hours: this.createHours(this.listHours)
    });
  }

  createHours(inputs) {
    const arr = inputs.map(item => {
      return new FormControl(item || false); // verificar esse reserved...
    });
    return new FormArray(arr);
  }
}
