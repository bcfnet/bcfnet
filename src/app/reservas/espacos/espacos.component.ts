import { Component, OnInit } from '@angular/core';
import { ReservasService } from '../../shared/services/reservas.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../shared/services/utils.service';
import { PermissionsService } from '../../shared/services/permissions.service';

@Component({
  selector: 'app-reservas',
  templateUrl: './espacos.component.html',
  styleUrls: ['./espacos.component.scss'],
  providers: [ReservasService]
})
export class EspacosComponent implements OnInit {
  public datas = [];
  public loading = true;

  constructor(
    private reservasService: ReservasService,
    private spinnerService: SpinnerService,
    public permissionService: PermissionsService,
    private utils: UtilsService
  ) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.reservasService.get('espaco')
      .subscribe((data: any) => {
        this.datas = data;

        this.datas = this.datas.filter((item) => {
          if (item.imagem) {
            item.imagem = this.utils.getBaseUrl() + '/' + item.imagem.replace('./', '');
          }
          return item;
        });

        this.datas = this.datas.map((item, idx) => {
          const days = [];

          if (item.dias_horas.dia) {
            for (const key of Object.keys(item.dias_horas.dia)) {
              days.push(item.dias_horas.dia[key]);
            }
          }
          item.dias_horas.dia = days.join(', ');

          const hours = [];
          if (item.dias_horas.hora) {
            for (const key2 of Object.keys(item.dias_horas.hora)) {
              hours.push(item.dias_horas.hora[key2] + ':00');
            }
          }

          item.dias_horas.hora = [].concat(hours.shift(), hours.pop()).join(' às ');

          return item;
        });

        this.spinnerService.hide();
        this.loading = false;
    });
  }
}
