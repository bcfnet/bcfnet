import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { ToasterService } from 'angular2-toaster';
import '@ckeditor/ckeditor5-build-classic/build/translations/pt-br';
import * as ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { ReservasService } from '../../../shared/services/reservas.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { disableCursor } from 'fullcalendar/src/util';

@Component({
  selector: 'app-cadastrar-espaco',
  templateUrl: './cadastrar-espaco.component.html',
  styleUrls: ['./cadastrar-espaco.component.scss']
})
export class CadastrarEspacoComponent implements OnInit {
  form: FormGroup;
  public daysWeek;
  public listHours = [];
  public showDay;
  public Editor = ClassicEditor;
  public imageBlob;


  constructor(
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private reservaService: ReservasService,
    private router: Router
  ) {

  }

  ngOnInit() {
    this.daysWeek = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SÁB'];
    for (let i = 0; i < 24; i++) {
      this.listHours.push(String('0' + i).slice(-2) + ':00');
    }

    this.createFormInputs();
  }

  createFormInputs() {
    this.form = new FormGroup({
      nome: new FormControl(),
      lotacao: new FormControl(),
      tipo_de_pagamento: new FormControl(),
      tipo_de_reserva: new FormControl(),
      informacao_adicional: new FormControl(),
      termos: new FormControl(),
      preco:  new FormControl(),
      limite_horas: new FormControl('', [Validators.max(24), Validators.min(1)]),
      limite_cancelamento_horas: new FormControl(),
      limite_cancelamento_dias: new FormControl(),
      aprovacao_automatica: new FormControl(false),
      days: this.createInputs(this.daysWeek),
      hours: this.createInputs(this.listHours)
    });
  }

  createInputs(inputs) {
    const arr = inputs.map(item => {
      return new FormControl(item || false);
    });
    return new FormArray(arr);
  }

  onSubmit() {
    console.log('this.form.value', this.form.value);

    const dias = [];
    this.form.value.days.map((item, index) => {
      if (item !== false) {
        dias.push(index);
      }
    });

    const horas = [];
    this.form.value.hours.map((item, index) => {
      if (item !== false) {
        horas.push(index);
      }
    });

    if (
      this.form.value.nome == null ||
      this.form.value.lotacao == null ||
      this.form.value.tipo_de_pagamento == null ||
      this.form.value.tipo_de_reserva == null ||
      this.form.value.termos == null ||
      this.form.value.preco == null ||
      this.form.value.limite_horas == null ||
      // (this.form.value.limite_cancelamento_horas == null || this.form.value.limite_cancelamento_dias == null) ||
      dias.length === 0 ||
      horas.length === 0
    ) {
      alert('Preencha todos os campos.');
      return false;
    }

    this.reservaService.create('espaco', {
      nome: this.form.value.nome,
      lotacao: this.form.value.lotacao,
      tipo_de_pagamento: this.form.value.tipo_de_pagamento,
      tipo_de_reserva: this.form.value.tipo_de_reserva,
      informacao_adicional: this.form.value.informacao_adicional,
      termos: this.form.value.termos,
      preco: this.form.value.preco,
      limite_horas: this.form.value.limite_horas,
      limite_cancelamento_horas: this.form.value.limite_cancelamento_horas,
      limite_cancelamento_dias: this.form.value.limite_cancelamento_dias,
      aprovacao_automatica: this.form.value.aprovacao_automatica,
      dias: dias,
      horas: horas,
      imagem: this.imageBlob,
    })
        .subscribe(
          (result) => {
            alert('Espaço criado com sucesso!');
            /*console.log(result);
            if (result.error === 0) {
              // console.log(JSON.stringify(result));
              // console.log(`Result: ${result}`);
              // this.toasterService.pop('success', 'Parabéns!', 'Sua foto foi publicada com sucesso.');
              // this.router.navigate(['/documentos/gerenciar-arquivos']);
            } else {
              this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
            }*/
            this.spinnerService.hide();
            this.router.navigate(['/reservas/espacos']);
          },
          (err) => {
            console.log(`Error: ${err}`);
            this.spinnerService.hide();
            this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
          }
        );
  }

  public upload(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {

      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.imageBlob = reader.result;
      };
    }
  }

}
