import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EspacosComponent } from './espacos/espacos.component';
import { ReservarComponent } from './espacos/reservar/reservar.component';
import { CadastrarEspacoComponent } from './espacos/cadastrar-espaco/cadastrar-espaco.component';
import { AtualizarEspacoComponent } from './espacos/atualizar-espaco/atualizar-espaco.component';
import { ReservasComponent } from './reservas/reservas.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'reservas',
  },
  {
    path: 'reservas',
    component: ReservasComponent
  },
  {
    path: 'espacos',
    component: EspacosComponent
  },
  {
    path: 'espacos/cadastrar',
    component: CadastrarEspacoComponent
  },
  {
    path: 'espacos/atualizar/:espacos_id',
    component: AtualizarEspacoComponent
  },
  {
    path: 'espacos/reservar/:espacos_id',
    component: ReservarComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReservasRoutingModule { }
