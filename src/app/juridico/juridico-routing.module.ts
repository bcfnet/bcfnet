import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProcessosComponent } from './processos/processos.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'acompanhamento-de-processos'
  },
  {
    path: 'acompanhamento-de-processos',
    component: ProcessosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class JuridicoRoutingModule { }
