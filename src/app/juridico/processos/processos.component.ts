import { Component, OnInit } from '@angular/core';
import { AdministrativoService } from '../../shared/services/administrativo.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';

@Component({
  selector: 'app-processos',
  templateUrl: './processos.component.html',
  styleUrls: ['./processos.component.scss']
})
export class ProcessosComponent implements OnInit {

  public datas = [];
  public loading = true;

  constructor(
    private administrativoService: AdministrativoService,
    private spinnerService: SpinnerService
  ) { }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.administrativoService.get('juridico', 'Acompanhamento de Processos')
      .subscribe((data: any) => {
        this.datas = data
          .map((administrativo) => {
            administrativo.arvore = administrativo.arvore.split('#')[2];
            return administrativo;
          });
        this.spinnerService.hide();
        this.loading = false;
        console.log(data);
    });
  }

}
