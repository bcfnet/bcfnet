import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JuridicoRoutingModule } from './juridico-routing.module';
import { ProcessosComponent } from './processos/processos.component';

@NgModule({
  imports: [
    CommonModule,
    JuridicoRoutingModule
  ],
  declarations: [ProcessosComponent]
})
export class JuridicoModule { }
