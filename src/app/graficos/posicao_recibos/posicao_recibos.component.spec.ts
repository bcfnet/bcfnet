import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosicaoRecibosComponent } from './posicao_recibos.component';

describe('PosicaoRecibosComponent', () => {
  let component: PosicaoRecibosComponent;
  let fixture: ComponentFixture<PosicaoRecibosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosicaoRecibosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosicaoRecibosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
