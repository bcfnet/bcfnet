import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import * as moment from 'moment';
import { UtilsService } from '../../shared/services/utils.service';

@Component({
  selector: 'app-posicao-recibos',
  templateUrl: './posicao_recibos.component.html',
  styleUrls: ['./posicao_recibos.component.scss']
})
export class PosicaoRecibosComponent implements OnInit {

  data = [];
  loading = true;
  mes;
  ano;
  type;
  periodos;

  constructor(
    private apiService: ApiService,
    private spinnerService: SpinnerService,
    private utils: UtilsService
  ) {
    this.mes = moment().format('MM');
    this.ano = moment().format('YYYY');
  }

  ngOnInit() {
    this.init();
    this.listarPeriodo();
  }

  init() {
    this.get();
  }

  onChange(periodo?: any, ) {
    this.spinnerService.show();
    periodo = periodo.split('/');
    this.mes = periodo[0];
    this.ano = periodo[1];
    this.get();
  }

  onChangeType(type?: string) {
    this.spinnerService.show();
    this.type = type;
    this.get(type);
  }

  listarPeriodo() {
    const begin = moment('2017-01-01');
    const end = moment();

    this.periodos = this.utils.getRangeOfDates(begin, end, 'month').reverse();
  }

  private get(type = 'ADMINISTRATIVOS') {
    // this.spinnerService.show();

    this.apiService.get('/financeiro/balancete_analitico_grafico_novo', {
      month: new Date().getMonth() + 1,
      year: new Date().getFullYear(),
      type: type
    })
    .subscribe((data: any) => {
      this.data = data;
      this.loading = false;
      this.spinnerService.hide();
    });
  }

}
