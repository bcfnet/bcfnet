import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficoAguaEEsgotoComponent } from './grafico-agua-e-esgoto.component';

describe('GraficoAguaEEsgotoComponent', () => {
  let component: GraficoAguaEEsgotoComponent;
  let fixture: ComponentFixture<GraficoAguaEEsgotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficoAguaEEsgotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficoAguaEEsgotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
