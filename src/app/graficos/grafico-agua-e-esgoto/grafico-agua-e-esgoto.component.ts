import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { PermissionsService } from '../../shared/services/permissions.service';
import { UserService } from '../../shared/services/user.service';
import { UtilsService } from '../../shared/services/utils.service';

@Component({
  selector: 'app-grafico-agua-e-esgoto',
  templateUrl: './grafico-agua-e-esgoto.component.html',
  styleUrls: ['./grafico-agua-e-esgoto.component.scss']
})
export class GraficoAguaEEsgotoComponent implements OnInit {

    public TotalDespesas: any;
    public Data: any;
    public labels;
    public Despesas: any;
    public loaded = false;

    //Line Chart Config
    public lineChartLabels: Array<any> = [];
    public lineChartData: Array<any>;
    public lineChartOptions: any = {
        scales: {
            yAxes: [
                {
                    id: 'y-axis-1',
                    type: 'linear',
                    display: true,
                    position: 'left',
                    ticks: {
                        padding: 10,
                        maxTicksLimit: 4,
                        callback: function (value, index, values) {
                            return 'R$' + value.toLocaleString('pt-BR');
                        }
                    }
                }
            ]
        },
        maintainAspectRatio: false,
        tooltips: {
            callbacks: {
                label: function(tooltip, data) {
                    const datasetLabel = data.datasets[tooltip.datasetIndex].label || '';
                    return datasetLabel + ' : R$ ' + tooltip.yLabel.toLocaleString('pt-BR');
                }
            }
        }
    };
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'bar';

    constructor(
        private apiService: ApiService,
        private permissionService: PermissionsService,
        private userService: UserService,
        private utils: UtilsService
    ) {
        this.labels = [];
        this.TotalDespesas = [];
    }

    ngOnInit() {
        const fullYear = new Date().getFullYear();
        this.init(fullYear);
    }

    init(fullYear: any) {
        const login = this.utils.getToken();

        this.apiService.get('/condominio/graficos', {
            year: fullYear,
            login: login
        }).subscribe((results: any) => {

            this.Despesas = results.filter((result) => {
                return result.titulo === 'AGUA E ESGOTO';
            });
            this.Despesas.reverse();

            const data = this.Despesas.map((result) => {
                this.lineChartLabels.push(result.mes_referencia);
                this.labels.push(result.mes_referencia);

                return parseFloat(this.format(result.valor));
            });

            this.lineChartData = [
                { data: data, label: 'Água e Esgoto' },
            ];

            this.TotalDespesas.Agua_Esgoto = data[ data.length - 1 ];

            this.loaded = true;
        });
    }

    format(value) {
        value = value.replace(',', '.');
        return value.toLocaleString('pt-BR');
    }

    onChange(data: string) {
        let valor = 0;
        this.Despesas.filter((result) => {
            return result.mes_referencia === data;
        }).map((result) => {
            valor = parseFloat(this.format(result.valor));
            return valor;
        });
        this.TotalDespesas.Agua_Esgoto = valor;
    }

}
