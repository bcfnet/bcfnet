import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GraficosRoutingModule } from './graficos-routing.module';

import { ThemeConstants } from '../shared/config/theme-constant';
import { ChartsModule } from 'ng2-charts';
import 'd3';
import 'nvd3';
import { NvD3Module } from 'ng2-nvd3';
import { GraficoDespesasComponent } from './grafico-despesas/grafico-despesas.component';
import { GraficoLuzEForcaComponent } from './grafico-luz-e-forca/grafico-luz-e-forca.component';
import { GraficoAguaEEsgotoComponent } from './grafico-agua-e-esgoto/grafico-agua-e-esgoto.component';
import { SharedModule } from '../shared/shared.module';
import { BalanceteAnaliticoGraficoComponent } from './balancete-analitico-grafico/balancete-analitico-grafico.component';
import { PosicaoRecibosComponent } from './posicao_recibos/posicao_recibos.component';
import { PosicaoRecibosGraficoAnaliticoComponent } from './posicao_recibos_grafico_analitico/posicao_recibos_grafico_analitico.component';
import { NgbModule, NgbTooltipConfig } from '@ng-bootstrap/ng-bootstrap';
import { DataTablesModule } from 'angular-datatables';

@NgModule({
  imports: [
    CommonModule,
    GraficosRoutingModule,
    ChartsModule,
    SharedModule,
    NvD3Module,
    NgbModule,
    DataTablesModule
  ],
  declarations: [
    GraficoDespesasComponent,
    GraficoLuzEForcaComponent,
    GraficoAguaEEsgotoComponent,
    BalanceteAnaliticoGraficoComponent,
    PosicaoRecibosComponent,
    PosicaoRecibosGraficoAnaliticoComponent,
  ],
  providers: [
    ThemeConstants,
    NgbTooltipConfig
  ],
  exports: [
    GraficoDespesasComponent,
    GraficoLuzEForcaComponent,
    GraficoAguaEEsgotoComponent,
    BalanceteAnaliticoGraficoComponent,
    PosicaoRecibosComponent,
    PosicaoRecibosGraficoAnaliticoComponent,
  ]
})
export class GraficosModule { }
