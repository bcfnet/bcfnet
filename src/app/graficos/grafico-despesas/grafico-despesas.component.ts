import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ApiService } from '../../shared/services/api.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../shared/services/utils.service';

@Component({
  selector: 'app-grafico-de-despesas',
  templateUrl: './grafico-despesas.component.html',
  styleUrls: ['./grafico-despesas.component.scss']
})
export class GraficoDespesasComponent implements OnInit {

    public TotalDespesas: any;
    public TotalDate: any;
    public loaded = false;
    public loading = true;

    //Line Chart Config
    public lineChartLabels: Array<any> = [];

    public lineChartData: Array<any>;
    public lineChartOptions: any = {
            scales: {
            yAxes: [
                    {
                        id: 'y-axis-1',
                        type: 'linear',
                        display: true,
                        position: 'left',
                        ticks: {
                            padding: 10,
                            maxTicksLimit: 4,
                            callback: function (value, index, values) {
                                return 'R$' + value.toLocaleString('pt-BR');
                            }
                        }
                    },
                    {
                        id: 'y-axis-2',
                        type: 'linear',
                        display: false
                    }
            ]
            },
            maintainAspectRatio: false,
            tooltips: {
                callbacks: {
                    label: function(tooltip, data) {
                        const datasetLabel = data.datasets[tooltip.datasetIndex].label || '';
                        return datasetLabel + ' : R$ ' + tooltip.yLabel.toLocaleString('pt-BR');
                    }
                }
            }
    };
    public lineChartLegend: boolean = true;
    public lineChartType: string = 'line';

    constructor(
        private apiService: ApiService,
        private spinnerService: SpinnerService,
        private utils: UtilsService
    ) {
        this.TotalDespesas = [];
    }

    ngOnInit() {
        this.init();
    }

    getLastMonths(limit) {
        const months = [];

        for (let i = limit; i >= 0; i--) {
            months.push(moment().subtract(i, 'M').format('MM/YYYY'));
        }
        return months;
    }

    getCurrentMonth() {
        return this.utils.capitalize(moment().format('MMMM/YYYY'));
    }

    init() {
        let posix = 0;
        const dates = this.getLastMonths(12);
        this.TotalDate = dates;

        const administrativo = [];
        const concessionarias = [];
        const contratos = [];
        const materiais = [];
        const pessoal = [];


        this.apiService.get('/financeiro/graficos', {
            month: new Date().getMonth() + 1,
            year: new Date().getFullYear()
        })
        .subscribe((results: any) => {

            if (results) {
                results.reverse();

                for (const result of results) {

                    this.lineChartLabels.push(result.Mes_Referencia);

                    for (const data of result.Dados) {
                        const Conta = data.Conta;
                        const Valor = data.Valor * -1;

                        switch (Conta) {
                            case 'ADMINISTRATIVAS':
                                administrativo[posix] = Valor;
                            break;
                            case 'CONCESSIONARIAS':
                                concessionarias[posix] = Valor;
                            break;
                            case 'CONTRATOS':
                                contratos[posix] = Valor;
                            break;
                            case 'MATERIAIS':
                                materiais[posix] = Valor;
                            break;
                            case 'PESSOAL':
                                pessoal[posix] = Valor; // toLocaleString()
                            break;
                        }

                        if (posix >= 10) {

                            this.TotalDespesas = {
                                Administrativo: administrativo[administrativo.length - 1],
                                Concessionarias: concessionarias[concessionarias.length - 1],
                                Contratos: contratos[contratos.length - 1],
                                Materiais: materiais[materiais.length - 1],
                                Pessoal: pessoal[pessoal.length - 1]
                            };
                            this.loaded = true;

                            this.lineChartData = [
                                { data: this.clean(administrativo), label: 'Administrativas' },
                                { data: this.clean(concessionarias), label: 'Concessionárias' },
                                { data: this.clean(contratos), label: 'Contratos' },
                                { data: this.clean(materiais), label: 'Materiais' },
                                { data: this.clean(pessoal), label: 'Pessoal' }
                            ];
                        }
                    }
                    posix++;
                }

            }

        });
    }

    clean(data) {
        for (let i = 0; i < 12; i++) {
            if (!data[i]) {
                data[i] = 0;
            }
        }
        return data;
    }

    onChange(data: string) {

    }

}
