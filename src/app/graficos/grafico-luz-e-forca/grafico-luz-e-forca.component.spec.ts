import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GraficoLuzEForcaComponent } from './grafico-luz-e-forca.component';

describe('GraficoLuzEForcaComponent', () => {
  let component: GraficoLuzEForcaComponent;
  let fixture: ComponentFixture<GraficoLuzEForcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GraficoLuzEForcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GraficoLuzEForcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
