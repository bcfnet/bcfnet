import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PosicaoRecibosGraficoAnaliticoComponent } from './posicao_recibos_grafico_analitico.component';

describe('PosicaoRecibosGraficoAnaliticoComponent', () => {
  let component: PosicaoRecibosGraficoAnaliticoComponent;
  let fixture: ComponentFixture<PosicaoRecibosGraficoAnaliticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PosicaoRecibosGraficoAnaliticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PosicaoRecibosGraficoAnaliticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
