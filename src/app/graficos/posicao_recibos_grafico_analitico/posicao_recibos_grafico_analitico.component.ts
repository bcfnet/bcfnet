import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../shared/services/api.service';
import * as moment from 'moment';
import { PosicaoRecibosService } from '../../shared/services/posicao_recibos.service';
import { PagerService } from 'src/app/shared/services/pager.service';
import { exportToCSV } from 'src/app/helpers';
import { PermissionsService } from 'src/app/shared/services/permissions.service';

@Component({
  selector: 'app-posicao-recibos-grafico-analitico',
  templateUrl: './posicao_recibos_grafico_analitico.component.html',
  styleUrls: ['./posicao_recibos_grafico_analitico.component.scss'],
  providers: [PosicaoRecibosService]
})
export class PosicaoRecibosGraficoAnaliticoComponent implements OnInit {
    dtOptions: DataTables.Settings = {};

    loading = true;
    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[] = [];
    private searchable = [];
    // array of all items to be paged
    private allItems: any[];

    constructor(
        public permissionService: PermissionsService,
        private apiService: ApiService,
        private posicaoRecibosService: PosicaoRecibosService,
        private pagerService: PagerService,
        private router: Router
    ) { }

    ngOnInit() {
        this.init();
    }

    init() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            lengthChange: false,
            searching: true,
            info: false,
            ordering: false,
            paging: true,
            language: {
                'search': 'Filtrar por palavra-chave',
                'loadingRecords': 'Carregando...',
                'processing':     'Processando...',
                'paginate': {
                    'first':      'Primeiro',
                    'last':       'Último',
                    'next':       'Próximo',
                    'previous':   'Anterior'
                }
            }
        };

        this.posicaoRecibosService.get(
                new Date().getMonth() + 1, new Date().getFullYear())
            .then(
                (posicao_recibos: any) => {
                    this.allItems = posicao_recibos;
                    this.searchable = this.allItems;
                    // initialize to page 1
                    this.setPage(1);
                    this.loading = false;
                },
                (error) => {
                    this.allItems = [];
                    this.loading = false;
                }
            );
    }

    setPage(page: number) {
        this.pager = this.pagerService.getPager(this.allItems.length, page, 1000);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    arrayOne(n: number): any[] {
            return Array(n);
    }

    formatToDate(str) {
        if (str === '') {
            return;
        }
        str = str.substring(0, str.length - 6);
        return moment(new Date(str)).format('DD/MM/YYYY');
    }

    formatToCurrency(str) {
        return str.toLocaleString('pt-BR');
    }

    onChange(text) {
        if (!text) {
            this.allItems = this.searchable;
            this.setPage(1);
            this.loading = false;

            return this.allItems;
        }

        const search = parseInt(text, 10);

        const temp = this.searchable.filter((d) => {
            if (text !== '') {
                return d.status === search;
            }
        });

        this.allItems = temp;

        this.setPage(1);

        if (this.allItems.length === 0) {
            this.loading = false;
        }
    }

    searchChange(text) {
        if (!text) {
            this.allItems = this.searchable;
            this.setPage(1);
            this.loading = false;

            return this.allItems;
        }

        const search = text.toLowerCase();

        const temp = this.searchable.filter((d) => {
            return (d.ComplementoUnidade.toLowerCase().indexOf(search) !== -1 ||
                d.ComplementoUnidade.toLowerCase().indexOf(search) !== -1 ||
                d.CodigoUnidade.indexOf(search) !== -1 ||
                d.NumeroRecibo.indexOf(search) !== -1 || !search);
        });

        this.allItems = temp;

        this.setPage(1);

        if (this.allItems.length === 0) {
            this.loading = false;
        }
    }

    exportAsCSV() {
        if (this.allItems.length) {
            exportToCSV(this.router.url + '-posicao-recibos-grafico-analitico' + '.csv', this.allItems);
        }
    }

}
