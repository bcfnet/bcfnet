import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../shared/services/api.service';
import { Unidade } from '../../shared/models/unidade';
import { UserService } from '../../shared/services/user.service';
import { BalanceteAnaliticoGraficoService } from '../../shared/services/balancete_analitico_grafico.service';
import { BalanceteAnaliticoGrafico } from '../../shared/models/balancete_analitico_grafico';
import { BaseChartDirective } from 'ng2-charts';
import * as moment from 'moment';
import { UtilsService } from '../../shared/services/utils.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-balancete-analitico-grafico',
  templateUrl: './balancete-analitico-grafico.component.html',
  styleUrls: ['./balancete-analitico-grafico.component.scss']
})
export class BalanceteAnaliticoGraficoComponent implements OnInit {
    dtOptions: DataTables.Settings = {};

    @ViewChild(BaseChartDirective) chart: BaseChartDirective;
    unidades: Array<Unidade> = [];
    private labels = [];
    private values = [];
    public periodos = [];
    public loading: boolean = true;
    // Donut Chart Config
    public donutChartLabels: string[]; //['Unidade', 'Fundo de Reserva'];
    public donutChartData: number[];
    public donutChartType: string = 'doughnut';
    public donutChartLegend: boolean = false;
    public dountChartOptions: any = {
        pieceLabel: {
            render: function (args) {
                const label = args.label.split(' ')[0].split('-')[1],
                    value = args.value;
                return label + ': R$ ' + value.toLocaleString('pt-BR');
        },
        chart: {
            type: 'pieChart',
            donut: true,
            labelThreshold: 0.01,
            showLabels: true,
            legend: {
                margin: {
                    top: 5,
                    right: 35,
                    bottom: 5,
                    left: 0
                }
            },
            pieceLabel: {
                render: 'label'
            }
        },
        cutoutPercentage: 75,
            tooltips: {
                callbacks: {
                    label: function(tooltip, data) {
                        const datasetLabel = data.datasets[tooltip.datasetIndex].label || '';
                        return datasetLabel + ' : R$ ' + tooltip.yLabel.toLocaleString('pt-BR');
                    }
                }
            }
        }
    };
    public data = [];
    public type;
    public Total: any = 0;
    public url: SafeResourceUrl;
    private base = 'https://www.bcfadm.com.br/bcfnet/pageflipper/?url=';
    private mes: any;
    private ano: any;
    private natureza: string;
    public identifier;
    private url_position;
    private urls = [];

    constructor(
        public userService: UserService,
        private balanceteGraficoAnaliticoService: BalanceteAnaliticoGraficoService,
        private apiService: ApiService,
        private spinnerService: SpinnerService,
        private sanitizer: DomSanitizer,
        private utils: UtilsService
    ) {
        this.donutChartData = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        this.natureza = 'D';
        this.mes = moment().format('MM');
        this.ano = moment().format('YYYY');
        this.data = [];
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.base);
    }

    ngOnInit() {
        this.dtOptions = {
            pagingType: 'full_numbers',
            lengthChange: false,
            searching: false,
            info: false,
            ordering: false,
            paging: true,
            language: {                
                "loadingRecords": "Carregando...",
                "processing":     "Processando...",
                "paginate": {
                    "first":      "Primeiro",
                    "last":       "Último",
                    "next":       "Próximo",
                    "previous":   "Anterior"
                }
            }
        };
        
        this.get();
        this.listarPeriodo();
    }

    onChange(periodo?: any, ) {
            this.spinnerService.show();
            periodo = periodo.split('/');
            this.mes = periodo[0];
            this.ano = periodo[1];
            this.get();
    }

    onChangeType(type?: string) {
            this.spinnerService.show();
            this.type = type;
            this.get(type);
    }

    /*onChangeNatureza(natureza?: string) {
            this.spinnerService.show();
            this.natureza = natureza;
            this.get();
    }*/

    listarPeriodo() {
        const begin = moment('2017-01-01');
        const end = moment();

        this.periodos = this.utils.getRangeOfDates(begin, end, 'month').reverse();
    }

    open(balancete) {
        if (!balancete.URL) {
            return;
        }

        let url = balancete.URL;
        url = this.base + this.utils.base64Encode(url);
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url);

        setTimeout(() => {
            this.url_position = this.urls.findIndex((value) => {
                return value.url === url;
            });
        }, 300);
    }

    next() {
        const limit = this.urls.length - 1;

        if (this.url_position < limit) {
            this.url_position++;
        } else {
            this.url_position = 0;
        }
        
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urls[this.url_position].url);
    }

    previous() {
        const limit = this.urls.length - 1;

        if (this.url_position != 0) {
            this.url_position--;            
        } else {
            this.url_position = limit;            
        }
        
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urls[this.url_position].url);
    }

    getModalTitle() {
        const balancete = this.urls[this.url_position];

        if (balancete) {
            return balancete.title;
        }
        return;
    }

    private get(type = 'PESSOAL') {
        this.Total = 0;
        this.values = [];
        this.labels = [];

        this.apiService.get('/financeiro/balancete_analitico_grafico_novo', {
            month: this.mes,
            year: this.ano,
            type: type
        })
        .subscribe((data: any) => {
            this.data = data;
            this.loading = false;
            this.spinnerService.hide();

            let total = 0;
            let count = 0;

            if (!data) {
                this.data = [];
                this.spinnerService.hide();
                return;
            }

            for (const balancete of data) {

                if (balancete) {
                    total = parseFloat(balancete.ValorCC) * -1;
                    this.Total += total;

                    if (balancete.URL) {
                        const url: any = this.base + this.utils.base64Encode(balancete.URL);
                        this.urls.push({
                            title: data.Campo2,
                            url: url
                        });
                    }
                } else {
                    total = 0;
                }

                if (total !== 0) {
                    this.values.push(total);
                    this.labels.push(balancete.Descricao.replace(/ +(?= )/g, ''));
                }

                count ++;

                if (count == data.length) {
                    this.loading = false;

                    setTimeout(() => {
                            if (this.chart && this.chart.chart && this.chart.chart.config) {
                                this.chart.chart.config.data.labels = this.labels;
                                this.chart.chart.config.data.datasets[0].data = this.values;
                                
                                this.chart.chart.update();
                                this.spinnerService.hide();
                            }
                    }, 1000);
                }
            }
        });
    }
}
