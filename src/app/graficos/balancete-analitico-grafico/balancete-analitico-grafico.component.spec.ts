import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceteAnaliticoGraficoComponent } from './balancete-analitico-grafico.component';

describe('BalanceteAnaliticoGraficoComponent', () => {
  let component: BalanceteAnaliticoGraficoComponent;
  let fixture: ComponentFixture<BalanceteAnaliticoGraficoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceteAnaliticoGraficoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceteAnaliticoGraficoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
