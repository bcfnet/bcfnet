import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MapaLocalizacaoRoutingModule } from './mapa-localizacao-routing.module';
import { MapaLocalizacaoComponent } from './mapa-localizacao.component';
import { NguiMapModule} from '@ngui/map';

@NgModule({
  imports: [
    CommonModule,
    MapaLocalizacaoRoutingModule,
    NguiMapModule.forRoot({apiUrl: 'https://maps.google.com/maps/api/js?key=AIzaSyDaBBrm53TkapfFGCXdjzRRettgUhCzgJ4'})
  ],
  declarations: [MapaLocalizacaoComponent]
})
export class MapaLocalizacaoModule { }
