import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MapaLocalizacaoComponent } from './mapa-localizacao.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'mapa-de-localizacao'
  },
  {
    path: 'mapa-de-localizacao',
    component: MapaLocalizacaoComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MapaLocalizacaoRoutingModule { }
