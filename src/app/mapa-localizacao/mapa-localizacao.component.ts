import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ApiService } from '../shared/services/api.service';
import { Unidade } from '../shared/models/unidade';
import { PermissionsService } from '../shared/services/permissions.service';
import { UserService } from '../shared/services/user.service';

@Component({
  selector: 'app-mapa-localizacao',
  templateUrl: './mapa-localizacao.component.html',
  styleUrls: ['./mapa-localizacao.component.scss']
})
export class MapaLocalizacaoComponent implements OnInit {

  mapConfig = {
    zoom: 17,
    styles: []
  };

  unidade: Unidade;
  unidades: Array<Unidade[]>;
  address: any = 'https://www.google.com';

  constructor(
    private apiService: ApiService,
    public sanitizer: DomSanitizer,
    private permissionService: PermissionsService,
    private userService: UserService
  ) {
      this.address = this.sanitizer.bypassSecurityTrustResourceUrl(this.address);
  }

  ngOnInit() {
    console.log('Relatório de Condômino');

    const data = {
      login: this.permissionService.isSindico() ? this.userService.getUserLogin() : this.permissionService.getCondominioLogin()
    };

    this.apiService.get('/cadastro/retorna_unidades', data)
      .subscribe((result: any) => {

        this.unidades = result;
        this.unidade = result[0];
        this.address = [this.unidade.EnderecoCondominio, this.unidade.Bairro,
            this.unidade.CEP, this.unidade.Cidade, this.unidade.UF, 'Brasil'].join(' ');
        this.address = this.sanitizer.bypassSecurityTrustResourceUrl(
            'https://www.google.com/maps/embed/v1/place?key=AIzaSyDaBBrm53TkapfFGCXdjzRRettgUhCzgJ4&q=' + encodeURIComponent(this.address)
        );
      });
  }
}
