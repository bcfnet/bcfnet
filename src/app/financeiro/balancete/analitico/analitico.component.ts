import { Component, OnInit } from '@angular/core';
import { BalanceteAnaliticoService } from '../../../shared/services/balancete_analitico.service';
import { UtilsService } from '../../../shared/services/utils.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';

@Component({
  selector: 'app-analitico',
  templateUrl: './analitico.component.html',
  styleUrls: ['./analitico.component.scss']
})
export class AnaliticoComponent implements OnInit {
    public data = [];
    public periodo;
    public loading = true;

    constructor(
        private balanceteAnaliticoService: BalanceteAnaliticoService,
        private spinnerService: SpinnerService,
        private utils: UtilsService
    ) {
        this.data = [];
    }

    ngOnInit() {
        this.get();
    }

    get() {
        this.spinnerService.show();

        this.balanceteAnaliticoService.get(this.periodo ? this.periodo[0] : new Date().getMonth() + 1,
            this.periodo ? this.periodo[1] : new Date().getFullYear())
        .subscribe((data: any) => {
            this.data = data.filter((balancete, index) => {
                if(index === 0) return false;
                                
                balancete.Campo1 = balancete.Campo1.trim();
                balancete.Campo2 = balancete.Campo2.trim();
                balancete.Campo3 = balancete.Campo3.trim();
                balancete.Campo4 = balancete.Campo4.trim().padStart(2, '0');

                if(balancete.Campo1 == "" && balancete.Campo2 == "" && balancete.Campo3 == "" && (balancete.Campo4 == "00" || balancete.Campo4 == "")) return false;
                
                if(balancete.Campo1 == "Sub - Total") {
                    balancete.subTotal = parseFloat(balancete.Campo2.replace('.', '').replace(',', '.')) > -1 ? 'positive' : 'negative';
                }                

                return balancete;
            });
            
            this.spinnerService.hide();
            this.loading = false;
        }, () => {
            this.loading = false;
        });
    }

    onChange(periodo) {
        this.periodo = periodo.split('/');
        this.get();
    }

    isString(value) {
        value = parseFloat(value);

        return isNaN(value);
    }

}
