import { Component, OnInit } from '@angular/core';
import { BalanceteSinteticoService } from '../../../shared/services/balancete_sintetico.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-sintetico',
  templateUrl: './sintetico.component.html',
  styleUrls: ['./sintetico.component.scss'],
  providers: [BalanceteSinteticoService]
})
export class SinteticoComponent implements OnInit {

    public data = [];
    public periodo;
    public loading = true;

    constructor(
        public utils: UtilsService,
        private balanceteSinteticoService: BalanceteSinteticoService,
        private spinnerService: SpinnerService
    ) {
        this.data = [];
    }

    ngOnInit() {
        this.get();
    }

    get() {
        this.spinnerService.show();

        this.balanceteSinteticoService.get(this.periodo ? this.periodo[0] : new Date().getMonth() + 1,
        this.periodo ? this.periodo[1] : new Date().getFullYear())
        .subscribe((data: any) => {
            this.data = data.filter((item, index) => {
                if (index == 0) return;
                if (item.Campo1.trim() == "" && item.Campo2.trim() == "" && item.Campo3.trim() == "") return false;                
                return item;
            });
            
            this.spinnerService.hide();
            this.loading = false;
        }, () => {
            this.loading = false;
        });
    }

    onChange(periodo) {
        this.periodo = periodo.split('/');
        this.get();
    }

}
