import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SemiSinteticoComponent } from './semi-sintetico.component';

describe('SemiSinteticoComponent', () => {
  let component: SemiSinteticoComponent;
  let fixture: ComponentFixture<SemiSinteticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SemiSinteticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SemiSinteticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
