import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BalanceteSemiSinteticoService } from '../../../shared/services/balancete_semi_sintetico.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-semi-sintetico',
  templateUrl: './semi-sintetico.component.html',
  styleUrls: ['./semi-sintetico.component.scss'],
  providers: [BalanceteSemiSinteticoService]
})
export class SemiSinteticoComponent implements OnInit {

  public tipo = 'C';
  public data = [];
  public periodo;
  public loading = true;

  constructor(
    private balanceteSemiSinteticoService: BalanceteSemiSinteticoService,
    private spinnerService: SpinnerService,
    private utils: UtilsService,
    private route: ActivatedRoute
  ) {
    this.data = [];

    this.tipo = this.route.snapshot.params.tipo === 'credito' ? 'C' : 'D';
  }

  ngOnInit() {
    this.get();
    this.spinnerService.display(true);
  }

  get() {
    this.balanceteSemiSinteticoService.get(this.periodo ? this.periodo[0] : new Date().getMonth() + 1,
    this.periodo ? this.periodo[1] : new Date().getFullYear(), this.tipo)
    .subscribe((data: any) => {
        this.data = data;
        this.spinnerService.display(false);
        this.loading = false;
      
    }, () => {
        this.loading = false;
    });
  }

  onChange(periodo) {
    this.periodo = periodo.split('/');
    this.get();
  }

}
