import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../shared/services/api.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-balancete-anual',
  templateUrl: './balancete-anual.component.html',
  styleUrls: ['./balancete-anual.component.scss']
})
export class BalanceteAnualComponent implements OnInit {
  public data = [];
  public periodo;
  public loading = true;

  constructor(
    private apiService: ApiService,
    private spinnerService: SpinnerService,
    private utils: UtilsService
  ) {
    this.data = [];
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.apiService.get('/financeiro/balancete_anual', {
        'month' : new Date().getMonth() + 1,
        'year' : new Date().getFullYear()
    })
    .subscribe((data: any) => {
        this.data = data;
        this.spinnerService.hide();
        this.loading = false;
    });
  }
}
