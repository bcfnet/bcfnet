import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceteAnualComponent } from './balancete-anual.component';

describe('BalanceteAnualComponent', () => {
  let component: BalanceteAnualComponent;
  let fixture: ComponentFixture<BalanceteAnualComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceteAnualComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceteAnualComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
