import { Component, OnInit } from '@angular/core';
import { BalanceteRecPagService } from '../../../shared/services/balancete_rec_pag.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-recpag',
  templateUrl: './recpag.component.html',
  styleUrls: ['./recpag.component.scss']
})
export class RecpagComponent implements OnInit {

  public data = [];
  public periodo;
  public loading = true;

  constructor(
    private balanceteRecpagService: BalanceteRecPagService,
    private spinnerService: SpinnerService,
    private utils: UtilsService
  ) {
    this.data = [];
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.balanceteRecpagService.get(this.periodo ? this.periodo[0] : new Date().getMonth() + 1,
    this.periodo ? this.periodo[1] : new Date().getFullYear())
    .subscribe((data: any) => {
        this.data = data.filter((item, index) => {
          if (item.Campo1 == "" && item.Campo2 == "" && item.Campo3 == "" && item.Campo4 == "" && item.Campo5 == " ") return false;

          return item;
        });
        
        this.spinnerService.hide();
        
        this.loading = false;
    }, () => {
        this.loading = false;
    });
  }

  onChange(periodo) {
    this.periodo = periodo.split('/');
    this.get();
  }

}
