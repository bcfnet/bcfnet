import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecpagComponent } from './recpag.component';

describe('RecpagComponent', () => {
  let component: RecpagComponent;
  let fixture: ComponentFixture<RecpagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecpagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecpagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
