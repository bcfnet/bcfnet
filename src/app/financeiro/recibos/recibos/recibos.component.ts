import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';

@Component({
  selector: 'app-recibos',
  templateUrl: './recibos.component.html',
  styleUrls: ['./recibos.component.scss']
})
export class RecibosComponent  {
  login;
  recibo;
  endpoint = 'http://www.bcfadm.com.br/bcfnet/boleto/';
  resource;
  @ViewChild('recibo') $recibo: ElementRef;

  constructor(
    private spinnerService: SpinnerService,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer
  ) {
    this.route.params.subscribe(params => {
      this.recibo = params.recibo;
      this.login = params.login;

      this.buildReciboUrl(this.login, this.recibo);
    });
  }

  exportAsPDF() {
    const params = `?login=${this.login}&recibo=${this.recibo}`;
    const url =  encodeURIComponent(this.endpoint + params).replace('https', 'http');

    window.open(`http://html2pdf-248222579.sa-east-1.elb.amazonaws.com/?url=${url}&margin=5mm&format=Legal&download=false&filename=Recibo_BCFNet`);
  }

  downloadFile() {
    const params = `?login=${this.login}&recibo=${this.recibo}`;
    const url =  encodeURIComponent(this.endpoint + params).replace('https', 'http');

    window.open(`http://html2pdf-248222579.sa-east-1.elb.amazonaws.com/?url=${url}&margin=5mm&format=Legal&download=true&filename=Recibo_BCFNet`);
  }

  buildReciboUrl(login, recibo) {
    const params = `?login=${login}&recibo=${recibo}`;

    this.resource = this.sanitizer.bypassSecurityTrustResourceUrl(
      this.endpoint + params
    );
    return this.resource;
  }

}
