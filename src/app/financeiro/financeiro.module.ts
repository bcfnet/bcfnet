import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FinanceiroRoutingModule } from './financeiro-routing.module';
import { CondominioComponent } from './condominio/condominio.component';
import { SegundaViaDeBoletosComponent } from './segunda-via-de-boletos/segunda-via-de-boletos.component';
import { SharedModule } from '../shared/shared.module';
import { AnaliticoComponent } from './balancete/analitico/analitico.component';
import { RecpagComponent } from './balancete/recpag/recpag.component';
import { SinteticoComponent } from './balancete/sintetico/sintetico.component';
import { ContasAPagarComponent } from './contas-a-pagar/contas-a-pagar.component';
import { RecibosComponent } from './recibos/recibos/recibos.component';
import { SemiSinteticoComponent } from './balancete/semi-sintetico/semi-sintetico.component';
import { SaldoAtualizadoComponent } from './demonstratitvos/saldo-atualizado/saldo-atualizado.component';
import { ComparativoMesAnteriorComponent } from './demonstratitvos/comparativo-mes-anterior/comparativo-mes-anterior.component';
import { ComparativoNoPeriodoComponent } from './demonstratitvos/comparativo-no-periodo/comparativo-no-periodo.component';
import { ContaCorrenteComponent } from './extrato/conta-corrente/conta-corrente.component';
import { CotasEmAbertoComponent } from './devedores/cotas-em-aberto/cotas-em-aberto.component';
import { DevedoresComReajusteComponent } from './devedores/devedores-com-reajuste/devedores-com-reajuste.component';
import { BalanceteAnualComponent } from './balancete/balancete-anual/balancete-anual.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FinanceiroRoutingModule
  ],
  declarations: [
    CondominioComponent,
    SegundaViaDeBoletosComponent,
    DevedoresComReajusteComponent,
    AnaliticoComponent,
    RecpagComponent,
    SinteticoComponent,
    ContaCorrenteComponent,
    ContasAPagarComponent,
    RecibosComponent,
    SemiSinteticoComponent,
    SaldoAtualizadoComponent,
    ComparativoMesAnteriorComponent,
    ComparativoNoPeriodoComponent,
    ContaCorrenteComponent,
    CotasEmAbertoComponent,
    BalanceteAnualComponent
  ]
})
export class FinanceiroModule { }
