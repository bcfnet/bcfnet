import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DevedoresComReajusteComponent } from './devedores-com-reajuste.component';

describe('DevedoresComReajusteComponent', () => {
  let component: DevedoresComReajusteComponent;
  let fixture: ComponentFixture<DevedoresComReajusteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DevedoresComReajusteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DevedoresComReajusteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
