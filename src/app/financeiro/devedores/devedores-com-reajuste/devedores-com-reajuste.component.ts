import { Component, OnInit } from '@angular/core';
import { DevedoresReajusteService } from '../../../shared/services/devedores_reajuste.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';

@Component({
  selector: 'app-devedores-com-reajuste',
  templateUrl: './devedores-com-reajuste.component.html',
  styleUrls: ['./devedores-com-reajuste.component.scss']
})
export class DevedoresComReajusteComponent implements OnInit {

  public data = [];
  public periodo;
  public loading = false;

  constructor(
    private devedoresService: DevedoresReajusteService,
    private spinnerService: SpinnerService
  ) {
    this.data = [];
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.devedoresService.get(this.periodo ? this.periodo[0] : new Date().getMonth() + 1,
      this.periodo ? this.periodo[1] : new Date().getFullYear())
    .subscribe((data: any) => {
      this.data = data;
      console.log(data);
      this.loading = true;
      this.spinnerService.hide();
    });
  }

  onChange(periodo) {
    this.periodo = periodo.split('/');
    this.get();
  }

}
