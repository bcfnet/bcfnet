import { Component, OnInit } from '@angular/core';
import { DevedoresService } from '../../../shared/services/devedores.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';

@Component({
  selector: 'app-cotas-em-aberto',
  templateUrl: './cotas-em-aberto.component.html',
  styleUrls: ['./cotas-em-aberto.component.scss']
})
export class CotasEmAbertoComponent implements OnInit {

  public data = [];
  public periodo;
  public loading = false;

  constructor(
    private devedoresService: DevedoresService,
    private spinnerService: SpinnerService
  ) {
    this.data = [];
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this.spinnerService.show();

    this.devedoresService.get(this.periodo ? this.periodo[0] : new Date().getMonth() + 1,
      this.periodo ? this.periodo[1] : new Date().getFullYear())
    .subscribe((data: any) => {
      this.data = data.filter((item) => {
        if( item.Campo1.trim() == "" && item.Campo2.trim() == "" && item.Campo3.trim() == "" && item.Campo4.trim() == "" && item.Campo5.trim() == "") return false;

        return item;
      });
      // console.table(this.data);
      this.loading = true;
      this.spinnerService.hide();
    });
  }

  onChange(periodo) {
    this.periodo = periodo.split('/');
    this.get();
  }

}
