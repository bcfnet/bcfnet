import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CotasEmAbertoComponent } from './cotas-em-aberto.component';

describe('CotasEmAbertoComponent', () => {
  let component: CotasEmAbertoComponent;
  let fixture: ComponentFixture<CotasEmAbertoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CotasEmAbertoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CotasEmAbertoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
