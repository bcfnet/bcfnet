import { Component, OnInit } from '@angular/core';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { ApiService } from '../../../shared/services/api.service';
import { PermissionsService } from '../../../shared/services/permissions.service';
import { UserService } from '../../../shared/services/user.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-conta-corrente',
  templateUrl: './conta-corrente.component.html',
  styleUrls: ['./conta-corrente.component.scss']
})
export class ContaCorrenteComponent implements OnInit {

    public data = [];
    public periodo;
    public NomeCliente;
    public loading = true;

    constructor(
        public utils: UtilsService,
        private apiService: ApiService,
        private userService: UserService,
        private spinnerService: SpinnerService,
        private permissionService: PermissionsService
    ) {
        this.data = [];
    }

    ngOnInit() {
        this.get();
    }

    get() {
        this.spinnerService.show();
        const api = this.permissionService.isProprietario() ?
        '/financeiro/extrato_conta_corrente_locacao' : '/financeiro/extrato_conta_corrente_condominio';

        this.apiService.get(api, {
            month: this.periodo ? this.periodo[0] : new Date().getMonth() + 1,
            year: this.periodo ? this.periodo[1] : new Date().getFullYear()
        })
        .subscribe((data: any) => {
            this.data = data;
            if (this.permissionService.isProprietario()) {
                this.NomeCliente = this.data[0];
            }
            // console.table(this.data);
            this.loading = false;
            this.spinnerService.hide();
        }, (error) => {
            this.loading = false;
            this.spinnerService.hide();
        });
    }

    onChange(periodo) {
        this.periodo = periodo.split('/');
        this.get();
    }

}
