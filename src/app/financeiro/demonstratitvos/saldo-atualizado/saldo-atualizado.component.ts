import { Component, OnInit } from '@angular/core';
import { SaldoAtualizadoService } from '../../../shared/services/saldo_atualizado.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-saldo-atualizado',
  templateUrl: './saldo-atualizado.component.html',
  styleUrls: ['./saldo-atualizado.component.scss'],
  providers: [SaldoAtualizadoService]
})
export class SaldoAtualizadoComponent implements OnInit {

  public data = [];
  public loading = true;

  constructor(
    private saldoAtualizadoService: SaldoAtualizadoService,
    private spinnerService: SpinnerService,
    private utils: UtilsService
  ) {
    this.data = [];
  }

  ngOnInit() {
    this.get();
    this.spinnerService.display(true);
  }

  get() {
    this.saldoAtualizadoService.get(5, 2018)
    .subscribe((data: any) => {
      this.data = data;
      console.log(data);
      this.spinnerService.display(false);
      this.loading = false;
      // console.table(data);
    }, () => {
        this.loading = false;
    });
  }

}
