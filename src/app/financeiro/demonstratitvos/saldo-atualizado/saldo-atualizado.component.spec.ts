import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaldoAtualizadoComponent } from './saldo-atualizado.component';

describe('SaldoAtualizadoComponent', () => {
  let component: SaldoAtualizadoComponent;
  let fixture: ComponentFixture<SaldoAtualizadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaldoAtualizadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaldoAtualizadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
