import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ApiService } from '../../../shared/services/api.service';
import { SpinnerService } from '../../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../../shared/services/utils.service';

@Component({
  selector: 'app-comparativo-mes-anterior',
  templateUrl: './comparativo-mes-anterior.component.html',
  styleUrls: ['./comparativo-mes-anterior.component.scss']
})
export class ComparativoMesAnteriorComponent implements OnInit {

  public data = [];
  public loading = true;

  constructor(
      public utils: UtilsService,
    private apiService: ApiService,
    private spinnerService: SpinnerService
  ) {
    this.data = [];
  }

  ngOnInit() {
    this.get();
    this.spinnerService.display(true);
  }

  get() {
    this.apiService.get('/financeiro/balancete_comparativo', {
      month: moment().subtract('1', 'month').format('MM'),
      year: moment().format('YYYY')
    })
    .subscribe((data: any) => {
      this.data = data.filter((item: any) => {
        if(item.Campo1 == "" && item.Campo2 == "" && item.Campo3 == "" && item.Campo4 == "" && item.Campo5 == "") return false;
        if(item.Campo1 == "" && item.Campo2 == " C" && item.Campo3 == "" && item.Campo4 == "" && item.Campo5 == " C") return false;
        return item;
      });
      this.spinnerService.display(false);
      this.loading = false;
      
    }, () => {
        this.loading = false;
    });
  }

}
