import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparativoMesAnteriorComponent } from './comparativo-mes-anterior.component';

describe('ComparativoMesAnteriorComponent', () => {
  let component: ComparativoMesAnteriorComponent;
  let fixture: ComponentFixture<ComparativoMesAnteriorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparativoMesAnteriorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparativoMesAnteriorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
