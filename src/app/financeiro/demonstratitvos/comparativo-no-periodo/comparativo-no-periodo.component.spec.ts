import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparativoNoPeriodoComponent } from './comparativo-no-periodo.component';

describe('ComparativoNoPeriodoComponent', () => {
  let component: ComparativoNoPeriodoComponent;
  let fixture: ComponentFixture<ComparativoNoPeriodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComparativoNoPeriodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparativoNoPeriodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
