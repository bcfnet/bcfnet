import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-condominio',
  templateUrl: './condominio.component.html',
  styleUrls: ['./condominio.component.scss']
})
export class CondominioComponent implements OnInit {

  optGroupConfig: any = {
    maxItems: 1,
    optgroupField: 'class',
    labelField: 'name',
    searchField: ['name'],
    render: {
      optgroup_header: function (data, escape) {
        return '<div class="optgroup-header">' +
          escape(data.label) +
          '</span></div>';
      }
    },
    optgroups: [
      { value: 'administrativo', label: 'Administrativo' },
      { value: 'departamento-pessoal', label: 'Departamento Pessoal' },
      { value: 'financeiro', label: 'Financeiro' },
      { value: 'juridico', label: 'Jurídico' }
    ]
  };

  optGroupOptions: any = [
    /*{class: 'administrativo', value: 'atas de assembleia', name: 'Atas de Assembléia' },
    {class: 'administrativo', value: 'convocacao-de-assembleias', name: 'Convocação de Assembléias' },
    {class: 'departamento-pessoal', value: 'departamento pessoal', name: 'Departamento Pessoal' },*/
    { class: 'financeiro', value: 'Acompanhamento de Processos', name: 'Acompanhamento de Processos' },
    { class: 'financeiro', value: 'balancete analitico', name: 'Balancete Analítico' },
    { class: 'financeiro', value: 'Balancete Rec/Pag Operaciona', name: 'Balancete Rec/Pag Operaciona' },
    { class: 'financeiro', value: 'Balancete Rec/Pag', name: 'Balancete Rec/Pag' },
    { class: 'financeiro', value: 'Balancete Sintetico', name: 'Balancete Sintetico' },
    { class: 'financeiro', value: 'Contas a Pagar', name: 'Contas a Pagar' },
    { class: 'financeiro', value: 'Demonstrativos', name: 'Demonstrativos' },
    { class: 'financeiro', value: 'Devedores', name: 'Devedores' },
    { class: 'financeiro', value: 'Extrato de Conta Corrente', name: 'Extrato de Conta Corrente' }

  ];

  optGroupValue: string[] = ['-- selecione --'];

  constructor() { }

  ngOnInit() {
  }

}
