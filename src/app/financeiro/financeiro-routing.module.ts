import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CondominioComponent } from './condominio/condominio.component';
import { SegundaViaDeBoletosComponent } from './segunda-via-de-boletos/segunda-via-de-boletos.component';
import { AnaliticoComponent } from './balancete/analitico/analitico.component';
import { RecpagComponent } from './balancete/recpag/recpag.component';
import { SinteticoComponent } from './balancete/sintetico/sintetico.component';
import { ContasAPagarComponent } from './contas-a-pagar/contas-a-pagar.component';
import { RecibosComponent } from './recibos/recibos/recibos.component';
import { SemiSinteticoComponent } from './balancete/semi-sintetico/semi-sintetico.component';
import { ComparativoMesAnteriorComponent } from './demonstratitvos/comparativo-mes-anterior/comparativo-mes-anterior.component';
import { ComparativoNoPeriodoComponent } from './demonstratitvos/comparativo-no-periodo/comparativo-no-periodo.component';
import { SaldoAtualizadoComponent } from './demonstratitvos/saldo-atualizado/saldo-atualizado.component';
import { ContaCorrenteComponent } from './extrato/conta-corrente/conta-corrente.component';
import { CotasEmAbertoComponent } from './devedores/cotas-em-aberto/cotas-em-aberto.component';
import { DevedoresComReajusteComponent } from './devedores/devedores-com-reajuste/devedores-com-reajuste.component';
import { BalanceteAnualComponent } from './balancete/balancete-anual/balancete-anual.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'condominio'
  },
  {
    path: 'balancete/analitico',
    component: AnaliticoComponent
  },
  {
    path: 'balancete/rec-pag',
    component: RecpagComponent
  },
  {
    path: 'balancete/sintetico',
    component: SinteticoComponent
  },
  {
    path: 'balancete/anual',
    component: BalanceteAnualComponent
  },
  {
    path: 'balancete/semi_sintetico/:tipo',
    component: SemiSinteticoComponent
  },
  {
    path: 'contas-a-pagar',
    component: ContasAPagarComponent
  },
  {
    path: 'condominio',
    component: CondominioComponent
  },
  {
    path: 'segunda-via-de-boletos',
    component: SegundaViaDeBoletosComponent
  },
  {
    path: 'recibos/:login/:recibo',
    component: RecibosComponent
  },
  {
    path: 'devedores-com-reajuste-para-unidade',
    component: DevedoresComReajusteComponent
  },
  {
    path: 'extrato/conta-corrente',
    component: ContaCorrenteComponent
  },
  {
    path: 'devedores',
    children: [
      { path: '', redirectTo: 'cotas-em-aberto', pathMatch: 'full' },
      { path: 'cotas-em-aberto', component: CotasEmAbertoComponent },
      { path: 'devedores-com-reajuste', component: DevedoresComReajusteComponent },
    ]
  },
  {
    path: 'demonstrativos',
    children: [
      { path: '', redirectTo: 'comparativo-mes-anterior', pathMatch: 'full' },
      { path: 'comparativo-mes-anterior', component: ComparativoMesAnteriorComponent },
      { path: 'no-periodo', component: ComparativoNoPeriodoComponent },
      { path: 'saldo-atualizado', component: SaldoAtualizadoComponent }
  ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceiroRoutingModule { }
