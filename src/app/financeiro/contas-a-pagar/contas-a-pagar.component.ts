import { Component, OnInit } from '@angular/core';
import { ContasAPagarService } from '../../shared/services/contas_a_pagar.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { UtilsService } from '../../shared/services/utils.service';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-contas-a-pagar',
  templateUrl: './contas-a-pagar.component.html',
  styleUrls: ['./contas-a-pagar.component.scss'],
  providers: [ContasAPagarService]
})
export class ContasAPagarComponent implements OnInit {

    public url: SafeResourceUrl;
    public periodo;
    public loading = true;
    public identifier;
    private url_position;
    private urls = [];
    public data = [];
    private base = 'https://www.bcfadm.com.br/bcfnet/pageflipper/?url=';

    constructor(
        private contasAPagarService: ContasAPagarService,
        private spinnerService: SpinnerService,
        private utils: UtilsService,
        private sanitizer: DomSanitizer
    ) {
        this.data = [];
    }

    ngOnInit() {
        this.get();
        this.spinnerService.display(true);
    }

    open(balancete) {
        if (!balancete.URL || balancete.URL === ' ') {
            return;
        }
        this.loading = true;

        let url = balancete.URL;
        url = this.base + this.utils.base64Encode(url);
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url);

        setTimeout(() => {
            this.url_position = this.urls.findIndex((value) => {
                return value.url === url;
            });
            this.loading = false;
        }, 300);
    }

    next() {
        const limit = this.urls.length - 1;

        if (this.url_position < limit) {
            this.url_position++;
        } else {
            this.url_position = 0;
        }
        
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urls[this.url_position].url);
    }

    previous() {
        const limit = this.urls.length - 1;

        if (this.url_position != 0) {
            this.url_position--;            
        } else {
            this.url_position = limit;            
        }
        
        this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.urls[this.url_position].url);
    }

    get() {
        this.contasAPagarService.get()
            .subscribe((result: any) => {
                this.data = result.map((data) => {
                    if (data.URL) {
                        const url: any = this.base + this.utils.base64Encode(data.URL);
                        this.urls.push({
                            title: data.Historico,
                            url: url
                        });
                    }
                    return data;
                });
                this.spinnerService.display(false);
            });
    }

    getModalTitle() {
        const balancete = this.urls[this.url_position];

        if (balancete) {
            return balancete.title;
        }
        return;
    }
}
