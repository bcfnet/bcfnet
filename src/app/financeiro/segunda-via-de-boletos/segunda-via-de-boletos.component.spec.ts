import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SegundaViaDeBoletosComponent } from './segunda-via-de-boletos.component';

describe('SegundaViaDeBoletosComponent', () => {
  let component: SegundaViaDeBoletosComponent;
  let fixture: ComponentFixture<SegundaViaDeBoletosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SegundaViaDeBoletosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SegundaViaDeBoletosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
