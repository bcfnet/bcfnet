import { Component, OnInit } from '@angular/core';
import { RecibosService } from '../../shared/services/recibos.service';
import { ApiService } from '../../shared/services/api.service';
import { Unidade } from '../../shared/models/unidade';
import { Router } from '@angular/router';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { PermissionsService } from '../../shared/services/permissions.service';
import { PagerService } from 'src/app/shared/services/pager.service';
import * as moment from 'moment';

@Component({
  selector: 'app-segunda-via-de-boletos',
  templateUrl: './segunda-via-de-boletos.component.html',
  styleUrls: ['./segunda-via-de-boletos.component.scss'],
  providers: [RecibosService]
})
export class SegundaViaDeBoletosComponent implements OnInit {
    unidades;
    recibos;
    datas = [];
    $datas = [];
    endpoint = 'https://www.bcfadm.com.br/bcfnet/boleto/';
    loading = true;
    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[] = [];
    private searchable = [];
    private allItems: any[] = [];

    constructor(
        private recibosService: RecibosService,
        private spinnerService: SpinnerService,
        private apiService: ApiService,
        private permissionService: PermissionsService,
        private pagerService: PagerService,
        private router: Router
    ) {
        this.recibos = [];
    }

    ngOnInit() {
        this.spinnerService.display(true);
        this.recibosService.get()
        .subscribe(recibos => {
            this.recibos = recibos;
            const data = this.permissionService.isSindico() ? {} : { login: this.permissionService.getCondominioLogin() };

            if (this.recibos.length > 0) {
            this.apiService.get('/cadastro/retorna_unidades', data)
                .subscribe((unidades: any) => {
                    this.unidades = unidades;
                    /*this.unidades.map(unidade => {
                        if (this.recibos.filter((recibo) => recibo.Complemento == unidade.Complemento_Unidade).length > 0) {
                            const obj = {
                                unidade: unidade,
                                recibo: this.recibos.filter((recibo) => {
                                    return recibo.Complemento == unidade.Complemento_Unidade;
                                })[0]
                            };
                            this.datas.push(obj);
                            this.allItems.push(obj);
                        }
                    });*/
                    this.recibos.map(recibo => {
                        if (this.unidades.filter((unidade) => recibo.Complemento == unidade.Complemento_Unidade).length > 0) {
                            const obj = {
                                recibo: recibo,
                                unidade: this.unidades.filter((unidade) => {
                                    return recibo.Complemento == unidade.Complemento_Unidade;
                                })[0]
                            };
                            this.datas.push(obj);
                            this.allItems.push(obj);
                        }
                    });

                    this.$datas = this.datas;
                    this.setPage(1);
                    this.spinnerService.display(false);
                });
            } else {
            this.spinnerService.display(false);
            }
            this.loading = false;
        });
    }

    formatToDate(str) {
        if (str === '') {
            return;
        }
        str = str.substring(0, str.length - 6);
        return moment(str).format('DD/MM/YYYY');
    }

    setPage(page: number) {
        this.pager = this.pagerService.getPager(this.allItems.length, page, 5);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    open(login, recibo) {
        this.router.navigate([`/financeiro/recibos/${login}/${recibo}`]);
    }

    exportAsPDF(login, recibo) {
        const params = `?login=${login}&recibo=${recibo}`;
        const url =  encodeURIComponent(this.endpoint + params).replace('https', 'http');

        window.open(`http://html2pdf-248222579.sa-east-1.elb.amazonaws.com/?url=${url}&margin=5mm&format=Legal&download=false&filename=Recibo_BCFNet`);
    }

    downloadFile(login, recibo) {
        const params = `?login=${login}&recibo=${recibo}`;
        const url =  encodeURIComponent(this.endpoint + params).replace('https', 'http');

        window.open(`http://html2pdf-248222579.sa-east-1.elb.amazonaws.com/?url=${url}&margin=5mm&format=Legal&download=true&filename=Recibo_BCFNet`);
    }

    searchChange(text) {
        if (!text) {
            this.allItems = this.$datas;
            this.setPage(1);
            this.loading = false;
            return this.datas;
        }

        const search = text.toLowerCase();

        const temp = this.$datas.filter((d) => {
            return (d.unidade.Complemento_Unidade.toLowerCase().indexOf(search) !== -1 ||
                d.unidade.Nome_do_Cliente.toLowerCase().indexOf(search) !== -1) || !search;
        });
        this.allItems = temp;

        this.setPage(1);

        if (this.datas.length === 0) {
            this.loading = false;
        }
    }
}
