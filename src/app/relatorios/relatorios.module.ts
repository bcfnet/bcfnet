import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTablesModule } from 'angular-datatables';

import { RelatoriosRoutingModule } from './relatorios-routing.module';
import { RelatorioDeCondominoComponent } from './relatorio-de-condomino/relatorio-de-condomino.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RelatoriosRoutingModule,
    SharedModule,
    DataTablesModule
  ],
  declarations: [RelatorioDeCondominoComponent]
})
export class RelatoriosModule { }
