import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RelatorioDeCondominoComponent } from './relatorio-de-condomino/relatorio-de-condomino.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'relatorio-de-condomino',
    pathMatch: 'full',
  },
  {
    path: 'relatorio-de-condomino',
    component: RelatorioDeCondominoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RelatoriosRoutingModule { }
