import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RelatorioDeCondominoComponent } from './relatorio-de-condomino.component';

describe('RelatorioDeCondominoComponent', () => {
  let component: RelatorioDeCondominoComponent;
  let fixture: ComponentFixture<RelatorioDeCondominoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RelatorioDeCondominoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RelatorioDeCondominoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
