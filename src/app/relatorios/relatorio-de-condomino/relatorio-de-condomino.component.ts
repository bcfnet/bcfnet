import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { Router } from "@angular/router";
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import { ApiService } from "../../shared/services/api.service";
import { Unidade } from "../../shared/models/unidade";
import { UserService } from "../../shared/services/user.service";
import { UtilsService } from "../../shared/services/utils.service";
import { SpinnerService } from "../../shared/components/spinner/spinner.service";
import { exportToCSV } from "src/app/helpers";
import { PermissionsService } from "src/app/shared/services/permissions.service";
import { PagerService } from 'src/app/shared/services/pager.service';
import { Subject } from 'rxjs';

@Component({
    selector: "app-relatorio-de-condomino",
    templateUrl: "./relatorio-de-condomino.component.html",
    styleUrls: ["./relatorio-de-condomino.component.scss"],
    providers: [ApiService, UserService]
})
export class RelatorioDeCondominoComponent implements OnInit {
    public dtOptions: DataTables.Settings = {};

    loading = true;
    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[] = [];
    private searchable = [];
    // array of all items to be paged
    private allItems: any[];

    dtTrigger: Subject<any> = new Subject();

    unidade: Unidade;
    unidades: any[] = [];
    @ViewChild('relatorio') relatorio: ElementRef;

    constructor(
        public permissionService: PermissionsService,
        public utils: UtilsService,
        private apiService: ApiService,
        private userService: UserService,
        private spinnerService: SpinnerService,
        private pagerService: PagerService,
        private router: Router
    ) {}

    ngOnInit() {
        const login = this.utils.getToken();

        this.spinnerService.show();

        this.apiService.get('/cadastro/retorna_unidades', {
            login: login
        })
        .subscribe((result: any) => {
            this.allItems = result;
            this.searchable = this.allItems;
            // initialize to page 1
            this.setPage(1);
            this.loading = false;

            this.dtTrigger.next();

            this.unidades = result;

            this.unidade = result[0];
            this.unidade.EnderecoCondominio = this.utils.utf8_decode(
                this.unidade.EnderecoCondominio
            );
            this.spinnerService.hide();
        });

        this.dtOptions = {
            pagingType: 'full_numbers',
            lengthChange: false,
            searching: true,
            info: false,
            ordering: false,
            paging: true,
            language: {
                'search': 'Filtrar por palavra-chave',
                'loadingRecords': 'Carregando...',
                'processing':     'Processando...',
                'paginate': {
                    'first':      'Primeiro',
                    'last':       'Último',
                    'next':       'Próximo',
                    'previous':   'Anterior'
                }
            }
        };
    }

    setPage(page: number) {
        this.pager = this.pagerService.getPager(this.allItems.length, page, 1000);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }

    exportAsCSV() {
        if (this.unidades.length) {
            exportToCSV(this.router.url + '.csv', this.unidades);
        }
    }

    exportAsPDF() {
        const balancete = this.relatorio.nativeElement;
        this.spinnerService.display(true);

        html2canvas(balancete).then((canvas) => {
        const pdf = new jsPDF('p', 'pt', 'a4');
        const $height = 1300;
        const $width = 1140;

        for (var i = 0; i <= balancete.clientHeight / $height; i++) {
            //! This is all just html2canvas stuff
            var srcImg  = canvas;
            var sX      = 0;
            var sY      = $height * i; // start 980 pixels down for every new page
            var sWidth  = $width;
            var sHeight = $height;
            var dX      = 0;
            var dY      = 0;
            var dWidth  = $width;
            var dHeight = $height;

            const onePageCanvas = document.createElement("canvas");
            onePageCanvas.setAttribute('width', `${$width}`);
            onePageCanvas.setAttribute('height', `${$height}`);
            var ctx = onePageCanvas.getContext('2d');
            ctx.fillStyle = '#FFFFFF';
            // details on this usage of this function: 
            // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
            ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);

            var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

            var width         = onePageCanvas.width;
            var height        = onePageCanvas.clientHeight;

            //! If we're on anything other than the first page,
            // add another page
            if (i > 0) {
                pdf.addPage([595.28, 841.89]); //8.5" x 11" in pts (in*72)
            }
            //! now we declare that we're working on that page
            pdf.setPage(i + 1);
            //! now we add content to that page!
            pdf.addImage(canvasDataURL, 'PNG', 0, 0, (width * .62), (height * .62), `IMAGE${i}`, 'fast');

            }
            pdf.save(`balancete_${new Date().getTime()}.pdf`); // todo: mudar nome para balancete_analitico_10_2010_timestamp.pdf
            this.spinnerService.hide();
        });
    }
}
