import { Component } from '@angular/core';
import { UserService } from './shared/services/user.service';

import '../assets/scss/app.scss';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(
    private userService: UserService
  ) {

  }

  isLoggedIn() {
    if (this.userService.getUserToken()) {
      return true;
    }
    return false;
  }
}
