import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class CondominioService {

    private path = '/condominio/condominios';

    constructor(
        private apiService: ApiService,
        private userService: UserService
    ) { }

    get() {
        return this.apiService.get(this.path);
    }

    compare(a, b) {
        if (a.login_internet < b.login_internet) {
            return -1;
        }

        if (a.login_internet > b.login_internet) {
            return 1;
        }
        return 0;
    }
}