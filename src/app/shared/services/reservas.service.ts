import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PermissionsService } from './permissions.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ReservasService {

  private path = '/reserva';

  constructor(
    private apiService: ApiService,
    private userService: UserService,
    private permissionService: PermissionsService
  ) { }

  get(route: string, data: any = []) {
    const login = this.permissionService.isSindico() ? this.permissionService.getCondominioLogin() : this.userService.getUserLogin();

    return this.apiService.get(`${this.path}/${route}`, { ...{ login }, ...data });
  }

  create(route: string, data: any) {
    const login = this.permissionService.isSindico() ? this.userService.getUserLogin() : this.permissionService.getCondominioLogin();

    return this.apiService.post(`${this.path}/${route}/create`, { ...{ login }, ...data });
  }

  update(route: string, data: any) {
    const login = this.permissionService.isSindico() ? this.userService.getUserLogin() : this.permissionService.getCondominioLogin();

    return this.apiService.post(`${this.path}/${route}/update`, { ...{ login }, ...data });
  }
}
