import { TestBed, inject } from '@angular/core/testing';

import { DevedoresReajusteService } from './devedores_reajuste.service';

describe('DevedoresReajusteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DevedoresReajusteService]
    });
  });

  it('should be created', inject([DevedoresReajusteService], (service: DevedoresReajusteService) => {
    expect(service).toBeTruthy();
  }));
});
