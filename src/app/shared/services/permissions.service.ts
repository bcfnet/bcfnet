import { Injectable } from '@angular/core';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {

  private roles = [
    'BCFADM', // começa com 0
    'SINDICO', // começa com 1 conhecido como síndico
    'CONDOMINO', // começa com 2
    'PROPRIETARIO', // começa com 3
    'INQUILINO', // começa com 4
    'GERENTE' // começa com 5
  ];

  constructor(
    private usersService: UserService
  ) { }

  identify(login: string, role: string) {
    const char = login.substring(0, 1);

    if (role !== '') {
      // todo: melhorar depois
      this.setRole(role);
    } else {
      this.setRole(char);
    }
  }

  getRole() {
    return localStorage.getItem('BCFNET.role');
  }

  setRole (position) {
    localStorage.setItem('BCFNET.role', this.roles[
      parseInt(position, 10)
    ]);
  }

  getCondominioLogin() {
    const login = this.usersService.getUserLogin();
    return [1, login.substring(1, 5)].join('');
  }

  isAdmin() {
    return this.getRole() === this.roles[0];
  }

  isSindico() {
    return this.getRole() === this.roles[1];
  }

  isCondomino() {
    return this.getRole() === this.roles[2];
  }

  isGerente() {
    return this.getRole() === this.roles[5];
  }

  isProprietario() {
    return this.getRole() === this.roles[3];
  }

  isLocatario() {
    return this.getRole() === this.roles[4];
  }
}
