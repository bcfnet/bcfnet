import { TestBed, inject } from '@angular/core/testing';

import { BalanceteRecPagService } from './balancete_rec_pag.service';

describe('BalanceteRecPagService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BalanceteRecPagService]
    });
  });

  it('should be created', inject([BalanceteRecPagService], (service: BalanceteRecPagService) => {
    expect(service).toBeTruthy();
  }));
});
