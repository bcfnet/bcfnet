import { TestBed, inject } from '@angular/core/testing';

import { DevedoresService } from './devedores.service';

describe('DevedoresService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DevedoresService]
    });
  });

  it('should be created', inject([DevedoresService], (service: DevedoresService) => {
    expect(service).toBeTruthy();
  }));
});
