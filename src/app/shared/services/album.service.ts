import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Album } from '../models/album';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  private endpoint = '/condominio/album';

  constructor(
    private apiService: ApiService
  ) { }

  create(data: any) {
    return this.apiService.post(`${this.endpoint}/create`, data);
  }

  update(id: string, arquivo: Album) {}

  get(login: string = null) {
    if (login == null) {
      return this.apiService.get(`${this.endpoint}`);
    } else {
      return this.apiService.get(`${this.endpoint}`, {
        login: login
      });
    }
  }

  delete(id: number) {
    return this.apiService.post(`${this.endpoint}/delete`, {
      id: id
    });
  }
}
