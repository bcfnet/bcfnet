import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { PermissionsService } from './permissions.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

    constructor(
        private permissionService: PermissionsService,
        private userService: UserService
    ) {
        moment().locale('pt-BR');
    }

    base64Encode(str, encoding = 'utf-8') {
        return btoa(str);
    }

    base64Decode(str, encoding = 'utf-8') {
        return atob(str);
    }

    convertToNumber(string) {
        return parseInt(string, 10);
    }

    convertToUTF8(string: string) {
        return decodeURIComponent(escape(string));
    }

    utf8_encode(s) {
        for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
			s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
		);
		return s.join("");
    }

    utf8_decode(s) {
        for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
            ((a = s[i][c](0)) & 0x80) &&
            (s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
            o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
        );
        return s.join("");
    }

    empty(string) {
        return string === undefined || string === ' ' || string === '' || string === null;
    }

    capitalize(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }

    getRangeOfDates(start, end, key, arr = [start.startOf(key)]) {
        if (start.isAfter(end)) {
        throw new Error('start must precede end');
        }

        const next = moment(start).add(1, key).startOf(key);

        if (next.isAfter(end, key)) {
        return arr;
        }

        return this.getRangeOfDates(next, end, key, arr.concat({
            'date': next.format('MM/YYYY'),
            'display': next.format('MMMM/YYYY')
        }));
    }

    getToken() {
        let login = this.userService.getUserLogin();

        if (this.permissionService.isCondomino() || this.permissionService.isGerente()) {
        login = this.permissionService.getCondominioLogin();
        }

        return login;
    }

    getBaseUrl() {
        return environment.apiHost;
    }

    numberWithCommas(x) {
        x = x.toString();
        const pattern = /(-?\d+)(\d{3})/;
        while (pattern.test(x)) {
            x = x.replace(pattern, '$1,$2');
        }
        return x;
    }

    timeout(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async sleep(time = 3000, fn, ...args) {
        await this.timeout(time);
        return fn(...args);
    }

    isString(value) {
        value = parseFloat(value);
        return isNaN(value);
    }
}
