import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { Subject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class UserService {
  private user: User;
  private userToken: string;
  private userLogin: string;
  private userSubject: Subject<User> = new Subject<User>();

  constructor(
    private http: HttpClient
  ) {
    this.detectBrowserUserToken();
    this.detectBrowserUserLogin();
  }

  detectBrowserUserToken() {
    const userToken = localStorage.getItem('userToken');
    this.setUserToken(userToken);
  }

  detectBrowserUserLogin() {
    const userLogin = localStorage.getItem('userLogin');
    this.setUserLogin(userLogin);
  }

  getUserToken() {
    return this.userToken;
  }

  setUserToken(userToken: string) {
    if (userToken) {
      localStorage.setItem('userToken', userToken);
      return this.userToken = userToken;
    }

    localStorage.removeItem('userToken');
    this.userToken = null;
  }

  setUserLogin(userLogin: string) {
    if (userLogin) {
      localStorage.setItem('userLogin', userLogin);
      return this.userLogin = userLogin;
    }

    localStorage.removeItem('userLogin');
    this.userLogin = null;
  }

  getUserLogin() {
    return this.userLogin;
  }

  getUser() {
    return this.user;
  }

  getUserSubject() {
    return this.userSubject;
  }

  setUser(user: User) {
    this.user = user;
    this.userSubject.next(this.user);
  }
  /*getStatusList(): Status[] {
    return [
      new Status('Ativo', 'check', 'green', 'active'),
      new Status('Convidado', 'mail-2', 'blue', 'invited'),
      new Status('Desativado', 'times', 'red', 'disabled'),
    ];
  } */
}
