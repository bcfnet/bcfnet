import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ContasAPagarService {

  private path = '/financeiro/contas_a_pagar';

  constructor(
    private apiService: ApiService
  ) { }

  get() {
    return this.apiService.get(this.path);
  }
}
