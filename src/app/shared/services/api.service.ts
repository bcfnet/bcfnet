import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { UserService } from './user.service';
import { PermissionsService } from './permissions.service';

@Injectable()
export class ApiService {

  private headers = new Headers(
    {'Content-Type': 'application/x-www-form-urlencoded'}
  );

  constructor(
    private http: HttpClient,
    private userService: UserService,
    private permissionService: PermissionsService,
    private router: Router) {}

  get(path, query?: any) {
    return this.http.get(environment.apiHost + path, this.generateRequestOptions(query))
      .pipe(
        catchError(this.handleError('get'))
      );
  }

  post(path, body?, query?) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' });
    const login = this.userService.getUserLogin();
    const token = this.userService.getUserToken();

    const options = {
      headers: headers
    };

    return this.http.post(environment.apiHost + path + `?login=${login}&token=${token}`, this.httpQueryBuilder(body), options)
      .pipe(
        tap((data: any) => this.log(`added hero w/ id=${data}`)),
        catchError(this.handleError('post'))
      );
  }

  put(path, body?, query?) {
    return this.http.put(environment.apiHost + path, body, this.generateRequestOptions(query))
      .pipe(
        catchError(this.handleError('put'))
      );
  }

  delete(path?) {
    return this.http.delete(environment.apiHost + path, this.generateRequestOptions())
      .pipe(
        catchError(this.handleError('delete'))
      );
  }

  open(path: string): void {
    return window.location.assign(environment.apiHost + path + '?token=' + this.userService.getUserToken());
  }

  login(login: string, password: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    });

    const body = `login=${login}&password=${password}`;

    return this.http.post(environment.apiHost + '/login', body, {
      headers: headers,
    })
    .pipe(
      map((response: any) => {
        if (response) {
          if (response.success === '1') {
            localStorage.setItem('account', JSON.stringify(response));
            localStorage.setItem('userToken', response.token);
            localStorage.setItem('userLogin', login);
            this.permissionService.identify(login, response.permissao);
            return response;
          } else {
            return false;
          }
        } else {
          return false;
        }
      }),
      catchError( this.handleError() )
    );
  }

  private generateRequestOptions(query: any = {}) {
    const headers = new HttpHeaders({
      'Content-Type': 'text/plain'
    });

    const options = {
      headers: headers,
      params: Object.assign({}, {
        token: this.userService.getUserToken(),
        login: this.userService.getUserLogin()
      }, query)
    };

    return options;
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private handleAuthenticationError() {
    const routeUrl = this.router.url;

    console.warn('User is not logged in. Redirecting to login route...');

    this.userService.setUserToken(null);
    this.userService.setUser(null);

    this.router.navigate(['/login']);
  }

  private httpQueryBuilder(params) {
    return Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
  }

  private log(logged: any) {
    console.log(logged);
  }
}
