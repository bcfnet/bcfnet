import { TestBed, inject } from '@angular/core/testing';

import { BalanceteSemiSinteticoService } from './balancete_semi_sintetico.service';

describe('BalanceteSemiSinteticoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BalanceteSemiSinteticoService]
    });
  });

  it('should be created', inject([BalanceteSemiSinteticoService], (service: BalanceteSemiSinteticoService) => {
    expect(service).toBeTruthy();
  }));
});
