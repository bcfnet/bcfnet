import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class BalanceteSemiSinteticoService {

  private path = '/financeiro/balancete_semi_sintetico';

  constructor(
    private apiService: ApiService
  ) { }

  get(month: number, year: number, type: string = 'C') {
    return this.apiService.get(this.path, {
      month: month,
      year: year,
      type: type
    });
  }
}
