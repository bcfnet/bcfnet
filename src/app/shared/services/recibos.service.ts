import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class RecibosService {

  private path = '/financeiro/recibos';

  constructor(
    private apiService: ApiService
  ) { }

  get() {
    return this.apiService.get(this.path);
  }
}
