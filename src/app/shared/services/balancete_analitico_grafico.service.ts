import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { BalanceteAnaliticoGrafico } from '../models/balancete_analitico_grafico';

@Injectable({
  providedIn: 'root'
})
export class BalanceteAnaliticoGraficoService {

  private path = '/financeiro/balancete_analitico_grafico';

  constructor(
    private apiService: ApiService
  ) { }

  get(data: BalanceteAnaliticoGrafico) {
    return this.apiService.get(this.path, data);
  }
}
