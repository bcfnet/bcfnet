import { TestBed, inject } from '@angular/core/testing';

import { SaldoAtualizadoService } from './saldo-atualizado.service';

describe('SaldoAtualizadoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SaldoAtualizadoService]
    });
  });

  it('should be created', inject([SaldoAtualizadoService], (service: SaldoAtualizadoService) => {
    expect(service).toBeTruthy();
  }));
});
