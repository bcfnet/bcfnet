import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class DevedoresService {

  private path = '/financeiro/devedores';

  constructor(
    private apiService: ApiService
  ) { }

  get(month: number, year: number) {
    return this.apiService.get(this.path, {
      month: month,
      year: year
    });
  }
}
