import { TestBed, inject } from '@angular/core/testing';

import { BalanceteAnaliticoService } from './balancete_analitico.service';

describe('BalanceteAnaliticoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BalanceteAnaliticoService]
    });
  });

  it('should be created', inject([BalanceteAnaliticoService], (service: BalanceteAnaliticoService) => {
    expect(service).toBeTruthy();
  }));
});
