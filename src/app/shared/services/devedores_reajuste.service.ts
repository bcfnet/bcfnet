import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class DevedoresReajusteService {

  private path = '/financeiro/devedores_reajuste';

  constructor(
    private apiService: ApiService
  ) { }

  get(month: number, year: number) {
    return this.apiService.get(this.path, {
      month: month,
      year: year
    });
  }
}
