import { Injectable } from '@angular/core';
import { Arquivo } from '../models/arquivo';
import { ApiService } from './api.service';
import { PermissionsService } from './permissions.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class GerenteService {

     private tableName = '/condominio/gerente';

     constructor(
          private apiService: ApiService,
          private userService: UserService,
          private permissionService: PermissionsService
     ) { }

     create(data) {
      return this.apiService.post(`${this.tableName}/create`, data);
     }

     update(id: number, data) {
      data.id = id;
      return this.apiService.post(`${this.tableName}/update`, data);
     }

     get(id: number = null) {

          if (id == null) {
               return this.apiService.get(this.tableName);
          } else {
               return this.apiService.get(this.tableName, {
                    id: id
               });
          }
     }

     filter() {
       return this.apiService.get(`${this.tableName}/filter`);
     }

     delete(id: number) {
          return this.apiService.post(`${this.tableName}/delete`, {
               id: id
          });
     }
}
