import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  private account: any; // Todo create Model for support account

  constructor() {
    if (!(localStorage.getItem('account') === null)) {
      this.account = JSON.parse(localStorage.getItem('account'));
    }
  }

  get() {
    return this.account;
  }
}
