import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Aviso } from '../models/aviso';
import { PermissionsService } from './permissions.service';

@Injectable({
  providedIn: 'root'
})
export class AvisoService {

    private endpoint = '/condominio/aviso';

    constructor(
        private apiService: ApiService,
        private permissionService: PermissionsService
    ) { }

    create(aviso: any) {
        if (this.permissionService.isCondomino()) {
            Object.assign(aviso, {
                condominio: this.permissionService.getCondominioLogin()
            });
        }
        return this.apiService.post(`${this.endpoint}/create`, aviso);
    }

    update(aviso: Aviso) {
        if (this.permissionService.isCondomino()) {
            Object.assign(aviso, {
                condominio: this.permissionService.getCondominioLogin()
            });
        }
        return this.apiService.post(`${this.endpoint}/update`, aviso);
    }

    get(id: number = null) {
        if (id == null) {
            return this.apiService.get(`${this.endpoint}`);
        } else {
            return this.apiService.get(`${this.endpoint}`, {
                id: id
            });
        }
    }

    get_total_from_manager() {
        return this.apiService.get(`${this.endpoint}/get_total_avisos_administradora`);
    }

    get_total_from_condom() {
        return this.apiService.get(`${this.endpoint}/get_total_avisos_condominio`);
    }

    delete(id: number) {
        return this.apiService.post(`${this.endpoint}/delete`, {
        id: id
        });
    }
}
