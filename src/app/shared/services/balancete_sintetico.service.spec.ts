import { TestBed, inject } from '@angular/core/testing';

import { BalanceteSinteticoService } from './balancete_sintetico.service';

describe('BalanceteSinteticoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BalanceteSinteticoService]
    });
  });

  it('should be created', inject([BalanceteSinteticoService], (service: BalanceteSinteticoService) => {
    expect(service).toBeTruthy();
  }));
});
