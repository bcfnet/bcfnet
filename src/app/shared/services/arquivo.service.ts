import { Injectable } from '@angular/core';
import { Arquivo } from '../models/arquivo';
import { ApiService } from './api.service';
import { PermissionsService } from './permissions.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class ArquivoService {

  constructor(
    private apiService: ApiService,
    private userService: UserService,
    private permissionService: PermissionsService
  ) { }

    create(arquivo: Arquivo) {
        return this.apiService.post('/administrativo/arquivo/create', arquivo);
    }

    update(id: string, arquivo: Arquivo) {
        arquivo.id = id;
        return this.apiService.post('/administrativo/arquivo/update', arquivo);
    }

  get(login: string = null, id: string = null) {

    if (this.permissionService.isProprietario()) {
      login = this.userService.getUserLogin();
    }

    if (login == null) {
      if (id == null) {
        return this.apiService.get('/administrativo/arquivo');
      } else {
        return this.apiService.get('/administrativo/arquivo', {
            id: id
        });
      }
    } else {
      if (id == null) {
        return this.apiService.get(`/administrativo/arquivo`, {
            login: login
        });
      } else {
        return this.apiService.get(`/administrativo/arquivo`, {
            login: login,
            id: id
        });
      }
    }
  }

  delete(login: string, id: number) {
    return this.apiService.post('/administrativo/arquivo/delete', {
      id: id,
      login: login
    });
  }
}
