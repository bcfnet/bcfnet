import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { PermissionsService } from './permissions.service';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AdministrativoService {

  private path = '/administrativo';

  constructor(
    private apiService: ApiService,
    private userService: UserService,
    private permissionService: PermissionsService
  ) { }

  get(route: string, subcategoria: string) {
    const login = this.permissionService.isSindico() ? this.userService.getUserLogin() : this.permissionService.getCondominioLogin();

    return this.apiService.get(`${this.path}/${route}`, {
      subcategoria: subcategoria,
      login: login
    });
  }
}
