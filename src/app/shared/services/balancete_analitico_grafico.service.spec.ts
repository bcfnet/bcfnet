import { TestBed, inject } from '@angular/core/testing';

import { BalanceteAnaliticoGraficoService } from './balancete_analitico_grafico.service';

describe('BalanceteAnaliticoGraficoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BalanceteAnaliticoGraficoService]
    });
  });

  it('should be created', inject([BalanceteAnaliticoGraficoService], (service: BalanceteAnaliticoGraficoService) => {
    expect(service).toBeTruthy();
  }));
});
