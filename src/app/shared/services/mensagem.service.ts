import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { UserService } from './user.service';
import { Mensagem } from '../models/mensagem';

@Injectable({
  providedIn: 'root'
})
export class MensagemService {

  constructor(
    private apiService: ApiService,
    private userService: UserService
  ) { }

  create(mensagem: Mensagem) {
    return this.apiService.post('/condominio/mensagem/create', mensagem);
  }

  update(id: string, mensagem: Mensagem) {}

  get(id: number = null) {
    if (id == null) {
      return this.apiService.get('/condominio/mensagem', {
          destinatario: this.userService.getUserLogin(),
          deleted: 0
      });
    } else {
      return this.apiService.get(`/condominio/mensagem`, {
          id: id,
          destinatario: this.userService.getUserLogin(),
          deleted: 0
      });
    }
  }

  getAll(deleted: number = 0) {
     return this.apiService.get('/condominio/mensagem', {
          deleted: deleted
     });
  }

  delete(id: number, force: number = 0) {
     return this.apiService.post('/condominio/mensagem/delete', {
          id: id,
          force: force
     });
  }

  deleteAll(login: string, id: number, force: number = 0) {
     return this.apiService.post('/condominio/mensagem/delete', {
          id: id,
          login: login,
          force: force,
          all: 1
     });
  }
}
