import { Injectable } from '@angular/core';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class PosicaoRecibosService {

    private path = '/financeiro/posicao_recibos_grafico_analitico';

    constructor(
        private apiService: ApiService
    ) { }

    async get(month, year) {
        return new Promise((resolve, reject) => {
            const recibos = [];

            this.apiService.get(this.path, {
                month: month,
                year: year,
                paid: 'S'
            }).subscribe((result: any) => {
                if (result.length > 0) {
                    result.map(($recibos) => {
                        $recibos.status = 1;
                        recibos.push($recibos);
                        return $recibos;
                    });

                    this.apiService.get(this.path, {
                        month: month,
                        year: year,
                        paid: 'N'
                    }).subscribe(($result: any) => {
                        if ($result.length > 0) {
                            $result.map(($recibos) => {
                                $recibos.status = 0;
                                recibos.push($recibos);
                                return $recibos;
                            });
                        }
                        resolve(recibos.sort(this.compare));
                    });
                } else {
                    this.apiService.get(this.path, {
                        month: month,
                        year: year,
                        paid: 'N'
                    }).subscribe(($result: any) => {
                        if ($result.length > 0) {
                            $result.map(($recibos) => {
                                $recibos.status = 0;
                                recibos.push($recibos);
                                return $recibos;
                            });
                        }
                        resolve(recibos.sort(this.compare));
                    });
                }
            });
        });
    }

    compare(a, b) {
        if (a.CodigoUnidade < b.CodigoUnidade) {
            return -1;
        }

        if (a.CodigoUnidade > b.CodigoUnidade) {
            return 1;
        }
        return 0;
    }
}
