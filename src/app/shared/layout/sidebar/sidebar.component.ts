import { Component, OnInit } from '@angular/core';
import { PermissionsService } from '../../services/permissions.service';
import { ROUTES } from './sidebar-routes.config';
import { AvisoService } from '../../services/aviso.service';
declare var $: any; // JQuery

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

    public menuItems: any[];
    public total_avisos_administradora = 0;
    public total_avisos_condominio = 0;

    constructor(
        public permissionService: PermissionsService,
        private avisosService: AvisoService
    ) {
        this.init();
    }

    ngOnInit() {
        $('.side-nav .side-nav-menu li a').click(function () {
            if ($(this).parent().hasClass('open')) {

                $(this).parent().children('.dropdown-menu').slideUp(200, function () {
                    $(this).parent().removeClass('open');
                });

            } else {
                $(this).parent().parent().children('li.open').children('.dropdown-menu').slideUp(200);
                $(this).parent().parent().children('li.open').children('a').removeClass('open');
                $(this).parent().parent().children('li.open').removeClass('open');
                $(this).parent().children('.dropdown-menu').slideDown(200, function () {
                    $(this).parent().addClass('open');
                });
            }
        });

        this.menuItems = ROUTES;
        // console.log(this.menuItems);
        /*ROUTES.filter(menuItem => {
            return menuItem.roles.includes(true); // this.permissionService.getPermission());
        }); */

        /*$('.side-nav-toggle').click((event) => {
            const toggle = $('.app');
            toggle.toggleClass('is-collapsed');
            return event.preventDefault();
        });*/

        $('.side-nav-toggle').click((event) => {
            const toggle = $('.app');
            toggle.toggleClass('is-collapsed');
            return event.preventDefault();
        });
    }

    init() {
        this.avisosService.get_total_from_manager()
            .subscribe((avisos: any) => {
                this.total_avisos_administradora = avisos.Total;
            });

        this.avisosService.get_total_from_condom()
            .subscribe((avisos: any) => {
                this.total_avisos_condominio = avisos.Total;
            });
    }

}
