import { Component, OnInit } from '@angular/core';
import { Router, RouterState} from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
import { AccountService } from '../../services/account.service';
import { SpinnerService } from '../../components/spinner/spinner.service';
import { AvisoService } from '../../services/aviso.service';
import { Aviso } from '../../models/aviso';
import { PermissionsService } from '../../services/permissions.service';
import { ApiService } from '../../services/api.service';
import { DadosPerfil } from '../../models/dados_perfil';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  public avisos: Array<Aviso> = [];
  public account: any;

  constructor(
    public permissionService: PermissionsService,
    private apiService: ApiService,
    private authService: AuthService,
    private accountService: AccountService,
    private spinnerService: SpinnerService,
    private avisosService: AvisoService,
    private utils: UtilsService,
    private router: Router
  ) {
    this.avisos = [];
  }

  ngOnInit() {
    this.init();
  }

  init() {
    this.account = this.accountService.get();

    if (this.permissionService.isProprietario() || this.permissionService.isLocatario() || this.permissionService.isCondomino()) {
        this.apiService.get(`/cadastro/dados_perfil`, {CodigoCliente: this.account.codigo}).subscribe((result: DadosPerfil) => {
            if (result.NomeCliente) {
                this.account.NomeCompleto = result.NomeCliente;
            } else {
                if (this.permissionService.isCondomino()) {
                    this.account.NomeCompleto = this.account.condomino;
                } else {
                    this.account.NomeCompleto = this.account.sindico;
                }
            }
        });
    } else {
        this.account.NomeCompleto = this.account.sindico;
    }

    this.avisosService.get_total_from_manager()
        .subscribe((avisos: any) => {
            if (avisos.Total > 0) {
              for (const aviso of avisos.Avisos) {
                this.avisos.push(aviso);
              }
            }
        });

    this.avisosService.get_total_from_condom()
        .subscribe((avisos: any) => {
          if (avisos.Total > 0) {
            for (const aviso of avisos.Avisos) {
              this.avisos.push(aviso);
            }
          }
        });
  }

  logout() {
    this.spinnerService.show();

    setTimeout(() => {
      if (this.authService.logout()) {
        this.spinnerService.hide();
        this.router.navigate(['/login']);
      }
    }, 1000);
  }

}
