import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, RouterState} from '@angular/router';
import { Observable } from 'rxjs';
import { UserService } from './services/user.service';
import {AuthService} from '../auth/auth.service';
import { PermissionsService } from './services/permissions.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private permissionService: PermissionsService,
    private router: Router,
  ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
    this.beforeHook();
    return this.handleRoute(state);
  }

  private beforeHook() {
    if (!this.userService.getUserToken()) {
      console.log('You dont have permission to view this page');
      this.redirectToRoute('/login');
    }
  }

  handleRoute(state: RouterStateSnapshot) {
    const user = this.userService.getUser();

    if (this.permissionService.isAdmin()) {

        if (!state.url.match('admin') && !state.url.match('avisos') && !state.url.match('mensagens')) {
            this.router.navigate(['/admin']);
            return false;
        }

    } else if (this.permissionService.isGerente()) {

        if (state.url.match('admin')) {
            this.router.navigate(['/dashboard']);
            // this.router.navigate(['/admin']);
            return false;
        }

    } else if (this.permissionService.isSindico()) {
        if (state.url.match('admin')) {
            this.router.navigate(['/dashboard']);
            return true;
        }/*else {
            this.router.navigate(['/']);
            return false;
        }*/
    } else if (this.permissionService.isProprietario()) {
        if ((state.url.match('admin') && !state.url.match('proprietario')) || state.url.match('dashboard')) {
            this.router.navigate(['/admin/proprietario']);
            return false;
        }

    } else if (this.permissionService.isLocatario()) {
        if (state.url.match('dashboard') && !state.url.match('locatario')) {
            this.router.navigate(['/dashboard/locatario']);
            return false;
        }
    }

    return true;
  }

  private redirectToRoute(path: string) {
    this.router.navigate([path]);
    return false;
  }
}
