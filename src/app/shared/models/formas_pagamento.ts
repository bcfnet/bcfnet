export interface FormasPagamento {
    CodigoCliente?: string;
    NomeCliente?: string;
    CPF_CNPJ?: string;
    Banco?: string;
    Banco_Digito?: string;
    Agencia?: string;
    Digito_Agencia?: string;
    Conta?: string;
    Digito_Conta?: string;
    dt_update?: string;
}
