export interface BalanceteAnalitico {
    login: number;
    month: number;
    year: number;
    token?: string;
}
