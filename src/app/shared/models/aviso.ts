export interface Aviso {
    id?: number;
    titulo: string;
    data_expiracao: string;
    visualizacao?: string;
    mensagem: string;
    condominio?: string;
    condomino?: string;
}
