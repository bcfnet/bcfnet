export interface Recibo {
    Codigo: number;
    Complemento: number;
    NumeroRecibo: number;
    DataVencimento: string;
    InstrucaoRecibo: string;
}

