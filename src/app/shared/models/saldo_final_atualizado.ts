export interface SaldoFinalAtualizado {
    MesAno: string;
    SaldoAnterior: number;
    SaldoFinal: number;
    TotalCredito: number;
    TotalDebito: number;
}