export interface DadosPerfil {
    CodigoCliente?: string;
    NomeCliente?: string;
    CPF_CNPJ?: string;
    E_Mail?: string;
    Telefone?: string;
    Celular?: string;
    Senha?: string;
    CEP?: string;
    Tipo_logradouro?: string;
    Enderecos?: string;
    Numero?: string;
    Complemento?: string;
    Bairro?: string;
    Estado?: string;
    Cidade?: string;
    dt_update?: string;
}
