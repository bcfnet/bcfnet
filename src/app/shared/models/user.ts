export class User {
  constructor(
    public id?: number,
    public login?: any,
    public document_number?: string,
    public password?: any,
    public status?: string,
    public type?: string,
    public role?: string,
  ) {}
}
