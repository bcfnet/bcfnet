export class Account {
    constructor(
      public id?: number,
      public login?: string,
      public document_number?: string,
      public password?: string,
      public status?: string,
      public type?: string,
      public role?: string,
    ) {}
}
