export class Unidade {
    CodigoCliente?: number;
    NomeCliente?: string;
    EnderecoCondominio?: string;
    Senha?: string;
    NomeSindico: string;
    Email: string;
    Tel_Sindico: string;
    Codigo_da_Unidade: number;
    Complemento_Unidade: string;
    Nome_do_Cliente: string;
    LoginUnidade: string;
    SenhaUnidade: string;
    NomeGerente: string;
    e_MailCondomino: string;
    UF: string;
    CPF_CNPJ?: string;
    CEP: string;
    Cidade: string;
    Bairro: string;
}
