export interface Gerente {
    CodigoFilial: number;
    NomeFilial: string;
    CodigoGerente: number;
    NomeGerente: string;
    Telefone: string;
    E_Mail: string;
}
