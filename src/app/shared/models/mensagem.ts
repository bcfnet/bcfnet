export interface Mensagem {
     id?: number;
     destinatario: string;
     login: string;
     nome: string;
     email: string;
     assunto: string;
     mensagem: string;
     link_arquivo?: any;
     mensagem_id?: number;
     lida?: boolean;
}
