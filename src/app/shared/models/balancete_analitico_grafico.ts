export class BalanceteAnaliticoGrafico {
    login: string;
    month: number;
    year: number;
    type: string;
    unit: string;
    token?: string;
}
