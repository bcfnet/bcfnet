export class Arquivo {
    id?: string;
    login: number;
    arvore?: string;
    pasta: string;
    tipo: string;
    descricao?: string;
    permissao: string;
    data_expiracao: string;
    data_inicial: string;
    data_final: string;
    data_inclusao: string;
    link_arquivo?: any;
    status?: any;
}
