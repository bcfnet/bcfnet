import 'flatpickr/dist/flatpickr.css';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { NgSelectizeModule } from 'ng-selectize';
import { NgxMaskModule } from 'ngx-mask';
import { FlatpickrModule } from 'angularx-flatpickr';
import { ModalComponent } from './components/modal/modal.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { BalanceteComponent } from './components/balancete/balancete.component';
import { SwitchBarComponent } from './components/switch-bar/switch-bar.component';
import { TableComponent } from './components/table/table.component';
import { TableRowComponent } from './components/table-row/table-row.component';
import { AgendaComponent } from '../agenda/agenda.component';
import { TileComponent } from './components/tile/tile.component';
import { RouterModule } from '@angular/router';
import { TilesComponent } from './components/tiles/tiles.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectizeModule,
    RouterModule,
    FlatpickrModule.forRoot(),
    NgxMaskModule.forRoot()
  ],
  exports: [
    AgendaComponent,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgSelectizeModule,
    NgxMaskModule,
    ModalComponent,
    SpinnerComponent,
    BalanceteComponent,
    SwitchBarComponent,
    TableComponent,
    TableRowComponent,
    TileComponent,
    TilesComponent
  ],
  declarations: [
    ModalComponent,
    SpinnerComponent,
    BalanceteComponent,
    SwitchBarComponent,
    TableComponent,
    TableRowComponent,
    AgendaComponent,
    TileComponent,
    TilesComponent
  ]
})
export class SharedModule { }
