import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {

  public loaderStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  display(value: boolean): void {
    this.loaderStatus.next(value);
  }

  show(): void {
    this.display(true);
  }

  hide(): void {
    this.display(false);
  }
}
