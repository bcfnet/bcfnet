import {Component, Input, OnDestroy, Inject, ViewEncapsulation, OnInit} from '@angular/core';
import {Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import { SpinnerService } from './spinner.service';

@Component({
    selector: 'app-spinner',
    templateUrl: './spinner.component.html',
    styleUrls: ['./spinner.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class SpinnerComponent implements OnInit, OnDestroy {
    public isSpinnerVisible = true;

    @Input() public backgroundColor = 'rgba(0, 115, 170, 0.69)';
    @Input() public loading = true;

    constructor(
        private router: Router,
        private spinnerService: SpinnerService,
        @Inject(DOCUMENT) private document: Document
    ) {
        /*this.isSpinnerVisible = this.loading;
        this.router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this.isSpinnerVisible = true;
            } else if ( event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
                this.isSpinnerVisible = false;
            }
        }, () => {
            this.isSpinnerVisible = false;
        });*/
    }

    ngOnInit() {
        this.spinnerService.loaderStatus.subscribe((val: boolean) => {
            this.isSpinnerVisible = val;
            // console.log(this.isSpinnerVisible);
        });
    }

    ngOnDestroy(): void {
        this.isSpinnerVisible = false;
    }
}
