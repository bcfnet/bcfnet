import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../../auth/auth.service';
import { SpinnerService } from '../spinner/spinner.service';
import { AccountService } from '../../services/account.service';

@Component({
  selector: 'app-switch-bar',
  templateUrl: './switch-bar.component.html',
  styleUrls: ['./switch-bar.component.scss']
})
export class SwitchBarComponent implements OnInit {

  account;

  constructor(
    public authService: AuthService,
    public spinnerService: SpinnerService,
    public accountService: AccountService,
    private router: Router
  ) { }

  ngOnInit() {
    this.account = this.accountService.get();
  }

  open() {
    console.log('abrir');
  }

  swapout() {
    this.authService.swapOff();
  }

}
