import { Component, OnInit, ViewChild, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import * as jsPDF from 'jspdf';
import * as html2canvas from 'html2canvas';
import * as moment from 'moment';
import { SpinnerService } from '../spinner/spinner.service';
import { ApiService } from '../../services/api.service';
import { Gerente } from '../../models/gerente';
import { Unidade } from '../../models/unidade';
import { UtilsService } from '../../services/utils.service';
import { PermissionsService } from '../../services/permissions.service';
import { AccountService } from '../../services/account.service';
import { exportToCSV, exportToPDF, imageToBlob } from 'src/app/helpers';

@Component({
  selector: 'app-balancete',
  templateUrl: './balancete.component.html',
  styleUrls: ['./balancete.component.scss']
})
export class BalanceteComponent implements OnInit {

  public unidade: Unidade;
  public unidades: Array<Unidade[]>;
  public gerente: Gerente;
  public periodos;
  public $periodo;
  public timer;
  public account;
  @ViewChild('balancete') balancete: ElementRef;
  @ViewChild('table') table: ElementRef;
  @Input() name: string = '';
  @Input() loading = true;
  @Input() childData: Array<any> = [];
  @Output() periodo = new EventEmitter();

  constructor(
        public accountService: AccountService,
        public permissionService: PermissionsService,
        private spinnerService: SpinnerService,
        private apiService: ApiService,
        private utils: UtilsService,
        private router: Router
  ) { }

    ngOnInit() {
        this.retornaUnidade();
        this.listarPeriodo();
        this.listarTimer();
        this.account = this.accountService.get();
    }

    exportAsExcel() {
        // todo: use @https://github.com/luwojtaszek/ngx-excel-export????? 
    }

    exportAsCSV() {
        if (this.childData.length) {
            exportToCSV(this.router.url + '.csv', this.childData);
        }
    }

    exportAsPrint() {
        exportToPDF(
            this.router.url + '-' + this.account.codigo,
            $('#balancete').clone(),
            $('style'),
            true
        );
    }

    exportAsPDF() {
        exportToPDF(
            this.router.url + '-' + this.account.codigo,
            $('#balancete').clone(),
            $('style'),
            false
        );
    }

    /*exportAsPDF() {
        const balancete = this.balancete.nativeElement;
        this.spinnerService.display(true);

        html2canvas(balancete).then((canvas) => {
        const pdf = new jsPDF('p', 'pt', 'a4');
        const $height = 1300;
        const $width = 1140;

        for (var i = 0; i <= balancete.clientHeight / $height; i++) {
            //! This is all just html2canvas stuff
            var srcImg  = canvas;
            var sX      = 0;
            var sY      = $height * i; // start 980 pixels down for every new page
            var sWidth  = $width;
            var sHeight = $height;
            var dX      = 0;
            var dY      = 0;
            var dWidth  = $width;
            var dHeight = $height;

            const onePageCanvas = document.createElement("canvas");
            onePageCanvas.setAttribute('width', `${$width}`);
            onePageCanvas.setAttribute('height', `${$height}`);
            var ctx = onePageCanvas.getContext('2d');
            ctx.fillStyle = '#FFFFFF';
            // details on this usage of this function: 
            // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Using_images#Slicing
            ctx.drawImage(srcImg,sX,sY,sWidth,sHeight,dX,dY,dWidth,dHeight);

            var canvasDataURL = onePageCanvas.toDataURL("image/png", 1.0);

            var width         = onePageCanvas.width;
            var height        = onePageCanvas.clientHeight;

            //! If we're on anything other than the first page,
            // add another page
            if (i > 0) {
                pdf.addPage([595.28, 841.89]); //8.5" x 11" in pts (in*72)
            }
            //! now we declare that we're working on that page
            pdf.setPage(i + 1);
            //! now we add content to that page!
            pdf.addImage(canvasDataURL, 'PNG', 0, 0, (width * .62), (height * .62), `IMAGE${i}`, 'fast');

            }
            pdf.save(`relatorio_de_condomino_${new Date().getTime()}.pdf`);
            this.spinnerService.hide();
        });
    }*/

    retornaUnidade() {
        const data = this.permissionService.isSindico() ? {} : { login: this.permissionService.getCondominioLogin() };

        this.apiService.get('/cadastro/retorna_unidades', data)
            .subscribe((result: any) => {
                this.unidades = result;
                this.unidade = result[0];
                this.unidade.EnderecoCondominio = this.utils.utf8_decode(this.unidade.EnderecoCondominio);
        });
    }

    listarPeriodo() {
        const begin = moment('2017-07-01');
        const end = moment();

        const startOfMonth = moment().startOf('month').format('DD/MM/YYYY');
        const endOfMonth   = moment().endOf('month').format('DD/MM/YYYY');

        this.periodos = this.utils.getRangeOfDates(begin, end, 'month').reverse();
        this.$periodo = {
            periodo: this.periodos[0],
            range: `(De: ${startOfMonth} a ${endOfMonth})`
        };
    }

    canShowAddress() {
        return !this.permissionService.isProprietario() && !this.permissionService.isLocatario();
    }

    onChange(periodo) {
        this.periodo.emit(periodo);

        periodo = periodo.split('/');

        const startOfMonth = moment(new Date(periodo[1], periodo[0], 0)).startOf('month').format('DD/MM/YYYY');
        const endOfMonth   = moment(new Date(periodo[1], periodo[0], 0)).endOf('month').format('DD/MM/YYYY');

        this.$periodo = {
            periodo: periodo,
            range: `(De: ${startOfMonth} a ${endOfMonth})`
        };
    }

    onReceiveChildData(data) {
        console.log('data', data);
    }

    listarTimer() {
        this.timer = moment().format('DD/MM/YYYY [às] HH:mm:ss');
    }

}
