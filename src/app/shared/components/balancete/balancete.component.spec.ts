import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BalanceteComponent } from './balancete.component';

describe('BalanceteComponent', () => {
  let component: BalanceteComponent;
  let fixture: ComponentFixture<BalanceteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BalanceteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BalanceteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
