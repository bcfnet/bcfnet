import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() dataSource = [];
  @Input() header = [];
  pages: number = 4;
  pageSize: number = 5;
  pageNumber: number = 0;
  currentIndex: number = 1;
  pagesIndex: Array <number>;
  pageStart: number = 1;
  prevPage;
  nextPage;
  public loading = false;
  private _dataSource = [];

  constructor() {

  }

  ngOnInit() {
    this.init();
    console.log(this.header);
  }

  init() {
    this._dataSource = this.dataSource;
    this.currentIndex = 1;
    this.pageStart = 1;
    this.pages = 4;

    this.pageNumber = parseInt('' + (this._dataSource.length / this.pageSize), 10);
    if (this._dataSource.length % this.pageSize !== 0) {
       this.pageNumber ++;
    }

    if (this.pageNumber  < this.pages) {
          this.pages =  this.pageNumber;
    }

    // this.refreshItems();
    console.log("this.pageNumber :  " + this.pageNumber);
  }

  onSearch(text: string) {
    console.log(this.dataSource);
    if (!text || this.dataSource.length === 0) {
      this.dataSource = this._dataSource;
      return this.dataSource;
    }
    const search = text.toLowerCase();

    const filtered = this.dataSource.filter((d) => {
       return d[this.header[0]].toLowerCase().indexOf(search) !== -1 ||
        d[this.header[1]].toLowerCase().indexOf(search) !== -1 || !search;
    });

    if (filtered.length === 0) {
      this.loading = false;
    } else {
      this.loading = true;
    }
    // update the rows
    this.dataSource = filtered;
  }

}
