import { Component, EventEmitter, Input, OnInit, Output, OnDestroy } from '@angular/core';
import { ModalService } from './modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit, OnDestroy {

  @Input() title: string;
  @Input() target: string;
  @Input() content: string;
  public isVisible = false;

  constructor(
    private modalService: ModalService
  ) {
    /*this.title = 'default';
    this.content = 'default';*/
    this.target = 'modal';
  }

  ngOnInit() {
    this.modalService.loaderStatus.subscribe((val: boolean) => {
        this.isVisible = val;
        // console.log(this.isSpinnerVisible);
    });

    this.modalService.dataset.subscribe((dataset: any) => {
        this.title = dataset.title;
        this.content = dataset.content;
    });
  }

  ngOnDestroy(): void {
      this.isVisible = false;
  }

}
