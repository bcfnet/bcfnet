import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  public loaderStatus: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public dataset = new Subject();

  display(value: boolean): void {
    this.loaderStatus.next(value);
    // console.log(value);
  }

  show(title: string, content: string, target: string = 'modal'): void {
    const value = {
          title: title,
          content: content,
          target: target
     };
    this.dataset.next(value);
    // console.log(value);
    this.display(true);
  }

  hide(): void {
    this.display(false);
  }
}
