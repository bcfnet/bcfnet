import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import * as $ from 'jquery';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { CalendarModule } from 'ap-angular2-fullcalendar';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { NgxPermissionsModule } from 'ngx-permissions';
import { FileDropModule } from 'ngx-file-drop';
import { StickyModule } from 'ng2-sticky-kit';
import { AngularStickyThingsModule } from '@w11k/angular-sticky-things';
import { ScrollToModule } from 'ng2-scroll-to';
import { AccountService } from './shared/services/account.service';
import { AuthGuard } from './shared/auth.guard';
import { AuthService } from './auth/auth.service';
import { ApiService } from './shared/services/api.service';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginComponent } from './auth/login/login.component';
import { UserService } from './shared/services/user.service';
import { HeaderComponent, FooterComponent, SidebarComponent } from './shared/layout';
import { PastaDigitalComponent } from './pasta-digital/pasta-digital.component';
import { SharedModule } from './shared/shared.module';
import { FullComponent } from './layouts/full/full.component';
import { UtilsService } from './shared/services/utils.service';
import { SpinnerService } from './shared/components/spinner/spinner.service';
import { PermissionsService } from './shared/services/permissions.service';
import { FaqComponent } from './faq/faq.component';
import { IframeAutoHeightDirective } from './shared/directives/iframeautoheight.directive';
import { EsqueceuSenhaComponent } from './auth/esqueceu-senha/esqueceu-senha.component';
import { GraficosModule } from './graficos/graficos.module';
import { FlatpickrModule } from 'angularx-flatpickr';
import {ToasterModule, ToasterService} from 'angular2-toaster';

import { registerLocaleData, HashLocationStrategy, LocationStrategy } from '@angular/common';
import localePt from '@angular/common/locales/pt';

registerLocaleData(localePt, 'pt');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    FullComponent,
    LoginComponent,
    PastaDigitalComponent,
    FaqComponent,
    IframeAutoHeightDirective,
    EsqueceuSenhaComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    GraficosModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    CalendarModule,
    NgxPermissionsModule.forRoot(),
    PerfectScrollbarModule,
    FileDropModule,
    StickyModule,
    AngularStickyThingsModule,
    ScrollToModule,
    FlatpickrModule.forRoot(),
    ToasterModule.forRoot()
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt' },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    AuthService,
    AuthGuard,
    ApiService,
    AccountService,
    PermissionsService,
    UserService,
    SpinnerService,
    UtilsService,
    ToasterService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
