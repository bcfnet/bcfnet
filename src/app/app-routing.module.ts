import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/auth.guard';
import { LoginComponent } from './auth/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AgendaComponent } from './agenda/agenda.component';
import { PastaDigitalComponent } from './pasta-digital/pasta-digital.component';
import { FullComponent } from './layouts/full/full.component';
import { FaqComponent } from './faq/faq.component';
import { EsqueceuSenhaComponent } from './auth/esqueceu-senha/esqueceu-senha.component';

const routes: Routes = [
    {
        path: '',
        component: FullComponent,
        canActivate: [AuthGuard],
        children: [
            { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
            { path: 'administrativo', loadChildren: './administrativo/administrativo.module#AdministrativoModule' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'financeiro', loadChildren: './financeiro/financeiro.module#FinanceiroModule' },
            { path: 'departamento-pessoal', loadChildren: './departamento-pessoal/departamento-pessoal.module#DepartamentoPessoalModule' },
            { path: 'relatorios', loadChildren: './relatorios/relatorios.module#RelatoriosModule' },
            { path: 'documentos', loadChildren: './documentos/documentos.module#DocumentosModule' },
            { path: 'graficos', loadChildren: './graficos/graficos.module#GraficosModule' },
            { path: 'juridico', loadChildren: './juridico/juridico.module#JuridicoModule' },
            { path: 'mensagens', loadChildren: './mensagens/mensagens.module#MensagensModule' },
            { path: 'mapa', loadChildren: './mapa-localizacao/mapa-localizacao.module#MapaLocalizacaoModule' },
            { path: 'album', loadChildren: './album/album.module#AlbumModule' },
            { path: 'avisos', loadChildren: './avisos/avisos.module#AvisosModule' },
            { path: 'agenda', component: AgendaComponent },
            { path: 'pasta-virtual', component: PastaDigitalComponent },
            { path: 'duvidas-frequentes', component: FaqComponent },
            { path: 'perfil', loadChildren: './perfil/perfil.module#PerfilModule' },
            { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
            { path: 'reservas', loadChildren: './reservas/reservas.module#ReservasModule' },
        ]
    },
    {
        path: 'login',
        children: [
            { path: '', component: LoginComponent },
            { path: 'esqueci-minha-senha', component: EsqueceuSenhaComponent }
        ]
    },
    {
        path: '**',
        redirectTo: '/dashboard'
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
