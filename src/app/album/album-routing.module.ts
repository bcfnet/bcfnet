import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastrarFotosComponent } from './cadastrar-fotos/cadastrar-fotos.component';
import { VisualizarAlbumComponent } from './visualizar-album/visualizar-album.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'cadastrar-fotos'
  },
  {
    path: 'cadastrar-fotos',
    component: CadastrarFotosComponent
  },
  {
    path: 'visualizar-album',
    component: VisualizarAlbumComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumRoutingModule { }
