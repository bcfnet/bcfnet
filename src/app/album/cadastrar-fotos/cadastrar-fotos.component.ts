import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {
  UploadEvent,
  UploadFile,
  FileSystemFileEntry,
  FileSystemDirectoryEntry
} from 'ngx-file-drop';
import { AlbumService } from '../../shared/services/album.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ToasterService } from 'angular2-toaster';
import { Album } from '../../shared/models/album';

@Component({
  selector: 'app-cadastrar-fotos',
  templateUrl: './cadastrar-fotos.component.html',
  styleUrls: ['./cadastrar-fotos.component.scss']
})
export class CadastrarFotosComponent {

  public files: UploadFile[] = [];
  public album: Array<Album>;

  constructor(
    private albumService: AlbumService,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private router: Router
  ) {

  }

  public upload(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {

      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        this.onSubmit(reader.result);
      };
      /*this.files.push(event.target.files[0]);
      console.log(this.files); */
      /*let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.form.get('avatar').setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        })
      }; */
    }
  }

  public dropped(event: UploadEvent) {
    this.files = event.files;
    for (const droppedFile of event.files) {
      // Is it a file?
      if (droppedFile.fileEntry.isFile) {
        const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
        fileEntry.file((file: File) => {
          console.log(droppedFile);
          // Here you can access the real file
          console.log(droppedFile.relativePath, file);
 
          /**
          // You could upload it like this:
          const formData = new FormData()
          formData.append('logo', file, relativePath)
 
          // Headers
          const headers = new HttpHeaders({
            'security-token': 'mytoken'
          })
 
          this.http.post('https://mybackend.com/api/upload/sanitize-and-save-logo', formData, { headers: headers, responseType: 'blob' })
          .subscribe(data => {
            // Sanitized logo returned from backend
          })
          **/
 
        });
      } else {
        // It was a directory (empty directories are added, otherwise only files)
        const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
        console.log(droppedFile.relativePath, fileEntry);
      }
    }
  }

  public fileOver(event) {
    console.log(event);
  }

  public fileLeave(event) {
    console.log(event);
  }

  public delete(index) {
    this.files.splice(index, 1);
  }

  onSubmit($file) {
    this.spinnerService.show();

    this.albumService.create({
      arquivo: $file
    })
        .subscribe(
          (result) => {
            console.log(result);
            if (result.error == 0) {
              // console.log(JSON.stringify(result));
              // console.log(`Result: ${result}`);
              this.toasterService.pop('success', 'Parabéns!', 'Sua foto foi publicada com sucesso.');
              // this.router.navigate(['/documentos/gerenciar-arquivos']);
            } else {
              this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
            }
            this.spinnerService.hide();
          },
          (err) => {
            console.log(`Error: ${err}`);
            this.spinnerService.hide();
            this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
          }
        );
  }

}
