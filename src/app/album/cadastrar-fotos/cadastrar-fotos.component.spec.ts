import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarFotosComponent } from './cadastrar-fotos.component';

describe('CadastrarFotosComponent', () => {
  let component: CadastrarFotosComponent;
  let fixture: ComponentFixture<CadastrarFotosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarFotosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarFotosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
