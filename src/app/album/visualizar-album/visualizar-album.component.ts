import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../../shared/services/album.service';
import { Album } from '../../shared/models/album';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ToasterService } from 'angular2-toaster';
import { PermissionsService } from '../../shared/services/permissions.service';
import { UtilsService } from '../../shared/services/utils.service';

@Component({
  selector: 'app-visualizar-album',
  templateUrl: './visualizar-album.component.html',
  styleUrls: ['./visualizar-album.component.scss']
})
export class VisualizarAlbumComponent implements OnInit {

  album: Album;
  albums: Array<Album[]>;
  message: string;
  masonryItems: any[];
  masonryContainer: any;

  ngOnInit() {
    this.get();
  }

  constructor(
    public permissionService: PermissionsService,
    private albumService: AlbumService,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private utils: UtilsService
  ) {
      this.masonryItems = [];
  }

  get() {
    this.masonryItems = [];
    this.spinnerService.show();
    const login = this.utils.getToken();
    // const login = this.permissionService.isCondomino() ? this.permissionService.getCondominioLogin() : null;
    this.albumService.get(login)
      .subscribe((albums: Array<Album[]>) => {
        this.albums = albums;
        this.albums.map((album: any) => {
          this.masonryItems.push({
            id: album.id,
            src: this.utils.getBaseUrl() + '/' + album.large.replace('./', ''),
            w: 700, h: 500 /*,
            'title': 'So nature',
            'date': '8/5/2017' */
          });
          return album;
        });
        this.spinnerService.hide();
      });
  }

  delete(id: number) {
    const confirm = window.confirm(`Você tem certeza que deseja deletar a foto ${id}?`);

    if (confirm) {
      this.spinnerService.show();
      this.albumService.delete(id)
        .subscribe(result => {
          if (result.error == 0) {
            this.toasterService.pop('success', 'Parabéns!', 'Sua foto foi apagada com sucesso.');
            this.get();
          } else {
            this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
          }
          this.spinnerService.hide();
        });
    }
  }

}
