import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizarAlbumComponent } from './visualizar-album.component';

describe('VisualizarAlbumComponent', () => {
  let component: VisualizarAlbumComponent;
  let fixture: ComponentFixture<VisualizarAlbumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizarAlbumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizarAlbumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
