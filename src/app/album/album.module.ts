import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMasonryModule } from 'ngx-masonry';
import { FileDropModule } from 'ngx-file-drop';
import { AlbumRoutingModule } from './album-routing.module';
import { CadastrarFotosComponent } from './cadastrar-fotos/cadastrar-fotos.component';
import { VisualizarAlbumComponent } from './visualizar-album/visualizar-album.component';

@NgModule({
  imports: [
    CommonModule,
    AlbumRoutingModule,
    NgxMasonryModule,
    FileDropModule
  ],
  declarations: [CadastrarFotosComponent, VisualizarAlbumComponent]
})
export class AlbumModule { }
