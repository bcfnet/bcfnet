import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { MensagensRoutingModule } from './mensagens-routing.module';
import { MensagensComponent } from './mensagens.component';
import { CadastrarMensagensComponent } from './cadastrar-mensagens/cadastrar-mensagens.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { ListarMensagemComponent } from './listar-mensagem/listar-mensagem.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    MensagensRoutingModule,
    PerfectScrollbarModule
  ],
  declarations: [MensagensComponent, CadastrarMensagensComponent, ListarMensagemComponent]
})
export class MensagensModule { }
