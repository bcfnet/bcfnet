import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarMensagemComponent } from './listar-mensagem.component';

describe('ListarMensagemComponent', () => {
  let component: ListarMensagemComponent;
  let fixture: ComponentFixture<ListarMensagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarMensagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarMensagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
