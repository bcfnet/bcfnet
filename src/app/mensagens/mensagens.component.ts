import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { MensagemService } from "../shared/services/mensagem.service";
import { Mensagem } from "../shared/models/mensagem";
import { SpinnerService } from "../shared/components/spinner/spinner.service";
import { ToasterService } from "angular2-toaster";
import { UtilsService } from "../shared/services/utils.service";
import { AccountService } from "../shared/services/account.service";
import { UserService } from "../shared/services/user.service";

@Component({
  selector: "app-mensagens",
  templateUrl: "./mensagens.component.html",
  styleUrls: ["./mensagens.component.scss"]
})
export class MensagensComponent implements OnInit {
    public emailLayout: any;
    public mensagens: Array<Mensagem> = [];
    public mensagem: any;
    public emails: any;
    public loading = true;
    private sub: any;
    private selectedId: number;

    constructor(
        public utils: UtilsService,
        public accountService: AccountService,
        public userService: UserService,
        private mensagemService: MensagemService,
        private spinnerService: SpinnerService,
        private toasterService: ToasterService,
        private route: ActivatedRoute,
        private router: Router
    ) {}

    ngOnInit() {
        this.get();

        this.emailLayout = {
            emailViewMobile: false,
            emailNavMobile: false
        };
    }

    get(id: number = null) {
        this.spinnerService.show();

        this.mensagemService.get(id).subscribe(
            (mensagens: Array<Mensagem>) => {
                this.mensagens = mensagens.map(mensagem => {
                    mensagem.assunto = this.utils.convertToUTF8(mensagem.assunto);
                    mensagem.mensagem = this.utils.convertToUTF8(mensagem.mensagem);
                    mensagem.link_arquivo = mensagem.link_arquivo !== '' ? 
                        this.utils.getBaseUrl() + mensagem.link_arquivo.replace('./', '/') : '';

                    return mensagem;
                });

                if (this.mensagens.length > 0 && id == null) {
                    this.mensagem = this.mensagens[0];
                    this.onSelect(this.mensagem.id);
                } else {
                    this.mensagem = null;
                }
                console.log(mensagens);

                this.loading = false;
                this.spinnerService.hide();
            },
            error => {
                console.log(error);
                this.loading = false;
                this.spinnerService.hide();
            }
        );
    }

    delete(id: number) {
            const confirm = window.confirm(`Você deseja deletar a mensagem ${id}?`);

            if (confirm) {
                this.spinnerService.show();

                this.mensagemService.delete(id).subscribe(result => {
                    if (result.error === "0") {
                        this.toasterService.pop(
                            'success',
                            'Parabéns!',
                            `A mensagem foi deletada com sucesso.`
                        );
                        this.get();
                    } else {
                        this.toasterService.pop(
                            'error',
                            'Atenção!',
                            `A mensagem não pôde ser deletada.`
                        );
                    }
                });
            }
    }

    formatBody = function(body) {
        return body.replace(/<(?:.|\n)*?>/gm, "");
    };

    isFileJpg(link_arquivo) {
        if (!link_arquivo.search('.jpg')) {
            return false;
        }
        return true;
    }

    isSelected(mensagem: Mensagem) {
        return mensagem.id === this.selectedId;
    }

    isOwner(sender) {
        return sender === this.userService.getUserLogin();
    }

    onFilter(type: string) {
        if (type === "sent") {
        } else {
        }
    }

    onSelect(id: number) {
        this.selectedId = id;
        this.mensagem = this.mensagens.filter((mensagem: any) => {
            return mensagem.id === id;
        })[0];
    }

    getSendData(date) {
        const data_envio = date.split(" ");
        const data = data_envio[0].split("-");
        date = [data[2], data[1], data[0]].join("/") + " às " + data_envio[1];
        return date;
    }
}
