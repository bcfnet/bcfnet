import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarMensagensComponent } from './cadastrar-mensagens.component';

describe('CadastrarMensagensComponent', () => {
  let component: CadastrarMensagensComponent;
  let fixture: ComponentFixture<CadastrarMensagensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarMensagensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarMensagensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
