import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../../shared/services/user.service';
import { ApiService } from '../../shared/services/api.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ToasterService } from 'angular2-toaster';
import { PermissionsService } from '../../shared/services/permissions.service';
import { Mensagem } from '../../shared/models/mensagem';
import { MensagemService } from '../../shared/services/mensagem.service';
import { AccountService } from '../../shared/services/account.service';
import { CondominioService } from '../../shared/services/condominio.service';
import { DadosPerfil } from 'src/app/shared/models/dados_perfil';

@Component({
    selector: 'app-cadastrar-mensagens',
    templateUrl: './cadastrar-mensagens.component.html',
    styleUrls: ['./cadastrar-mensagens.component.scss']
})
export class CadastrarMensagensComponent implements OnInit {

    errorMessage: string;
    submitting: boolean;
    form: FormGroup;
    mensagem: Mensagem;
    account;
    public condominios = [];

    constructor(
        public userService: UserService,
        public permissionService: PermissionsService,
        private condominioService: CondominioService,
        private apiService: ApiService,
        private accountService: AccountService,
        private spinnerService: SpinnerService,
        private toasterService: ToasterService,
        private mensagemService: MensagemService,
        private router: Router
    ) {
        this.createForm();
        this.account = this.accountService.get();
    }

    ngOnInit() {
        this.get();
    }

    onSelect(condominio) {
        const index = this.condominios.findIndex(($condominio => $condominio.login_internet === condominio.login_internet));
        //this.condominios[index].selected = !condominio.selected;
        //console.log(condominio);
        condominio.selected = !condominio.selected;
    }

    upload(event) {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {

            const [file] = event.target.files;
            reader.readAsDataURL(file);

            reader.onload = () => {
                this.form.controls['link_arquivo'].setValue(reader.result);
            };
        }
    }

    onSelectAll() {
        this.condominios.map((condominios) => {
            condominios.selected = true;
            return condominios;
        });
    }

    onUnselectAll() {
        this.condominios.map((condominios) => {
            condominios.selected = false;
            return condominios;
        });
    }

    onSubmit() {
        if (this.permissionService.isAdmin()) {
            const condominios = this.condominios.filter((condominio) => condominio.selected === true);
            this.form.controls['destinatario'].setValue(condominios[0].login_internet, {onlySelf: true});

            if (this.form.valid || condominios.length > 0) {

                this.mensagem = this.form.value;
                this.spinnerService.show();

                if (condominios.length > 0) {
                    let index = 0;
                    condominios.forEach((condominio) => {
                        this.mensagem.destinatario = condominio.login_internet;

                        this.mensagemService.create(this.mensagem)
                            .subscribe((result) => {
                                // console.log(result);
                            });
                        // console.log(this.mensagem);
                        index++;

                        if (index === condominios.length) {
                            this.toasterService.pop('success', 'Parabéns!', 
                                'Sua mensagem foi publicada com sucesso para o(s) destinatário(s) selecionado(s).');
                            this.router.navigate(['/mensagens/']);
                            this.spinnerService.hide();
                        }
                    });
                }
            } else {
                this.toasterService.pop('error', 'Atenção!', 'Há erros de validação no formulário.');
            }
        } else {
            console.log(this.form);
            if (this.form.valid) {
                this.spinnerService.show();
                this.mensagem = this.form.value;
                this.mensagem.destinatario = this.mensagem.destinatario === '0' ? 'bcfadm' : this.permissionService.getCondominioLogin();

                this.mensagemService.create(this.mensagem)
                    .subscribe(
                        (result) => {
                            if (result.error === '0') {
                                this.toasterService.pop('success', 'Parabéns!', 'Sua mensagem foi publicada com sucesso.');
                                this.router.navigate(['/mensagens/']);
                            } else {
                                this.toasterService.pop('error', 'Atenção!',
                                    result.status);
                            }
                            this.spinnerService.hide();
                        },
                        (err) => {
                            console.log(`Error: ${err}`);
                            this.spinnerService.hide();
                            this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
                        }
                    );
            } else {
                this.toasterService.pop('error', 'Atenção!', 'Há erros de validação no formulário.');
            }
        }

    }
    private createForm() {
        this.form = new FormGroup({
            nome: new FormControl(null, Validators.required),
            // email: new FormControl(null, Validators.required),
            assunto: new FormControl(null, Validators.required),
            destinatario: new FormControl(null, Validators.required),
            mensagem: new FormControl(null, Validators.required),
            link_arquivo: new FormControl(null)
        });

        this.autoFillName();
    }

    private formatDate(data: any) {
        return [data.day, data.month, data.year].join('/');
    }

    private get() {
        if (!this.permissionService.isAdmin()) {
            return false;
        }
        this.condominioService.get()
            .subscribe((condominios: any) => {
                condominios = condominios.sort(this.condominioService.compare);
                condominios.shift();

                if (condominios.length > 0) {
                    for (const condominio of condominios) {
                        condominio.selected = false;
                        this.condominios.push(condominio);
                    }
                }
            });
    }

    private autoFillName() {
        const account = this.accountService.get();

        if (this.permissionService.isProprietario() || this.permissionService.isLocatario() || this.permissionService.isCondomino()) {
            this.apiService.get(`/cadastro/dados_perfil`, {CodigoCliente: account.codigo}).subscribe((result: DadosPerfil) => {
                if (result.NomeCliente) {
                    account.NomeCompleto = result.NomeCliente;
                } else {
                    if (this.permissionService.isCondomino()) {
                        account.NomeCompleto = account.condomino;
                    } else {
                        account.NomeCompleto = account.sindico;
                    }
                }
                this.form.controls['nome'].setValue(account.NomeCompleto);
            });
        } else {
            this.form.controls['nome'].setValue(account.sindico);
        }
    }

}

