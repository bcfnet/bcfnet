import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MensagensComponent } from './mensagens.component';
import { CadastrarMensagensComponent } from './cadastrar-mensagens/cadastrar-mensagens.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MensagensComponent
      },
      {
        path: 'cadastrar',
        component: CadastrarMensagensComponent
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MensagensRoutingModule { }
