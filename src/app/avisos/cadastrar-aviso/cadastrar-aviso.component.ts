import { Component, OnInit } from '@angular/core';
import { Aviso } from '../../shared/models/aviso';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AvisoService } from '../../shared/services/aviso.service';
import { ApiService } from '../../shared/services/api.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ToasterService } from 'angular2-toaster';
import { UserService } from '../../shared/services/user.service';
import { PermissionsService } from '../../shared/services/permissions.service';

@Component({
  selector: 'app-cadastrar-aviso',
  templateUrl: './cadastrar-aviso.component.html',
  styleUrls: ['./cadastrar-aviso.component.scss']
})
export class CadastrarAvisoComponent implements OnInit {

    errorMessage: string;
    submitting: boolean;
    form: FormGroup;
    aviso: Aviso;

    constructor(
        public userService: UserService,
        public avisoService: AvisoService,
        public permissionService: PermissionsService,
        private apiService: ApiService,
        private spinnerService: SpinnerService,
        private toasterService: ToasterService,
        private router: Router
    ) {
        
    }

    ngOnInit() {
        this.createForm();
    }

    onSubmit() {
        if (this.form.valid) {
            this.spinnerService.show();
            this.aviso = this.form.value;
            this.aviso.data_expiracao = this.aviso.data_expiracao ? this.formatDate(this.aviso.data_expiracao) : null;

            if (this.permissionService.isSindico()  || this.permissionService.isAdmin()  ||  this.permissionService.isGerente()) {
                this.aviso.condominio = this.userService.getUserLogin();
                this.aviso.condomino = '';

                if (this.permissionService.isAdmin()) {
                    this.aviso.condominio = '';
                } else {
                    this.aviso.visualizacao = '';
                }
            } else {
                this.aviso.condominio = '';
                // todo: this.aviso.condominio = this.userService.getUserLogin();
                this.aviso.condomino = this.userService.getUserLogin();
            }

            this.avisoService.create(this.aviso)
                .subscribe(
                (result) => {
                    if (result.error === '0') {
                        this.toasterService.pop('success', 'Parabéns!', 'O aviso foi cadastrado com sucesso.');

                        this.router.navigate(['/avisos/']);
                    } else {
                        this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
                    }
                    this.spinnerService.hide();
                },
                (err) => {
                    console.log(`Error: ${err}`);
                    this.spinnerService.hide();
                    this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
                }
            );
        } else {
            this.toasterService.pop('error', 'Atenção!', 'Há erros de validação no formulário.');
        }

    }
    private createForm() {
        this.form = new FormGroup({
            titulo: new FormControl(null, Validators.required),
            mensagem: new FormControl(null, Validators.required),
            data_expiracao: new FormControl(null, Validators.required),
            visualizacao: new FormControl(null)
        });

        if (this.permissionService.isAdmin()) {
            this.form.controls['visualizacao'].setValue('todos');
        }
    }

    private formatDate(data: any) {
        return [data.day, data.month, data.year].join('/');
    }

}
