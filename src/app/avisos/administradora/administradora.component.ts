import { Component, OnInit } from '@angular/core';
import { Aviso } from '../../shared/models/aviso';
import { ModalService } from '../../shared/components/modal/modal.service';
import { UtilsService } from '../../shared/services/utils.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { AvisoService } from '../../shared/services/aviso.service';
import { UserService } from '../../shared/services/user.service';
import { PermissionsService } from '../../shared/services/permissions.service';

@Component({
  selector: 'app-administradora',
  templateUrl: './administradora.component.html',
  styleUrls: ['./administradora.component.scss']
})
export class AdministradoraComponent implements OnInit {

  errorMessage: string;
  submitting: boolean;
  aviso: Aviso;
  avisos: Array<Aviso> = [];
  loading = true;

  constructor(
    public userService: UserService,
    public permissionService: PermissionsService,
    private avisoService: AvisoService,
    private spinnerService: SpinnerService,
    private utils: UtilsService,
    private modalService: ModalService
  ) { }

  ngOnInit() {
    this.get();
  }

  open(title: string, message: string) {
    this.modalService.show(this.utils.convertToUTF8(title), this.utils.convertToUTF8(message));
  }

  private get() {
    this.spinnerService.show();

    this.avisoService.get_total_from_manager()
      .subscribe((result: any) => {
        this.avisos = result.Avisos;

        this.spinnerService.hide();
        this.loading = false;
      }, () => {
        this.spinnerService.hide();
        this.loading = false;
      });
  }

}
