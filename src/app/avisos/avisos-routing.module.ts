import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastrarAvisoComponent } from './cadastrar-aviso/cadastrar-aviso.component';
import { AdministradoraComponent } from './administradora/administradora.component';
import { AtualizarAvisoComponent } from './atualizar-aviso/atualizar-aviso.component';
import { QuadroDeAvisosComponent } from './quadro-de-avisos/quadro-de-avisos.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'quadro-de-avisos' },
      { path: 'avisos-da-administradora', component: AdministradoraComponent },
      { path: 'cadastrar-aviso', component: CadastrarAvisoComponent },
      { path: 'atualizar/:id', component: AtualizarAvisoComponent },
      { path: 'quadro-de-avisos', component: QuadroDeAvisosComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AvisosRoutingModule { }
