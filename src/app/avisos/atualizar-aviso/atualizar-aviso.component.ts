import { Component, OnInit } from '@angular/core';
import { Aviso } from '../../shared/models/aviso';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AvisoService } from '../../shared/services/aviso.service';
import { ApiService } from '../../shared/services/api.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { ToasterService } from 'angular2-toaster';
import { UserService } from '../../shared/services/user.service';
import { PermissionsService } from '../../shared/services/permissions.service';

@Component({
  selector: 'app-atualizar-aviso',
  templateUrl: './atualizar-aviso.component.html',
  styleUrls: ['./atualizar-aviso.component.scss']
})
export class AtualizarAvisoComponent implements OnInit {

  errorMessage: string;
  submitting: boolean;
  form: FormGroup;
  aviso: Aviso;
  private id: number;

  constructor(
    public permissionService: PermissionsService,
    public userService: UserService,
    public avisoService: AvisoService,
    private apiService: ApiService,
    private spinnerService: SpinnerService,
    private toasterService: ToasterService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.createForm();
  }

  ngOnInit() {
    this.get();
  }

  get() {
    this.id = this.route.snapshot.params.id;

    this.avisoService.get(this.id)
        .subscribe((result: any) => {
            if (result.length > 0) {
                this.aviso = result[0];

                this.form.controls['titulo'].setValue(this.aviso.titulo, {onlySelf: true});
                this.form.controls['data_expiracao'].setValue(this.aviso.data_expiracao, {onlySelf: false});
                this.form.controls['mensagem'].setValue(this.aviso.mensagem, {onlySelf: true});

                if (this.permissionService.isAdmin()) {
                    this.form.controls['visualizacao'].setValue(this.aviso.visualizacao, {onlySelf: true});
                }
            console.log(this.aviso);
            } else {
                this.router.navigate(['/avisos']);
            }
        });
  }

  onSubmit() {
    if (this.form.valid) {
      this.spinnerService.show();
      this.aviso = this.form.value;
      this.aviso.data_expiracao = this.aviso.data_expiracao ? this.formatDate(this.aviso.data_expiracao) : null;
      this.aviso.id = this.id;

      if (this.permissionService.isSindico()  || this.permissionService.isAdmin()  ||  this.permissionService.isGerente()) {
            this.aviso.condominio = this.userService.getUserLogin();
            this.aviso.condomino = '';

            if (this.permissionService.isAdmin()) {
                this.aviso.condominio = '';
            } else {
                this.aviso.visualizacao = '';
            }
            
      } else {
        this.aviso.condominio = '';
        this.aviso.condomino = this.userService.getUserLogin();
      }

      this.avisoService.update(this.aviso)
        .subscribe(
          (result) => {
            console.log(this.aviso);
            if (result.error === '0') {
              this.toasterService.pop('success', 'Parabéns!', 'O aviso foi atualizado com sucesso.');

              this.router.navigate(['/avisos/']);
            } else {
              this.toasterService.pop('error', 'Atenção!', 'O aviso não pôde ser atualizado.');
            }
            this.spinnerService.hide();
          },
          (err) => {
            console.log(`Error: ${err}`);
            this.spinnerService.hide();
            this.toasterService.pop('error', 'Atenção!', 'Há um error durante a tentativa de cadastro. Por favor, refaça.');
          }
        );
    } else {
      this.toasterService.pop('error', 'Atenção!', 'Há erros de validação no formulário.');
    }

  }
  private createForm() {
    this.form = new FormGroup({
        titulo: new FormControl(null, Validators.required),
        mensagem: new FormControl(null, Validators.required),
        data_expiracao: new FormControl(null, Validators.required),
        visualizacao: new FormControl(null)
    });
  }

  private formatDate(data: any) {
    return [data.day, data.month, data.year].join('/');
  }

}
