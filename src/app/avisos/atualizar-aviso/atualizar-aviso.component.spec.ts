import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtualizarAvisoComponent } from './atualizar-aviso.component';

describe('AtualizarAvisoComponent', () => {
  let component: AtualizarAvisoComponent;
  let fixture: ComponentFixture<AtualizarAvisoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtualizarAvisoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtualizarAvisoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
