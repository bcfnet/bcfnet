import { Component, OnInit } from '@angular/core';
import { UtilsService } from '../../shared/services/utils.service';
import { UserService } from '../../shared/services/user.service';
import { ToasterService } from 'angular2-toaster';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { AvisoService } from '../../shared/services/aviso.service';
import { Aviso } from '../../shared/models/aviso';
import { ModalService } from '../../shared/components/modal/modal.service';
import { PermissionsService } from '../../shared/services/permissions.service';

@Component({
  selector: 'app-quadro-de-avisos',
  templateUrl: './quadro-de-avisos.component.html',
  styleUrls: ['./quadro-de-avisos.component.scss']
})
export class QuadroDeAvisosComponent implements OnInit {

    errorMessage: string;
    submitting: boolean;
    aviso: Aviso;
    avisos: Array<Aviso> = [];
    loading = true;

    constructor(
        public userService: UserService,
        public permissionService: PermissionsService,
        private avisoService: AvisoService,
        private spinnerService: SpinnerService,
        private toasterService: ToasterService,
        private utils: UtilsService,
        private modalService: ModalService
    ) { }

    ngOnInit() {
        this.get();
    }

    delete(id: number) {
        const confirm = window.confirm('Você tem certeza disso?'); // todo: trocar por algo mais interessante...

        if (confirm) {
        this.spinnerService.show();
        this.avisoService.delete(id)
            .subscribe((result: any) => {
                if (result.error === '0') {
                    this.toasterService.pop('success', 'Parabéns!', `O aviso foi deletado com sucesso.`);
                    this.get();
                } else {
                    this.toasterService.pop('error', 'Atenção!', `O aviso não pôde ser deletado.`);
                }
            });
            this.spinnerService.hide();
        }
    }

    open(title: string, message: string) {
        this.modalService.show(this.utils.convertToUTF8(title), this.utils.convertToUTF8(message));
    }

    canAcess(aviso) {
        if (this.permissionService.isAdmin()) {
            return true;
        }

        if (aviso.condominio !== '' && aviso.condomino === '' && (this.permissionService.isSindico() || this.permissionService.isAdmin())) {
            return true;
        } else if (aviso.condomino !== '' && aviso.condomino !== '' && aviso.condomino === this.userService.getUserLogin()) {
            return true;
        }
        return false;
    }

    private get() {
        this.spinnerService.show();

        this.avisoService.get()
        .subscribe((result: Array<Aviso>) => {
            this.avisos = result;

            this.spinnerService.hide();
            this.loading = false;
        }, () => {
            this.spinnerService.hide();
            this.loading = false;
        });
    }

}
