import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { AvisosRoutingModule } from './avisos-routing.module';
import { CadastrarAvisoComponent } from './cadastrar-aviso/cadastrar-aviso.component';
import { AdministradoraComponent } from './administradora/administradora.component';
import { NgbModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { NgbDateMomentParserFormatter } from '../shared/extends/ngb-datemoment-parser-formatter';
import { QuadroDeAvisosComponent } from './quadro-de-avisos/quadro-de-avisos.component';
import { AtualizarAvisoComponent } from './atualizar-aviso/atualizar-aviso.component';

@NgModule({
  imports: [
    CommonModule,
    AvisosRoutingModule,
    SharedModule,
    NgbModule.forRoot(),
  ],
  declarations: [CadastrarAvisoComponent, AdministradoraComponent, QuadroDeAvisosComponent, AtualizarAvisoComponent],
  providers: [
    {
      provide: NgbDateParserFormatter,
      useFactory: () => {
        return new NgbDateMomentParserFormatter('DD/MM/YYYY');
      }
    }
  ]
})
export class AvisosModule { }
